$(document).foundation();

// main menu toggle
$('#toggleMenu').on('click', function(){
    var effect = 'slide',
        options = { direction: 'right' },
        duration = 250;
    $('#menuItems').toggle(effect, options, duration);
});

// property tour gallery
var mySwiper = new Swiper ('.swiper-container', {
   direction: 'horizontal',
   loop: true,
   speed: 1000,
   keyboardControl: true,
   nextButton: '.swiper-button-next',
   prevButton: '.swiper-button-prev'
});

// test autoplay swiper
var testSwiper = new Swiper('.swiper-test', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    direction: 'vertical',
    slidesPerView: 1,
    spaceBetween: 25,
    mousewheelControl: true
});

// property tour panel toggle on click
$('#distractionFree').on('click', function(){
    // toggle the panel and the toggle button content
    $('#propertyDetails').toggleClass('compressed');
    $(this).text(function(i, text){
        return text === " " ? "Go distraction free (esc)" : " "; 
    });
    // pin the slider left-toggle to the details panel
    $('.swiper-button-prev').toggleClass('sbp-compressed');
});

// property tour panel toggle on Escape key
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        // toggle the panel and the toggle button content
        $('#propertyDetails').toggleClass('compressed');
        $(this).text(function(i, text){
            return text === " " ? "Go distraction free (esc)" : " ";
        });
        // pin the slider left-toggle to the details panel
        $('.swiper-button-prev').toggleClass('sbp-compressed');
    }   
});

// help page expander
$('.expander-trigger').click(function(){
    $(this).toggleClass("expander-hidden");
});

// help page back button
function goBack() {
    history.go(-1);
}

$('.go-back').on('click', function(){
    goBack();
});

// This code is to fix the issue of the "Go distraction free (esc)" 
// text disappearing when the esc key is used in the property.
$(document).on('keyup',function(evt) {
    if (evt.keyCode == 27) {
       $("#distractionFree").text("Go distraction free (esc)");
    }
});

// Fixes Bitbucket issue #19
// If the user clicked esc to go distraction free and then
// clicked on the "<" symbol, the text was gone.  This code
// adds it back.
$('#distractionFree').on('click', function(){
    $("#distractionFree").text("Go distraction free (esc)");
});

