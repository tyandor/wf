var scenarioOneFeedback = JSON.parse(localStorage.getItem("feedbackScenarioOne"));

$(document).ready(function(){
    // load rick video
    jwplayer("feedback-video").setup({
        file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/vF/3NwxLgCYAiU_wT2zNpocQL8u4apCNK7OGLApuSPTU/s1-feedback.mp4?x=0&h=7f1a05f063f07c34a1cc7f13e0baa5c7",
        image: "../assets/img/ls/video-posters/rick-poster.jpg",
        tracks: [{ 
            file: "../assets/captions/s1-feedback.vtt", 
            label: "English",
            kind: "captions",
            "default": true 
        }],
        captions: {
            color: "FFCC00",
            backgroundColor: "000000",
            backgroundOpacity: 50
        }
    });

    var points = scenarioOneFeedback.points;
    if (points > 9) {
        $('#feedback-video').hide();
        $('.passed').show();
        $('.ls-fail').hide();
        $('.ls-pass').show();
    }

    // add message
    // $('.message').html(scenarioOneFeedback.message);
    var messageArray = scenarioOneFeedback.message;
    var msgHTML = $.map(messageArray, function(value) {
        return('<p>' + value + '</p>');
    });
    $(".message").html("<div class='note'>" + msgHTML.join("") + "</div>");

    // add recommended sections
    var sectionArray = scenarioOneFeedback.sections.sort();
    if(sectionArray[0] == "all") {
        $(".sections").html("All of them will be important for you.");
    } else {
        var sctHTML = $.map(sectionArray, function(value) {
            return('<a class="large button">' + value + '</a>');
        });
        $(".sections").html(sctHTML.join(""));
    }

    console.log(scenarioOneFeedback);
});

