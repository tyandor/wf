// event handler for home button
var siteurl = 'http://wf.21skills.com';

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#ffffff',
    height: '7px',
    bottom: false
});

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function() {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;

    el.on('click', function(){
        $('html, body').animate({
            scrollTop: link.offset().top
        }, 1000);
        return false;
    });

});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

    $this.append("<span></span>");
    var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(5);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// MLS Report Video
jwplayer("continuous-verification").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/Gp/10H7vNzGgodues6BKYCxqQF5g5U2TzrkR2JqtQ0ck/ls5-clues.mp4?x=0&h=f733f16f81685d16e1da9a3e8f24c751",
    image: "../assets/img/ls/video-posters/continuous-verification.jpg"
});

$(".play-continuous-verification").click(function(e){
    $('.jw-media .jw-reset').get(0).play();
    e.preventDefault();
});

// WHO NEEDS REAL ESTATE AGENTS
jwplayer("who-needs-agents").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/we/X_YwFD6VOwIvR6hoJDs3mQr87qs5HQVSxTLNckYII/ls5-leveling-with-agents.mp4?x=0&h=d5885ea19a26821d5acc279ea66cfa8f",
    image: "../assets/img/ls/video-posters/leveling-with-agents.png",
    tracks: [{ 
        file: "../assets/captions/ls5-leveling-with-agents.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// PAR LEVELING WITH AGENTS VIDEO
$(".play-agents").click(function(e){
    $('.jw-media .jw-reset').get(0).play();
    e.preventDefault();
});

// Connecting the Dots Five
jwplayer("connecting-the-dots-five").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/YU/IxFLv-TdC_Gb8jeO8zD8ufibFQYkQwV02iEmPsfNE/ls5-rick-message.mp4?x=0&h=f277ebe365b11a99e6ee25e10f2b8a9e",
    image: "../assets/img/ls/video-posters/connecting-the-dots.jpg",
    tracks: [{ 
        file: "../assets/captions/ls5-rick-message.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// learning section introduction - animate checklist on load 
var introListAnimate = new TimelineMax()
    .to(".ls-header", 0.5, {opacity: 1})
    .fromTo(".ls-header .cl-title", 0.5, {scale:0}, {scale:1}) 
    .add("clist", 1.75)
    .staggerFromTo(".ls-header .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .staggerFromTo(".ls-header .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .fromTo(".ls-header .scrollDown", 1, {opacity:0}, {opacity:1, delay:3});

$(window).on('load', function() {
    introListAnimate.play();
});

// animate all the supplimentary images
$(".circle-shape img").each(function (index, elem) {
    var circleUp = TweenMax.fromTo(elem, 0.5, {scale: 0.01, z: 500 }, {scale: 1, z: 0 });
    new ScrollMagic.Scene({
        duration: "50%",
        triggerElement: elem,
        triggerHook: "onEnter",
        offset: 100
    })
    .setTween(circleUp)
    .addTo(controller)
});

// flip the knowledge check cards
TweenMax.set(".knowledgeCheck", {perspective:800});
TweenMax.set(".check", {transformStyle:"preserve-3d"});
TweenMax.set(".back", {rotationY:-180});
TweenMax.set([".back", ".front"], {backfaceVisibility:"hidden"});

$('.forward').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:180, ease:Back.easeOut});
    $(this).parent(".check").addClass("showing");
    $(this).removeClass("forward");
    $(this).addClass("reverse");
});

$('.reverse').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:0, ease:Back.easeOut});
    $(this).parent(".check").removeClass("showing");
    $(this).removeClass("reverse");
    $(this).addClass("forward");
});

// animate the #points-to-remember list
// var rememberThis = new TimelineMax()
//     .staggerFromTo(".points-to-remember ul li", 0.5, {rotationX:-90, transformOrigin:"center top"}, {rotationX:0, ease:Power1.easeOut}, 0.25);
 
// var animateKeepUp = new ScrollMagic.Scene({
//     triggerElement: '#points-to-remember',
//     triggerHook: 0.25
// })
// .setTween(rememberThis)
// .addTo(controller);

// animate the #points-to-remember bkg and header
var whatWeExpect = new TimelineMax()
    .add("end", 1)
    .fromTo('.alpha-ramp', 1, {css:{backgroundColor:"#557A8E"}, ease:Power2.easeOut}, {css:{backgroundColor:"#5f9556"}, ease:Power2.easeOut}, "end")
    .fromTo('#points-to-remember-l5 h2', 1, {y:-700, scale:2, ease:Power2.easeOut}, {y:0, scale:1, ease:Power2.easeOut}, "end");

var animateWWE = new ScrollMagic.Scene({
    triggerElement: '#points-to-remember-l1',
    triggerHook: 0.9,
    offset: -350,
    duration: 900
})
.setTween(whatWeExpect)
.addTo(controller);

TweenMax.set(".points-to-remember ul", {visibility:"visible"});

var rememberThis = new TimelineMax()
    .staggerFromTo(".points-to-remember ul li", 0.5, {x:-250, opacity: 0, ease:Power1.easeOut}, {x:0, opacity:1, ease:Power1.easeOut}, 0.45)
    .fromTo(".l1", 0.5, {height:0}, {height:"100%"}, 2)
    .fromTo(".l2", 0.5, {width:0}, {width:"100%"})
    .fromTo(".l3", 0.5, {height:0}, {height:"100%"})
    .fromTo(".l4", 0.5, {width:0}, {width:"100%"})
    .timeScale(2);
 
var animatePTR = new ScrollMagic.Scene({
    triggerElement: '.points-to-remember',
    triggerHook: 0.5
})
.setTween(rememberThis)
.addTo(controller);


// animate learning section footer menu
var introListAnimate = new TimelineMax()
  .to(".ls-footer", 0.25, {opacity: 1})
  .fromTo(".ls-footer .cl-title", 0.5, {scale:0}, {scale:1}) 
  .add("clist", 1.75)
  .staggerFromTo(".ls-footer .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
  .staggerFromTo(".ls-footer .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist");
 
var introList = new ScrollMagic.Scene({
    triggerElement: '.ls-footer',
    triggerHook: 0.5
})
.setTween(introListAnimate)
.addTo(controller);

/* SLIDERS */
$(document).ready( function (){
    $('.bullet').first().addClass('active');
    $('.slider1-item').first().addClass('active');

    $('.bullet').click( function(){
        var $this = $(this);
        var $siblings = $this.parent().children();
        var position = $siblings.index($this);

        $('.slider1-item').removeClass('active').eq(position).addClass('active');
        $('.bullet').removeClass('active').eq(position).addClass('active');
    });

    $('.next1, .prev1').click( function(){
        var $this = $(this);
        var current = $('.slider1-items').find('.active');
        var position = $('.slider1-items').children().index(current);
        var numItems = $('.slider1-item').length;

        if($this.hasClass('next1'))
        {
            if(position < numItems-1 )
            { 
                $('.active').removeClass('active').next().addClass('active');
            } else 
            { 
                $('.slider1-item').removeClass('active').first().addClass('active'); 
                $('.bullet').removeClass('active').first().addClass('active'); 
            }//else
        } else 
        {
            if(position === 0)
            {  
                $('.slider1-item').removeClass('active').last().addClass('active');
                $('.bullet').removeClass('active').last().addClass('active'); 
            } else 
            {
                $('.active').removeClass('active').prev().addClass('active');
            }        
        }
    }); // click function
});// document ready


// SLIDER 2
$(document).ready( function (){
    $('.bullet2').first().addClass('active2');
    $('.slider2-item').first().addClass('active2');

    $('.bullet2').click( function(){
        var $this = $(this);
        var $siblings = $this.parent().children();
        var position = $siblings.index($this);

        $('.slider2-item').removeClass('active2').eq(position).addClass('active2');
        $('.bullet2').removeClass('active2').eq(position).addClass('active2');
    });

    $('.next2, .prev2').click( function(){
        var $this = $(this);
        var current = $('.slider2-items').find('.active2');
        var position = $('.slider2-items').children().index(current);
        var numItems = $('.slider2-item').length;

        if($this.hasClass('next2'))
        {
            if(position < numItems-1 )
            { 
                $('.active2').removeClass('active2').next().addClass('active2');
            } else 
            { 
                $('.slider2-item').removeClass('active2').first().addClass('active2'); 
                $('.bullet2').removeClass('active2').first().addClass('active2'); 
            }//else
        } else 
        {
            if(position === 0)
            {  
                $('.slider2-item').removeClass('active2').last().addClass('active2');
                $('.bullet2').removeClass('active2').last().addClass('active2'); 
            } else 
            {
                $('.active2').removeClass('active2').prev().addClass('active2');
            }        
        }
    }); // click function
});// document ready


$(document).ready( function (){
    $('.bullet3').first().addClass('active3');
    $('.slider3-item').first().addClass('active3');

    $('.bullet3').click( function(){
        var $this = $(this);
        var $siblings = $this.parent().children();
        var position = $siblings.index($this);

        $('.slider3-item').removeClass('active3').eq(position).addClass('active3');
        $('.bullet3').removeClass('active3').eq(position).addClass('active3');
    });

    $('.next3, .prev3').click( function(){
        var $this = $(this);
        var current = $('.slider3-items').find('.active3');
        var position = $('.slider3-items').children().index(current);
        var numItems = $('.slider3-item').length;

        if($this.hasClass('next3'))
        {
            if(position < numItems-1 )
            { 
                $('.active3').removeClass('active3').next().addClass('active3');
            } else 
            { 
                $('.slider3-item').removeClass('active3').first().addClass('active3'); 
                $('.bullet3').removeClass('active3').first().addClass('active3'); 
            }//else
        } else 
        {
            if(position === 0)
            {  
                $('.slider3-item').removeClass('active3').last().addClass('active3');
                $('.bullet3').removeClass('active3').last().addClass('active3'); 
            } else 
            {
                $('.active3').removeClass('active3').prev().addClass('active3');
            }        
        }
    }); // click function
});// document ready
