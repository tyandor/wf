var scenarioTwoFeedback = JSON.parse(localStorage.getItem("feedbackScenarioTwo")),
    s2attempts = localStorage.getItem("scenarioTwoAttempts"),
    passed = false;

$(document).ready(function(){
    // load rick video
    jwplayer("feedback-video").setup({
        file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/um/x0caD77FwsJpP_1nHv15nK-Z2YqlwUEBVVybiwAUs/stay-smart-and-farewell.mp4?x=0&h=009a389082a70a782d7ac7c08b836e05",
        image: "../assets/img/ls/video-posters/rick-poster.jpg",
        tracks: [{ 
            file: "../assets/captions/stay-smart-and-farewell.vtt", 
            label: "English",
            kind: "captions",
            "default": true 
        }],
        captions: {
            color: "FFCC00",
            backgroundColor: "000000",
            backgroundOpacity: 50
        }
    });

    // messaging variables
    var messageArray = scenarioTwoFeedback.message;
    var points = scenarioTwoFeedback.points;
    var msgHTML = $.map(messageArray, function(value) {
        return('<p>' + value + '</p>');
    });

    // display feedback notes
    $(".message").html("<div class='note'>" + msgHTML.join("") + "</div>");

    // add recommended sections
    var sectionArray = scenarioTwoFeedback.sections.sort();
    if(sectionArray[0] == "all") {
        $(".sections").html("All of them will be important for you.");
    } else {
        var sctHTML = $.map(sectionArray, function(value) {
            return('<a class="large button">' + value + '</a>');
        });
        $(".sections").html(sctHTML.join(""));
    }

    console.log(scenarioTwoFeedback);

    // determine pass/fail and reveal feedback content
    if (points >= 24) {
        $('.passed').show();
        passed = true;
        localStorage.setItem("passed", passed);
    } else if (points < 24 && JSON.stringify(s2attempts) == "2" && s2attempts == "2") {
        $('.failed-final').show();
    } else if (points < 24 && JSON.stringify(s2attempts) == "2") {
        $('.failed-second').show();
        console.log("Correct trigger");
    } else if (points < 24 && JSON.stringify(s2attempts) == "1") {
        $('.failed-first').show();
    }

    console.log("S2 Attempts: " + JSON.stringify(s2attempts));
    console.log("Passed? " + passed);

});

