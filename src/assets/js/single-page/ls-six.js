// event handler for home button
var siteurl = 'http://wf.21skills.com';

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#ffffff',
    height: '7px',
    bottom: false
});

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function() {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;

    el.on('click', function(){
        console.log(link);
        $('html, body').animate({
            scrollTop: link.offset().top
        }, 1000);
        return false;
    });

});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {
            console.log(sectionID);

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

        $this.append("<span></span>");
        var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(5);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// PAR LEVELING WITH AGENTS VIDEO
$(".play-smart").click(function(e){
    $('.video-smart').get(0).play();
    e.preventDefault();
});

// learning section introduction - animate checklist on load 
var introListAnimate = new TimelineMax()
    .to(".ls-header", 0.5, {opacity: 1})
    .fromTo(".ls-header .cl-title", 0.5, {scale:0}, {scale:1}) 
    .add("clist", 1.75)
    .staggerFromTo(".ls-header .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .staggerFromTo(".ls-header .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .fromTo(".ls-header .scrollDown", 1, {opacity:0}, {opacity:1, delay:3});

$(window).on('load', function() {
    introListAnimate.play();
});

// animate all the supplimentary images
$(".circle-shape img").each(function (index, elem) {
    var circleUp = TweenMax.fromTo(elem, 0.5, {scale: 0.01, z: 500 }, {scale: 1, z: 0 });
    new ScrollMagic.Scene({
        duration: "50%",
        triggerElement: elem,
        triggerHook: "onEnter",
        offset: 100
    })
    .setTween(circleUp)
    .addTo(controller)
});

// animate the #search-by-features graph
var $features = $('#features'),
    $price = $('#price'),
    $lines = $('.line'),
    $graph = $('#graph'),
    $graphMask = $('#graphMask'),
    mainTl = new TimelineMax();

function clearGraph() {
    var clearTl = new TimelineMax();
    clearTl
        .set($lines, {drawSVG: 0})
        .set($graph, {autoAlpha: 0.9})
        .set($graphMask, {attr: {x: -960}});
    return clearTl;
}

function animateGraph() {
    var graphTl = new TimelineMax();
    //tweens
    graphTl
        // .to($graphMask, 1.77, {attr: {x: 42}, ease:Power4.easeOut});
    return graphTl;
}

function init() {
    mainTl
        .add(clearGraph());
}

init();

// create scene to animate in the graph
var graphIn = new TimelineMax()
    .to($graphMask, 1.9, {attr: {x: 42}, ease:Power4.easeOut});

var scene = new ScrollMagic.Scene({triggerElement: "#search-by-features", triggerHook: 0.25, duration: 401})
    .setTween(graphIn)
    .addTo(controller);

// flip the knowledge check cards
TweenMax.set(".knowledgeCheck", {perspective:800});
TweenMax.set(".check", {transformStyle:"preserve-3d"});
TweenMax.set(".back", {rotationY:-180});
TweenMax.set([".back", ".front"], {backfaceVisibility:"hidden"});

$('.forward').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:180, ease:Back.easeOut});
    $(this).parent(".check").addClass("showing");
    $(this).removeClass("forward");
    $(this).addClass("reverse");
});

$('.reverse').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:0, ease:Back.easeOut});
    $(this).parent(".check").removeClass("showing");
    $(this).removeClass("reverse");
    $(this).addClass("forward");
});

// Flip card
CSSPlugin.defaultTransformPerspective = 1000;

TweenMax.set($(".back"), {rotationY:-180});

// Flip Card One
$.each($(".father"), function(i,element) {
  
    var frontCard = $(this).children(".front"),
      backCard = $(this).children(".back"),
      tl = new TimelineMax({paused:true, reversed:true});
    
    tl
        .to(frontCard, 1, {rotationY:180})
        .to(backCard, 1, {rotationY:0},0)
        .to(element, .5, {z:50},0)
        .to(element, .5, {z:0},.5);
    
    element.animation = tl;
  
});


$(".father").click(function () { 
    this.animation.reversed() ? this.animation.play() : this.animation.reverse();       
});

// Flip Card Two
TweenMax.set($(".back-two"), {rotationY:-180});

$.each($(".father-two"), function(i,element) {
  
    var frontCard = $(this).children(".front-two"),
      backCard = $(this).children(".back-two"),
      tl = new TimelineMax({paused:true, reversed:true});
    
    tl
        .to(frontCard, 1, {rotationY:180})
        .to(backCard, 1, {rotationY:0},0)
        .to(element, .5, {z:50},0)
        .to(element, .5, {z:0},.5);
    
    element.animation = tl;
  
});

$(".father-two").click(function () { 
    this.animation.reversed() ? this.animation.play() : this.animation.reverse();       
});

// Connecting the Dots Six
jwplayer("connecting-the-dots-six").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/Zm/Y1r8Ch418pSJ7u1lvQkETUJHRoeQbJUAHyVpmNNg8/ls6-rick-message.mp4?x=0&h=3b1c6390e4fdff81813cc9e6c569174e",
    image: "../assets/img/ls/video-posters/connecting-the-dots.jpg",
    tracks: [{ 
        file: "../assets/captions/ls6-rick-message.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// animate the #points-to-remember bkg and header
var whatWeExpect = new TimelineMax()
    .add("end", 1)
    .fromTo('.alpha-ramp', 1, {css:{backgroundColor:"#557A8E"}, ease:Power2.easeOut}, {css:{backgroundColor:"#5f9556"}, ease:Power2.easeOut}, "end")
    .fromTo('#points-to-remember-l6 h2', 1, {y:-700, scale:2, ease:Power2.easeOut}, {y:0, scale:1, ease:Power2.easeOut}, "end");

var animateWWE = new ScrollMagic.Scene({
    triggerElement: '#points-to-remember-l1',
    triggerHook: 0.9,
    offset: -350,
    duration: 900
})
.setTween(whatWeExpect)
.addTo(controller);

TweenMax.set(".points-to-remember ul", {visibility:"visible"});

var rememberThis = new TimelineMax()
    .staggerFromTo(".points-to-remember ul li", 0.5, {x:-250, opacity: 0, ease:Power1.easeOut}, {x:0, opacity:1, ease:Power1.easeOut}, 0.45)
    .fromTo(".l1", 0.5, {height:0}, {height:"100%"}, 2)
    .fromTo(".l2", 0.5, {width:0}, {width:"100%"})
    .fromTo(".l3", 0.5, {height:0}, {height:"100%"})
    .fromTo(".l4", 0.5, {width:0}, {width:"100%"})
    .timeScale(2);
 
var animatePTR = new ScrollMagic.Scene({
    triggerElement: '.points-to-remember',
    triggerHook: 0.5
})
.setTween(rememberThis)
.addTo(controller);

// get the link for the final simulation
var siteurl = 'http://wf.21skills.com';

if(!(Cookies.get('fsLink'))) {
    var finalSimulation = getRandomInt(1, 3);
    Cookies.set('fsLink', finalSimulation);
}

if(Cookies.get('fsLink') == 1) {
    $('#goToFinalSimulation').attr('href', siteurl+'/mod/scorm/view.php?id=3');
} else if(Cookies.get('fsLink') == 2) {
    $('#goToFinalSimulation').attr('href', siteurl+'/mod/scorm/view.php?id=4');
}

