// SIMULATION TUTORIAL VIDEO
jwplayer("simulation-tutorial").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/Nl/RiIfVGUZdsRc9gqTgQSKtsIdvib4T7-sgPHkAkzUQ/wf-first-sim-tut.mp4?x=0&h=45bf8e18e46020d81f6b383466ff6941",
    image: "../assets/img/tut-vid-poster.png",
    tracks: [{ 
        file: "../assets/captions/home-page.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

