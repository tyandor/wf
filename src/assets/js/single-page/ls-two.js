// event handler for home button
var siteurl = 'http://wf.21skills.com';

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#ffffff',
    height: '7px',
    bottom: false
});

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function() {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;

    el.on('click', function(){
        console.log(link);
        $('html, body').animate({
            scrollTop: link.offset().top
        }, 1000);
        return false;
    });

});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {
            console.log(sectionID);

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

    $this.append("<span></span>");
    var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(3);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// animate all the supplimentary images
$(".circle-shape img").each(function (index, elem) {
    var circleUp = TweenMax.fromTo(elem, 0.5, {scale: 0.01, z: 500 }, {scale: 1, z: 0 });
    new ScrollMagic.Scene({
        duration: "50%",
        triggerElement: elem,
        triggerHook: "onEnter",
        offset: 100
    })
    .setTween(circleUp)
    .addTo(controller)
});

// fade in the features-not-price graph
var featuresUp = new TimelineMax()
    .fromTo("#features-not-price", 1, {opacity:0}, {opacity:1});
 
var animateKeepUp = new ScrollMagic.Scene({
    duration: "35%",
    triggerElement: '#features-not-price',
    triggerHook: 0.5
})
.setTween(featuresUp)
.addTo(controller);

// // animate the #points-to-remember list
// var rememberThis = new TimelineMax()
//     .staggerFromTo(".points-to-remember ul li", 0.5, {rotationX:-90, transformOrigin:"center top"}, {rotationX:0, ease:Power1.easeOut}, 0.25);
 
// var animateKeepUp = new ScrollMagic.Scene({
//     triggerElement: '#points-to-remember',
//     triggerHook: 0.25
// })
// .setTween(rememberThis)
// .addTo(controller);

// animate the #points-to-remember bkg and header
var whatWeExpect = new TimelineMax()
    .add("end", 1)
    .fromTo('.alpha-ramp', 1, {css:{backgroundColor:"#557A8E"}, ease:Power2.easeOut}, {css:{backgroundColor:"#5f9556"}, ease:Power2.easeOut}, "end")
    .fromTo('#points-to-remember-l2 h2', 1, {y:-700, scale:2, ease:Power2.easeOut}, {y:0, scale:1, ease:Power2.easeOut}, "end");

var animateWWE = new ScrollMagic.Scene({
    triggerElement: '#points-to-remember-l1',
    triggerHook: 0.9,
    offset: -350,
    duration: 900
})
.setTween(whatWeExpect)
.addTo(controller);

TweenMax.set(".points-to-remember ul", {visibility:"visible"});

var rememberThis = new TimelineMax()
    .staggerFromTo(".points-to-remember ul li", 0.5, {x:-250, opacity: 0, ease:Power1.easeOut}, {x:0, opacity:1, ease:Power1.easeOut}, 0.45)
    .fromTo(".l1", 0.5, {height:0}, {height:"100%"}, 2)
    .fromTo(".l2", 0.5, {width:0}, {width:"100%"})
    .fromTo(".l3", 0.5, {height:0}, {height:"100%"})
    .fromTo(".l4", 0.5, {width:0}, {width:"100%"})
    .timeScale(2);
 
var animatePTR = new ScrollMagic.Scene({
    triggerElement: '.points-to-remember',
    triggerHook: 0.5
})
.setTween(rememberThis)
.addTo(controller);

// animate learning section footer menu
var introListAnimate = new TimelineMax()
  .to(".ls-footer", 0.25, {opacity: 1})
  .fromTo(".ls-footer .cl-title", 0.5, {scale:0}, {scale:1}) 
  .add("clist", 1.75)
  .staggerFromTo(".ls-footer .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
  .staggerFromTo(".ls-footer .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist");
 
var introList = new ScrollMagic.Scene({
    triggerElement: '.ls-footer',
    triggerHook: 0.5
})
.setTween(introListAnimate)
.addTo(controller);

// VIDEO KNOWLEDGE CHECK
jwplayer("video-knowledge-check-two").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/8j/NSQGrVOJIYY2s-prDB79FKdJ6B6tBnZa03eKdpaYw/gloc-excelsior-h264.mp4?x=0&h=144d7fd6588d6b99f2a0422795b6538a",
    image: "../assets/mov/gloc/gloc-poster.png",
    tracks: [{ 
        file: "../assets/captions/gloc-excelsior-h264.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});


// Connecting the Dots Two
jwplayer("connecting-the-dots-two").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/FS/RzF1JXpT5N9sQXnI_1rkcirmVexPS62q4JLCCUZtw/ls2-rick-message.mp4?x=0&h=73e4a81948dc79db1ae3d012d801be0e",
    image: "../assets/img/ls/video-posters/connecting-the-dots.jpg",
    tracks: [{ 
        file: "../assets/captions/ls2-rick-message.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// Knowledge Check         
function checkAnswer() {

    var radioValue = $("input[name='check-two']:checked").val();

    if (radioValue=='correct'){
        console.log('You got it!');
        $("#knowledge-check-response").css({display: 'inline'});
        $("#knowledge-check-response div:first").removeClass('kc-incorrect');
        $("#knowledge-check-response div:first").addClass('kc-correct');               
        $("#knowledge-check-response h3").empty();
        $("#knowledge-check-response p").empty();
        $("#knowledge-check-response h3").append('Correct'); 
        $("#knowledge-check-response p").append('Yes! You should analyze at least two years of market data in order to see the whole picture.'); 
        
    } else if (radioValue =='incorrect'){
        console.log('Try again');
        $("#knowledge-check-response").css({display: 'inline'});
        $("#knowledge-check-response div:first").removeClass('kc-correct');
        $("#knowledge-check-response div:first").addClass('kc-incorrect');
        $("#knowledge-check-response h3").empty();
        $("#knowledge-check-response p").empty();
        $("#knowledge-check-response h3").append('Incorrect');  
        $("#knowledge-check-response p").append('Slow down! Make sure you understand the market before you take any further actions.');
    }
};
    
function myFunction() {
    document.getElementById("knowledge-check-two").reset();
    $("#knowledge-check-response").css({display: 'none'});
    $("#knowledge-check-response h3").empty();
    console.log('working');
}

