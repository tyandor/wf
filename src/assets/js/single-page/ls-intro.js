// event handler for home button
var siteurl = 'http://wf.21skills.com';

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#fff',
    height: '7px',
    bottom: false
});

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function(index, elem) {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;
});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {
            console.log(sectionID);

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

    $this.append("<span></span>");
    var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(5);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// ls intro loading animation
var whyAmIHere = new TimelineMax();
whyAmIHere.insertMultiple([
    new TweenMax.fromTo("#why-am-i-here h1", 1, {y:-500}, {ease: Power3.easeOut, y:0}),
    new TweenMax.fromTo("#why-am-i-here .ls-copy", 1.5, {opacity:0}, {opacity:1, delay:1.5}),
    new TweenMax.fromTo(".scrollDown", 0.5, {y:100}, {ease: Power3.easeOut, y:0, delay:3})
]);

// house in a box animation
var houseInBox = new TimelineMax();
houseInBox.insertMultiple([
    new TweenMax.fromTo("#house", 2, {scale:0, opacity:0}, {scale:1, opacity: 1}),
    new TweenMax.fromTo("#house", 4, {x:0, y:-1000, rotation:0, transformOrigin:"center center"}, {x:-250, y:-400, rotation:-22, transformOrigin:"center center"})
]);

var putHouseInBox = new ScrollMagic.Scene({
    triggerElement: "#what-is-a-supported-appraisal",
    triggerHook: 0.25,
    duration: "100%" 
})
.setTween(houseInBox)
.addTo(controller);

// #packing-boxes conveyor belt video
var conveyorVideo = document.getElementById("conveyor");

function conveyPlay() {
    conveyorVideo.play();
}
function conveyPause() {
    conveyorVideo.pause();
}

new ScrollMagic.Scene({ triggerElement: "#packing-boxes", triggerHook: 0.5, duration: '150%' })
    .on('start', function () {
        conveyPlay();
    })
    .on('end', function () {
        conveyPause();
    })
    .addTo(controller);

// learning section introduction - animate checklist on scroll 
var introListAnimate = new TimelineMax()
  .to(".ls-footer", 0.25, {opacity: 1})
  .fromTo("#ls-introduction .cl-title", 0.5, {scale:0}, {scale:1}) 
  .add("clist", 1.75)
  .staggerFromTo("#ls-introduction .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
  .staggerFromTo("#ls-introduction .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist");
 
var introList = new ScrollMagic.Scene({
    triggerElement: '#ls-introduction',
    triggerHook: 0.5
})
.setTween(introListAnimate)
.addTo(controller);

