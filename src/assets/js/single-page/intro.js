$(document).ready(function() {
    Cookies.set('intro', true);
});

var $whatNow = $('#whatNow'),
    $interact = $('#interact'),
    $fillOut = $('#fillOut'),
    $tour = $('#tour'),
    $downloads = $('#downloads'),
    $getSupport = $('#getSupport')
    $helpOverlay = $('#helpOverlay')
    $gotIt = $('#gotIt');

$('#menuItems').show();

function hideIntro() {
    var hideIntroTl = new TimelineMax();

    hideIntroTl
        .add('menu')
        .to($interact, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, 'menu')
        .to($fillOut, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, 'menu')
        .to($tour, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, 'menu')
        .to($downloads, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, 'menu')
        .to($getSupport, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, 'menu')
        .to($whatNow, 0.75, {y: -500, opacity: 0, display: 'none'})
        .to($gotIt, 0.5, {opacity: 0, display: 'none'})
        .to($helpOverlay, 0.5, {opacity: 0, display: 'none'});

    return hideIntroTl;
}

function runIntroTl() {
    var introTl = new TimelineMax();

    introTl
        .fromTo($helpOverlay, 0.5, {opacity: 0, display: 'none'}, {opacity: 1, display: 'block'})
        .fromTo($whatNow, 0.75, {y: -500, opacity: 0, display: 'none'}, {y: 0, opacity: 1, display: 'block'})
        .fromTo($interact, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, {y: 0, scale: 1, opacity: 1, display: 'block', ease: Power3.easeOut})
        .fromTo($fillOut, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, {y: 0, scale: 1, opacity: 1, display: 'block', ease: Power3.easeOut})
        .fromTo($tour, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, {y: 0, scale: 1, opacity: 1, display: 'block', ease: Power3.easeOut})
        .fromTo($downloads, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, {y: 0, scale: 1, opacity: 1, display: 'block', ease: Power3.easeOut})
        .fromTo($getSupport, 0.5, {y: -200, scale: 0, transformOrigin: 'center top', display: 'none'}, {y: 0, scale: 1, opacity: 1, display: 'block', ease: Power3.easeOut})
        .fromTo($gotIt, 1, {opacity: 0, display: 'none'}, {opacity: 1, display: 'block'}, "+=2.5");

    return introTl;
}

if(!(Cookies.get('intro'))) {
    runIntroTl();
}

// reverse animation on #gotIt click
$('#gotIt').on('click', function(){
    hideIntro();
});

