// MLS In-Depth Tutorial S2
jwplayer("mls-tutorial").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/_G/4R2mzUIbV2j-fmh1X39RfnqDdeuT9OLYJxjoExX3s/mls-in-depth-tut-revised-2.mp4?x=0&h=9477c44af883e85ce4e49e8217652b97",
    image: "../assets/img/mls-indepth-poster.jpg",
    tracks: [{ 
        file: "../assets/captions/mls-in-depth-tut.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

