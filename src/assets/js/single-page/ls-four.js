// event handler for home button
var siteurl = 'http://wf.21skills.com';

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#ffffff',
    height: '7px',
    bottom: false
});

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function() {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;

    el.on('click', function(){
        console.log(link);
        $('html, body').animate({
            scrollTop: link.offset().top
        }, 1000);
        return false;
    });

});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {
            console.log(sectionID);

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

        $this.append("<span></span>");
        var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(3);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// learning section introduction - animate checklist on load 
var introListAnimate = new TimelineMax()
    .to(".ls-header", 0.5, {opacity: 1})
    .fromTo(".ls-header .cl-title", 0.5, {scale:0}, {scale:1}) 
    .add("clist", 1.75)
    .staggerFromTo(".ls-header .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .staggerFromTo(".ls-header .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
    .fromTo(".ls-header .scrollDown", 1, {opacity:0}, {opacity:1, delay:3});

$(window).on('load', function() {
    introListAnimate.play();
});

// animate all the supplimentary images
$(".circle-shape img").each(function (index, elem) {
    var circleUp = TweenMax.fromTo(elem, 0.5, {scale: 0.01, z: 500 }, {scale: 1, z: 0 });
    new ScrollMagic.Scene({
        duration: "50%",
        triggerElement: elem,
        triggerHook: "onEnter",
        offset: 100
    })
    .setTween(circleUp)
    .addTo(controller)
});

// flip the knowledge check cards
TweenMax.set(".knowledgeCheck", {perspective:800});
TweenMax.set(".check", {transformStyle:"preserve-3d"});
TweenMax.set(".back", {rotationY:-180});
TweenMax.set([".back", ".front"], {backfaceVisibility:"hidden"});

$('.forward').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:180, ease:Back.easeOut});
    $(this).parent(".check").addClass("showing");
    $(this).removeClass("forward");
    $(this).addClass("reverse");
});

$('.reverse').on('click', function(){
    TweenMax.to($(this).parent(".check"), 1.2, {rotationY:0, ease:Back.easeOut});
    $(this).parent(".check").removeClass("showing");
    $(this).removeClass("reverse");
    $(this).addClass("forward");
});


// Video - WOULD YOU MAKE THIS MISTAKE 
jwplayer("would-you-make-mistake").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/L_/E3dRAlOPJ-s79_Y32BAMEZuCBzABlBrg6A2IOR79E/ls4-1of2-hills-and-flats.mp4?x=0&h=7016e7555e93920d407724f434e4d332",
    image: "../assets/img/ls/video-posters/highs-and-lows.jpg",
    tracks: [{ 
        file: "../assets/captions/ls4-1of2-highs-and-lows.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// Video - DO YOU KNOW YOUR BUYER?
$(".play-hills").click(function(e){
    $('.jw-media .jw-reset').get(0).play();
    e.preventDefault();
});


// AVOID THE MULLIGAN
jwplayer("avoid-mulligan").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/Pb/A91kmeOymgs7Q3VElt2nQW_N7lQJ9s-fp1iEl9Rrw/ls4-2of2-par-for-the-course-final.mp4?x=0&h=dffc5b2822fc1476973abffc8cd0d468",
    image: "../assets/img/ls/video-posters/par-for-the-course.jpg",
    tracks: [{ 
        file: "../assets/captions/ls4-2of2-par-for-the-course-final.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

// Video - DO YOU KNOW YOUR BUYER?
$(".play-golf").click(function(e){
    $('.jw-media .jw-reset').get(1).play();
    e.preventDefault();
});


// GLOC 
jwplayer("video-knowledge-check-four").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/fa/LfruKJ2BcnAJhNAI-tvUvCJT9PzfwbLGsPgXS1dU0/gloc-beachfront-bliss-final.mp4?x=0&h=d4b5a782956b8b5381d38c0937b92267",
    image: "../assets/mov/gloc/gloc-poster.png",
    tracks: [{ 
        file: "../assets/captions/gloc-beachfront-bliss-final.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});


// Connecting the Dots Four
jwplayer("connecting-the-dots-four").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/0M/t6i5QfWF4tbreuEGzZeBZajZvGmaYxNZcoVLIw3K8/ls4-rick-message.mp4?x=0&h=137d9dffe1477cacdc869d1aec121782",
    image: "../assets/img/ls/video-posters/connecting-the-dots.jpg",
    tracks: [{ 
        file: "../assets/captions/ls4-rick-message.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});


// animate the #points-to-remember list
// var rememberThis = new TimelineMax()
//     .staggerFromTo(".points-to-remember ul li", 0.5, {rotationX:-90, transformOrigin:"center top"}, {rotationX:0, ease:Power1.easeOut}, 0.25);
 
// var animateKeepUp = new ScrollMagic.Scene({
//     triggerElement: '#points-to-remember',
//     triggerHook: 0.25
// })
// .setTween(rememberThis)
// .addTo(controller);

// animate the #points-to-remember bkg and header
var whatWeExpect = new TimelineMax()
    .add("end", 1)
    .fromTo('.alpha-ramp', 1, {css:{backgroundColor:"#557A8E"}, ease:Power2.easeOut}, {css:{backgroundColor:"#5f9556"}, ease:Power2.easeOut}, "end")
    .fromTo('#points-to-remember-l4 h2', 1, {y:-700, scale:2, ease:Power2.easeOut}, {y:0, scale:1, ease:Power2.easeOut}, "end");

var animateWWE = new ScrollMagic.Scene({
    triggerElement: '#points-to-remember-l1',
    triggerHook: 0.9,
    offset: -350,
    duration: 900
})
.setTween(whatWeExpect)
.addTo(controller);

TweenMax.set(".points-to-remember ul", {visibility:"visible"});

var rememberThis = new TimelineMax()
    .staggerFromTo(".points-to-remember ul li", 0.5, {x:-250, opacity: 0, ease:Power1.easeOut}, {x:0, opacity:1, ease:Power1.easeOut}, 0.45)
    .fromTo(".l1", 0.5, {height:0}, {height:"100%"}, 2)
    .fromTo(".l2", 0.5, {width:0}, {width:"100%"})
    .fromTo(".l3", 0.5, {height:0}, {height:"100%"})
    .fromTo(".l4", 0.5, {width:0}, {width:"100%"})
    .timeScale(2);
 
var animatePTR = new ScrollMagic.Scene({
    triggerElement: '.points-to-remember',
    triggerHook: 0.5
})
.setTween(rememberThis)
.addTo(controller);

// #ls4 #adjustmentsVideo video
$(document).ready(function() {
    var adjustmentsVideo = document.getElementById("#adjustmentsVideo");
});

function adjustmentsPlay() {
    adjustmentsVideo.play();
}
function adjustmentsPause() {
    adjustmentsVideo.pause();
}

new ScrollMagic.Scene({ 
    triggerElement: "#adjustmentsVideo", 
    triggerHook: 0.5
})
.on('enter', function () {
    adjustmentsPlay();
})
.on('end', function () {
    adjustmentsPlay();
})
.addTo(controller);

// animate learning section footer menu
var introListAnimate = new TimelineMax()
  .to(".ls-footer", 0.25, {opacity: 1})
  .fromTo(".ls-footer .cl-title", 0.5, {scale:0}, {scale:1}) 
  .add("clist", 1.75)
  .staggerFromTo(".ls-footer .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
  .staggerFromTo(".ls-footer .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist");
 
var introList = new ScrollMagic.Scene({
    triggerElement: '.ls-footer',
    triggerHook: 0.5
})
.setTween(introListAnimate)
.addTo(controller);

// Knowledge Check         
function checkAnswer() {

    var radioValue = $("input[name='check-four']:checked").val();

    if (radioValue=='correct'){
        console.log('You got it!');
        $("#knowledge-check-response").css({display: 'inline'});
        $("#knowledge-check-response div:first").removeClass('kc-incorrect');
        $("#knowledge-check-response div:first").addClass('kc-correct');               
        $("#knowledge-check-response h3").empty();
        $("#knowledge-check-response p").empty();
        $("#knowledge-check-response h3").append('Correct'); 
        $("#knowledge-check-response p").append('Yes! Sensitivity analysis is not a widely recognized technique to determine and support adjustments.'); 

        
    } else if (radioValue =='incorrect'){
        console.log('Try again');
        $("#knowledge-check-response").css({display: 'inline'});
        $("#knowledge-check-response div:first").removeClass('kc-correct');
        $("#knowledge-check-response div:first").addClass('kc-incorrect');
        $("#knowledge-check-response h3").empty();
        $("#knowledge-check-response p").empty();
        $("#knowledge-check-response h3").append('Incorrect');  
        $("#knowledge-check-response p").append('No, you can definitely use that technique to determine and support adjustments.');
    }
};
    
function myFunction() {
    document.getElementById("knowledge-check-four").reset();
    $("#knowledge-check-response").css({display: 'none'});
    $("#knowledge-check-response h3").empty();
    console.log('working');
}

