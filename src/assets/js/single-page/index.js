// set site url
var siteurl = 'http://www.theeliteappraiser.com';

// main menu event handlers
$('.open-menu').on('click', function(){
    $('.lp-navigation').slideToggle(250);
});

$('.close-menu').on('click', function(){
    $('.lp-navigation').slideToggle(250);
});

// set link for final simulation
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

if(!(Cookies.get('fsLink'))) {
    var finalSimulation = getRandomInt(1, 3);
    Cookies.set('fsLink', finalSimulation);
}

if(Cookies.get('fsLink') == 1) {
    $('#finalSimulation').attr('href', siteurl+'/mod/scorm/view.php?id=3');
} else if(Cookies.get('fsLink') == 2) {
    $('#finalSimulation').attr('href', siteurl+'/mod/scorm/view.php?id=4');
}

// link to initial simulation and set cookie ot enable learning sections
$(document).on("click", "#firstSimulation, #getStarted", function () {
    Cookies.set('ls', 'true');
    window.location.href = siteurl+"/mod/scorm/view.php?id=1";
});

// enable learning section links
if(Cookies.get('ls')) {
    $('.lp-navigation.bottom').hide();
    $('#firstSimulation').addClass('active');
    $('#learningSections, .learning-sections a').removeClass('disabled');
    $('.lp-navigation.top').show();
    $('#landing-page-tutorial, .problem-solution, #introduction').hide();
}

// event handlers for learning section links
// $(document).on("click", "#lsintro", function () {
//    window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/introduction.html"
// });

// $(document).on("click", "#lsone", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/one.html"
// });

// $(document).on("click", "#lstwo", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/two.html"
// });

// $(document).on("click", "#lsthree", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/three.html"
// });

// $(document).on("click", "#lsfour", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/four.html"
// });

// $(document).on("click", "#lsfive", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/five.html"
// });

// $(document).on("click", "#lssix", function () {
//     window.location.href = siteurl+"/pluginfile.php/36/mod_resource/content/2/wf-appraisal-learning-sections/learning-sections/six.html"
// });

// click handler to show intro content
$('#siv').on('click', function(){
    $('#landing-page-tutorial, .problem-solution, #introduction').show();
    $('.lp-navigation.top').addClass('shortened');
});

// set cookie to indicate engagement with learning sections
$(document).on("click", ".learning-sections a, #learningSections", function () {
    Cookies.set('fs', 'true');
});

// enable final simulation link
if(Cookies.get('fs')) {
    $('#learningSections, .learning-sections a').addClass('active');
    $('#finalSimulation').removeClass('disabled');
    $('.lp-navigation.top .lead').html("Continue reviewing learning section content by selecting the number of the learning section you would like to view. If you have finished reviewing the learning sections, select the <span>Final Simulation</span> button to enter the Final Simulation.");
}

// set cookie upon activation of first try at final simulation
$(document).on("click", "#finalSimulation", function () {
    Cookies.set('completed', 'true');    
});

// switch the final sumulation link in case user needs a second try
function switchSimulation() {
    if(Cookies.get('fsLink') == 1) {
        $('#finalSimulation').html('Final Simulation 2').attr('href', siteurl+'/mod/scorm/view.php?id=4');
    $('#finalSimulation').attr('href', siteurl+'/mod/scorm/view.php?id=4');
    } else if(Cookies.get('fsLink') == 2) {
        $('#finalSimulation').html('Final Simulation 2').attr('href', siteurl+'/mod/scorm/view.php?id=3');
    }
}

// run switchSimulation if user has completed first try
if(Cookies.get('completed')) {
    switchSimulation();
}

// redirect for launching final simulation second try 
// for users coming from LS6 or FS feedback pages
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    else {
       return results[1] || 0;
    }
}

if($.urlParam('fsredirect')=='true') {
    if(Cookies.get('fsLink') == 1) {
        window.location.href = siteurl+'/mod/scorm/view.php?id=4';
    } else if(Cookies.get('fsLink') == 2) {
        window.location.href = siteurl+'/mod/scorm/view.php?id=3';
    } 
}

// event handler for logout
$(document).on("click", ".log-out", function () {
    window.location.href = siteurl+"/login/logout.php";
});

// temp func to use in dev to clear cookies via the console
function clearCookies() {
    Cookies.remove('ls');
    Cookies.remove('fs');
    Cookies.remove('completed');
    location.reload();
}

// get feedback object from S1 to add .active to learning sections

// video modal functions
function rickPlay() {
    var rickVideo = document.getElementById("rickIntro");
    rickVideo.play();
}
function rickPause() {
    var rickVideo = document.getElementById("rickIntro");
    rickVideo.pause();
}

// trigger play/pause on open/close of the modal
$('#rickVideo').on('open.zf.reveal', function() {
    rickPlay();
});

$('#rickVideo').on('closed.zf.reveal', function() {
    rickPause();
});

// TUTORIAL VIDEO
jwplayer("tutorialVideo").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/Ai/NWQ_UaNcDhIOvZh4czvpR5-LjTgivP2_JjEhS5iro/wf-landing-page-tut.mp4?x=0&h=5f11b5277ed306b778c36b172b032e18",
    image: "assets/img/tut-vid-poster.png",
    tracks: [{ 
        file: "assets/captions/s1-intro.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

