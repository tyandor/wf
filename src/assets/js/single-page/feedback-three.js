var scenarioThreeFeedback = JSON.parse(localStorage.getItem("feedbackScenarioThree")),
    s3attempts = localStorage.getItem("scenarioThreeAttempts"),
    passed = false;

$(document).ready(function(){
    // load rick video
    jwplayer("feedback-video").setup({
        file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/um/x0caD77FwsJpP_1nHv15nK-Z2YqlwUEBVVybiwAUs/stay-smart-and-farewell.mp4?x=0&h=009a389082a70a782d7ac7c08b836e05",
        image: "../assets/img/ls/video-posters/rick-poster.jpg",
        tracks: [{ 
            file: "../assets/captions/stay-smart-and-farewell.vtt", 
            label: "English",
            kind: "captions",
            "default": true 
        }],
        captions: {
            color: "FFCC00",
            backgroundColor: "000000",
            backgroundOpacity: 50
        }
    });

    // messaging variables
    var messageArray = scenarioThreeFeedback.message;
    var points = scenarioThreeFeedback.points;
    var msgHTML = $.map(messageArray, function(value) {
        return('<p>' + value + '</p>');
    });
    
    // display point count for testing
    $(".percentage").html("<div class='stat'>" + points + "</div>");

    // display feedback notes
    $(".message").html("<div class='note'>" + msgHTML.join("") + "</div>");

    // add recommended sections
    var sectionArray = scenarioThreeFeedback.sections.sort();
    if(sectionArray[0] == "all") {
        $(".sections").html("All of them will be important for you.");
    } else {
        var sctHTML = $.map(sectionArray, function(value) {
            return('<a class="large button">' + value + '</a>');
        });
        $(".sections").html(sctHTML.join(""));
    }

    // determine pass/fail and reveal feedback content
    if (points >= 24) {
        $('.passed').show();
        passed = true;
        localStorage.setItem("passed", passed);
    } else if (points < 24 && s3attempts == 2 && s3attempts == 2) {
        $('.failed-final').show();
    } else if (points < 24 && s3attempts == 2) {
        $('.failed-second').show();
    } else if (points < 24 && s3attempts == 1) {
        $('.failed-first').show();
    }
   
    console.log("S3 Attempts: " + s3attempts);
    console.log("Passed? " + passed);

});

