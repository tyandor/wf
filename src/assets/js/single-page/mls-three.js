// JavaScript prototypes
Number.prototype.toCurrency = function(cent){
    var num = this;
    cent = cent || parseInt(cent) ? 2 : 0;
    return "$" + num.toFixed(cent).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
};

Number.prototype.toMarked = function(){
    return '' + String(this).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

String.prototype.toNumber = function(){
    var str = this;
    if(str.length > 0){
        str = str.replace(/[^0-9\.]+/g, "");
    } else {
        str = "0";
    }
    // Check for decimal or round number
    if(str.indexOf(".") > -1){
        return parseFloat(str);
    } else {
        return parseInt(str);
    }
};

// Grouped data
var groupedData3 = [];
var tempGroupedData3 = [];

// Comparable data
// var compData = JSON.parse(localStorage.getItem("compDataCollectionS3"));
var scormData = parent.scormGet('cmi.suspend_data');
if(!(typeof scormData == 'undefined' || scormData === null || scormData == '')){
    scenarioThreeWorksheet = JSON.parse(scormData);
    if(scenarioThreeWorksheet.comparableData.length > 0){
        compData = scenarioThreeWorksheet.comparableData;
    }else{
        var compData=[];
    }
}else{
    var scenarioThreeWorksheet={};
    var compData=[];
}

if(!compData){
    compData = [];
}
var tempCompData = [];
var arrayRawData = [];

$('#viewSavedData').hide();

// bottom right toolbar toggle
$('#toggleMenu').on('click', function(){
    $('#menuItems').toggle();
});

// toggle the downloads popup
$('#toggleDp').on('click', function(){
    $('.dropdown-pane').toggleClass('is-open');
});

var monthNames = ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

// set the height of the DataTable relative to the #mlsToolbar
var dtHeight = $(window).height() - $('#mlsToolbar').height() - 90;  

// init and config the data table
var table = $('#mlsData').DataTable({
    "order": [],
    ajax: '../assets/mls-data/data-s3.json',
    columns: [
    {
        sortable: false,
        "className": 'details-control',
        "data": null,
        "defaultContent": '', 
        "searchable": false
    },
    {data: 'checkbox', 
        render: function (data, type, row) {
            if (type === 'display') {
                return '<input type="checkbox" class="selected_item editor-active">';
            }
            return data;
        },
        className: "dt-body-center",
        sortable: false, 
        "searchable": false
    },            
    {data: 'status', 
        "searchable": true},
    {data: 'streetNumber', 
        sortable: true, 
        "searchable": false},
    {data: 'streetName', 
        sortable: true, 
        "searchable": false},
    {data: 'closingDate',
        render: function (data, type, row) {
            if (type === 'display' || type === 'filter') {
                var rowvalue = row["EventDate"];
                var rowvalueallday = row["AllDayEvent"];
                if(data != ''){
                    if (rowvalueallday == 0) {
                        return (moment(data).format("MM/DD/YYYY (HH:mm)"));
                    } else {
                        return (moment(data).format("MM/DD/YYYY"));
                    }
                }
            }
            return data;
        }, 
        "searchable": true
    },
    {data: 'soldPrice',
        render: $.fn.dataTable.render.number( ',', '.', 0, '$' ),
        "searchable": false
    },
    {data: 'listPrice', 
        render: $.fn.dataTable.render.number( ',', '.', 0, '$' ),
        "searchable": false},
    {data: 'listDate',
        render: function (data, type, row) {
            if (type === 'display' || type === 'filter') {
                var rowvalue = row["EventDate"];
                var rowvalueallday = row["AllDayEvent"];
                if (rowvalueallday == 0) {
                    return (moment(data).format("MM/DD/YYYY (HH:mm)"));
                } else {
                    return (moment(data).format("MM/DD/YYYY"));
                }
            }
            return data;
        }, 
        "searchable": false
    },
    {data: 'gla', 
        "searchable": true},
    {data: 'yearBuilt', 
        "searchable": true},
    {data: 'siteSquareFeet', 
        "searchable": true},
    {data: 'bed', 
        "searchable": true},
    {data: 'bath', 
        "searchable": false},
    {data: 'view', 
        sortable: false, 
        visible: false},
    {data: 'area', 
        visible: false, 
        "searchable": true},
    {data: 'designStyle', 
        visible: false, 
        "searchable": false},
    {data: 'quality', 
        visible: false, 
        "searchable": false},
    {data: 'condition', 
        visible: false, 
        "searchable": false},
    {data: 'garage', 
        visible: false, 
        "searchable": false},
    {data: 'amenities', 
        visible: false}
    ],
    paging: true,
    bFilter: false,
    bLengthChange: false,
    searching: true,
    scrollY: dtHeight,
    scrollX: true,
    select: true,
    scroller: {
        loadingIndicator: false
    },
    fixedHeader: {
        header: true,
        footer: true
    },
    // Datatables bootstrap modal for row details
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    tempCompData = data;
                    
                    // Check if the property is already added to the comparable collection
                    var visible = 'visible';
                    for(var i = 0; i < compData.length; i++ ){
                        if(compData.length > 8 || compData[i]['id'] == tempCompData['id']){
                            visible = 'hidden';
                            break;
                        }
                    }

                    return 'Details for '+data.streetNumber+' '+data.streetName;
                }
            } ),
            renderer: 
                function ( api, rowIdx, columns ) {
                    var data = $.map( columns, function ( col, i ) {
                        return col.columnIndex > 1 ?
                            '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
                            '<td style=\"height: 32px; margin: 10px; width: 140px;\"><b>'+col.title+':'+'</b></td> '+
                            '<td style=\"margin: 10px;\">'+col.data+'</td>'+
                            '</tr>' :
                            '';
                    } ).join('');
                    return data ?
                        $('<table class="table table-sm table-striped"/>').append( data ) :
                        false;
                }
        }
    },
    "infoCallback": function(settings, start, end, max, total, pre){
      return "Showing " + start + "-" + end + " of " + total + " Properties"
         + ((total !== max) ? " (filtered from " + max + " total entries)" : "");
   }    
});

// tooltips for Property Details and Select All Properties
$('#propertyDetails').tooltip();
$('#checkall').tooltip();

// tie the modal-close action to the Escape key
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.modal').modal('hide');
    }   
});

// enable tabs on #detailsView
$('#dataDetails a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
});
// tie saveForGroupedDataAnalysis button to tab
$('.saveForGroupedDataAnalysis').click(function (e) {
    e.preventDefault();
    $('#groupedDataTab a').tab('show');
    $('#dataDetails li').each(function() {
        $(this).removeClass('active');
    });
    $('#groupedDataTab').addClass('active');
    $('#txtGroupedDataName').focus();
});

// show 'view saved data' button on main view
$('#btnGroupedDataSave').click(function (e) {
    e.preventDefault();
    $('#viewSavedData').show();
});

if (localStorage.getItem("groupedData3") !== null) {
    $('#viewSavedData').show();
}

// global search function
$('#globalFilter').keyup(function () {
    table.search($(this).val()).draw();
    UpdateMinMax();
});

// multi-select controls
$('.selectpicker').selectpicker({
    style: 'btn-default',
    liveSearch: false,
    size: 'auto'
});

// date range picker
$('.input-daterange input').each(function () {
    $(this).datepicker("clearDates");
});

// slide controls
var slider, slider1, slider2, slider3;
var selectedMarkets = [];
var selectedStatus = [];
var selectedView = [];
var startDate = "";
var endDate = "";
var arr = [];
var dollar_vals = [];
var checklen;
var chartOutput = [];
var htmlString1 = "";
var htmlString = "";
var data;
var sqlQuery = "";
var tableInfo = "";

$('#butReset').click(function(){
    // Delete all local storage for the scenario
    localStorage.removeItem("groupedData3");
    // localStorage.removeItem("compDataCollectionS3");
    // localStorage.removeItem("worksheetScenarioThree");
    // localStorage.removeItem("feedbackScenarioThree");
    parent.scormSet('cmi.suspend_data', '');
    location.reload();
});

$('#graphOneMonth').click(function(){
    $('#benchmarkingGraphDetail').show();
    $('#benchmarkingGraphDetail2').hide();
    $('#benchmarkingGraphDetail3').hide();
});

$('#graphTwoMonth').click(function(){
    $('#benchmarkingGraphDetail').hide();
    $('#benchmarkingGraphDetail2').show();
    $('#benchmarkingGraphDetail3').hide();
});

$('#graphThreeMonth').click(function(){
    $('#benchmarkingGraphDetail').hide();
    $('#benchmarkingGraphDetail2').hide();
    $('#benchmarkingGraphDetail3').show();
});


$(function () {
    // action to go back to the mls view while maintaining state
    $(document).on('click', ".returnBack", function () {
        resetChecked();
        $("#detailsView").hide();
        $("#mlsToolbar").show();
        $("#dataTable").show();
        $("#code").hide();
        $("#viewComparisonData").prop('disabled', true);
    });

    $('#checkall').click(function (event) {  //on click
        // Reset all checkbox on datatables
        var allrows = table.rows().nodes();
        $(allrows).prop('checked', false);

        var rows = table.rows({'search': 'applied'}).nodes();
        $('.selected_item', rows).prop('checked', this.checked);
        
        verifyCheckedRow();
    });

    table.on("change", ":checkbox", function(){
        verifyCheckedRow();
    });

    $("#viewComparisonData").on('click', function () {

        $("#benchmarkingGraphDetail").show();
        $("#benchmarkingGraphDetail2").hide();
        $("#benchmarkingGraphDetail3").hide();
        
        htmlString = "";
        propertyCount = "";
        sqlQuery = "";
        tableInfo = "";
        arrayRawData = [];
        // $(".bg-inverse").show();

        //      var arrFieldLabels = ["", "Sale Date: " , "Sale Price: " , "Area: " , "Site: " , "View: " , "Yearbuild: " , "Gla: " , "Bed: " , "Bath: "];
        var i = 1;
        var propertyNo = "";
        var propertyAddress = "";
        var propertyClosingDate = "";
        var propertysoldPrice = "";
        var propertymarketArea = "";
        var propertysite = "";
        var propertyview = "";
        var propertyyearBuilt = "";
        var propertygla = "";
        var propertybeds = "";
        var propertybaths = "";
        var selectedClass = "";

        // Array for chart data
        var dataArray = [];
        var soldCount = 0;
        var activePendingCount = 0;
        var totalSelectedCount = 0;

        // Loop through datatables
        table.$(".selected_item").each(function () {
            if ($(this).is(":checked")) {
                var selected_tr = $(this).closest("tr");
                data = table.data().row(selected_tr).data();

                // Raw data array
                arrayRawData.push(data);

                // Build array for chart data
                // Only graph sold property and count the sold/not sold types
                if(data.status.toLowerCase() == 'sld'){
                    dataArray.push(data);
                    soldCount++;
                } else {
                    activePendingCount++;
                }
                totalSelectedCount++;

                if (selectedClass == "") {
                    selectedClass = "odd";
                } else {
                    selectedClass = "";
                }
                //               console.log(table.data().row(selected_tr).data().closingDate);
                sqlQuery += "Insert into table values (";
                sqlQuery += "'" + "Property# " + i + "'";
                sqlQuery += ", '" + data.streetNumber + " " + data.streetName + "'";
                sqlQuery += ", '" + data.closingDate + "'";
                sqlQuery += ", '" + data.soldPrice + "'";
                sqlQuery += ", '" + data.area + "'";
                sqlQuery += ", '" + data.siteSquareFeet + "'";
                sqlQuery += ", '" + data.view + "'";
                sqlQuery += ", '" + data.yearBuilt + "'";
                sqlQuery += ", '" + data.gla + "'";
                sqlQuery += ", '" + data.bed + "'";
                sqlQuery += ", '" + data.bath + "');";
                sqlQuery += "<br>";

                htmlString1 = "";
                htmlString1 += "<div class='col-md-3'><div class='panel panel-default'>";
                htmlString1 += "<div class='item_info panel-heading'><h2 class='panel-title'>" + data.streetNumber + " " + data.streetName + "</h2></div>";
                htmlString1 += "<div class='list-group'>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Sale Date:</h5><p class='list-group-item-text'> " + data.closingDate + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Sale Price:</h5><p class='list-group-item-text'> " + data.soldPrice + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Area:</h5><p class='list-group-item-text'> " + data.area + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Site:</h5><p class='list-group-item-text'> " + data.siteSquareFeet + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>View:</h5><p class='list-group-item-text'> " + data.view + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Year Built:</h5><p class='list-group-item-text'> " + data.yearBuilt + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Gla:</h5><p class='list-group-item-text'> " + data.gla + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Beds:</h5><p class='list-group-item-text'> " + data.bed + "</p></div>";
                htmlString1 += "<div class='list-group-item item_info " + selectedClass + "'><h5 class='list-group-item-heading'>Baths:</h5><p class='list-group-item-text'> " + data.bath + "</p></div>";
                htmlString1 += "<div class='callControls'><p>Speak with the:</p>";
                htmlString1 += "<div class='row'><div class='col-xs-6'><a href='../s3/conversation/"+data.listingAgent+"' class='btn btn-primary btn-block' target='_blank'>Listing Agent</a></div>";
                htmlString1 += "<div class='col-xs-6'><a href='../s3/conversation/"+data.sellingAgent+"' class='btn btn-primary btn-block' target='_blank'>Selling Agent</a></div></div>";
                htmlString1 += "</div>";

                // Check if the property is already in the comparable collection
                var visible = true;

                for(var i = 0; i < compData.length; i++ ){
                    if(compData.length > 8 || compData[i]['id'] == data.id){
                        visible = false;
                        break;
                    }
                }

                if(compData.length < 13 && visible){
                    htmlString1 += "<div class='comparableControl callControls'><button class='btn btn-primary btn-block' target='_blank' dataId=\""+data.id+"\"><span class='glyphicon glyphicon-plus'></span>  Add comp to Answer Form</button></div>";
                }

                if(!visible){
                    htmlString1 += "<div class='removeComparableControl callControls'><button class='btn btn-primary btn-block' target='_blank' dataId=\""+data.id+"\"><span class='glyphicon glyphicon-minus'></span>  Remove comp from Answer Form</button></div>";
                }

                htmlString1 += "</div></div></div>";
                htmlString += htmlString1;

                //Tables Display Code
                propertyNo += "<td>Property " + i + "</td>";
                propertyAddress += "<td>" + data.streetNumber + " " + data.streetName + "</td>";
                propertyClosingDate += "<td>Sale Date: " + data.closingDate + "</td>";
                propertysoldPrice += "<td>Sale Price: " + data.soldPrice + "</td>";
                propertymarketArea += "<td>Area: " + data.area + "</td>";
                propertysite += "<td>Site: " + data.siteSquareFeet + "</td>";
                propertyview += "<td>View: " + data.view + "</td>";
                propertyyearBuilt += "<td>Year Built: " + data.yearBuilt + "</td>";
                propertygla += "<td>Gla: " + data.gla + "</td>";
                propertybeds += "<td>Beds: " + data.bed + "</td>";
                propertybaths += "<td>Baths: " + data.bath + "</td>";
                i += 1;
            }
        });

        // Calculate raw data for Grouped Data tab to an object
        tempGroupedData3 = processGroupedData(arrayRawData);
        
        tableInfo = '<table id="export_table"><tr>' + propertyNo + '</tr><tr>' + propertyAddress + '</tr><tr>' + propertyClosingDate + '</tr><tr>' + propertysoldPrice + '</tr><tr>' + propertymarketArea + '</tr><tr>' + propertysite + '</tr><tr>' + propertyview + '</tr><tr>' + propertyyearBuilt + '</tr><tr>' + propertygla + '</tr><tr>' + propertybeds + '</tr><tr>' + propertybaths + '</tr>';
        tableInfo += '</table>';

        // Add selected property count based on status, does not need the active and pending count
        selectedProperty = "";
        selectedProperty += "Showing <span>" + totalSelectedCount + "</span> Properties Records";

        propertyCountDetail = "";
        propertyCountDetail = "<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Note:</strong> Only properties with a status of Sold are represented in the Benchmarking Graph and Data</div>";
        
        // Insufficient sales warning
        propertyCountDetail += soldCount < 5 ? "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Important Note:</strong> Months with a Sales Count of less than 5 records is unlikely to generate a realiable trend due to insufficient data.</div>" : "";
        propertyCount += propertyCountDetail;

        // switch views from Home to Details
        $("#detailsView").show();
        $("#selectedPropertiesDetail").html(htmlString);
        $("#propertyCountDetail").html(selectedProperty);
        $(".notewarning").html(propertyCountDetail);
        // $("#table_export").html(tableInfo);
        $("#mlsToolbar").hide();
        $("#dataTable").hide();

        // Rendering chart
        // Build chart data source
        chartOutput = [];
        for(var n = 0; n < dataArray.length; n++){
            if(dataArray[n] != null){
                var dollar = dataArray[n].soldPrice;
                var objData = {
                    'saledate': Date.parse(dataArray[n].closingDate),
                    'saleprice': dollar
                };
                chartOutput.push(objData);
            }
        }

        // Sort chart data, ascending order
        chartOutput.sort(function (a, b) {
            return a.saledate - b.saledate
        });
        
        var aMonthAverages = [];
        var monthSaleTotal = 0;
        var monthSaleCount = 0;
        var aMonthNames = [];
        var monthTotalArray = [];
        var monthSaleCountArray = [];

         // Get the last date from the data series
        var index = chartOutput.length - 1;
        var lastDt = '';
        if(chartOutput[index] && typeof chartOutput[index].saledate !== "undefined"){
            lastDt = new Date(chartOutput[index].saledate);
        } else {
            lastDt = new Date(chartOutput[index-1].saledate)
        }

        // Set date range from the date picker
        var startDt = startDate ? startDate : '2014-01-06';
        var endDt = endDate ? endDate : lastDt;

        var curDate = new Date(startDt);
        var maxDate = new Date(endDt);
        // Set max date to the next day (12 am)
        maxDate.setDate(maxDate.getDate()+1);

        // Loop through dates, to group data by month
        while(curDate < maxDate){
            var monthSaleData = [];
            // Loop through the selected data
            for(var i = 0; i < chartOutput.length; i++){
                var dt = new Date(chartOutput[i].saledate);
                if(dt.getMonth() == curDate.getMonth() && dt.getYear() == curDate.getYear()){
                    monthSaleTotal += chartOutput[i].saleprice;
                    monthSaleCount += 1;
                    monthSaleData.push(chartOutput[i].saleprice);
                }                 
            }

            var tempMonthAve = monthSaleTotal > 0 ? monthSaleTotal/monthSaleCount : 0;
            var tempMonthMedian = getMedian(monthSaleData);
            aMonthAverages.push({'monthAverage': tempMonthAve, 'monthSaleTotal': monthSaleTotal, 'monthSaleCount' : monthSaleCount, 'monthMedian' : tempMonthMedian});
            monthTotalArray.push(monthSaleTotal);
            aMonthNames.push(curDate.toString());
            
            monthSaleCountArray.push({'dateMonth' : curDate.toString(), 'monthSaleTotal': monthSaleTotal, 'monthSaleCount' : monthSaleCount, 'monthMedianValue' : tempMonthMedian, 'monthAverageValue' : tempMonthAve, 'monthSaleData' : monthSaleData});

            // Set for the next month
            monthSaleTotal = 0;
            monthSaleCount = 0;
            curDate.setDate(1);
            curDate.setMonth(curDate.getMonth() + 1);
        }

        var MonthValues = [];
        var AverageArray = [];
        var MedianArray = [];
        var objThreeMonthTotal = new Object();
        var objMonthSaleCount = new Object();
        var lineChartArray = [];
        
        // Setup month values for chart
        for (var b = 0; b < aMonthNames.length; b++) {
            var date = new Date(aMonthNames[b]);
            var month = monthNames[date.getMonth()];
            var year = date.getFullYear();
            var dateFormat = month + ' ' + year;
            MonthValues.push(dateFormat);
        }

        // Padding for the first two months data
        lineChartArray[0] = null;
        lineChartArray[1] = null;
        
        for (var c = 0; c < aMonthAverages.length; c++) {
            AverageArray.push(aMonthAverages[c].monthAverage);
            MedianArray.push(aMonthAverages[c].monthMedian);

            // Monthly sale count
            objMonthSaleCount[MonthValues[c]] = aMonthAverages[c].monthSaleCount;
            
            if (c > 1) {
                var threeMonthTotal = aMonthAverages[c].monthSaleCount + aMonthAverages[c - 1].monthSaleCount + aMonthAverages[c - 2].monthSaleCount;
                var movingSaleTotal = aMonthAverages[c].monthSaleTotal + aMonthAverages[c - 1].monthSaleTotal + aMonthAverages[c - 2].monthSaleTotal;
                var movingAverage = (aMonthAverages[c].monthMedian + aMonthAverages[c - 1].monthMedian + aMonthAverages[c - 2].monthMedian) / 3;
                // movingAverage = threeMonthTotal > 0 ? Math.round((movingSaleTotal/threeMonthTotal)/1000)*1000 : 0; // Round to the nearest 1000
                
                lineChartArray.push(movingAverage);
                objThreeMonthTotal[MonthValues[c]] = threeMonthTotal;                
            }
        }

        $('#benchmarkingGraphDetail').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: MonthValues
            }],
            yAxis: [{// Primary yAxis
                labels: {
                    format: '${value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                min: 0,
                max: Math.max.apply(Math, AverageArray)
            }, {// Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '${value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true,
                min: 0,
                max: Math.max.apply(Math, AverageArray)
            }],
            legend: {
                    layout: 'horizontal',
                    align: 'left',
                    x: 0,
                    verticalAlign: 'top',
                    y: 0,
                    floating: false,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            credits:{
                enabled: false
            },
            series: [{
                name: 'Median Sale Price',
                type: 'column',
                yAxis: 1,
                color: '#74983E',
                data: MedianArray,
                tooltip: {
                    valuePrefix: '$'
                }

            }, {
                name: '3-Month Average of Median Sale Price',
                type: 'line',
                color: '#a02b22',
                visible: true,
                data: lineChartArray,
                toolTip: {
                    valuePrefix: '$'
                } 
            }],
            tooltip: {
                valuePrefix: '$',
                shared: true,
                crosshairs: true,
                formatter: function() {
                    var s = '<b>'+ this.x +'</b><br/>';                
                    $.each(this.points, function(i, point) {
                        s += '<span style="color:'+point.series.color+'">'+point.series.name+'</span>: <b>'+point.y.toCurrency()+'</b><br />'
                        s += i == 0 && objMonthSaleCount[this.x] ? '<span style="color:'+point.series.color+'">Number of Sales:</span> <b>' + objMonthSaleCount[this.x] + '</b><br />' : '' ;
                    });
                    s+= objThreeMonthTotal[this.x] ? '3-Month Sales Total: <b>' + objThreeMonthTotal[this.x] + '</b>' : '';
                    return s;
                },
            }
        });

        // toggle active class on month button group
        $('#monthView .btn-group button').click(function() {
            $(this).addClass('active').siblings().removeClass('active');
        });
        
        // Build and render benchmarking data
        // Rolling month sale count
        $('#tblMonthSaleCount').DataTable({
            order: [],
            bInfo: false,
            destroy: true, 
            data: monthSaleCountArray,
            columns: [
                {data: 'dateMonth', 
                    sortable: false,
                    render: function(data, type, row){
                        var dt = new Date(data);
                        return monthNames[dt.getMonth()] + ' ' + dt.getFullYear();
                    }
                },
                {data: 'monthSaleCount', sortable: false},
                {data: 'monthMedianValue', sortable: false,
                    render: function(data, type, row){
                        return data > 0 ? data.toCurrency(0) : '';
                    }
                } 
            ],
            paging: false,
            searching: false
        });
        // Rolling two month sale count
        // Build array for data source
        var arrayTwoMonthSaleCount = compoundMonthSaleCount(monthSaleCountArray, 2);

        // Benchmark graph
        plotRunningMonthGraph($('#benchmarkingGraphDetail2'), arrayTwoMonthSaleCount);
                
        // Benchmark data
        $('#tblTwoMonthSaleCount').DataTable({
            order: [],
            bInfo: false,
            destroy: true,
            data: arrayTwoMonthSaleCount,
            columns: [
                {data: 'monthRange', sortable: false},
                {data: 'saleCount', sortable: false},
                {data: 'medianValue', sortable: false}
            ],
            paging: false,
            searching: false
        });

        // Rolling three-month sale count
        // Build array for data source
        var arrayThreeMonthSaleCount = compoundMonthSaleCount(monthSaleCountArray, 3);

        // Benchmark graph
        plotRunningMonthGraph($('#benchmarkingGraphDetail3'), arrayThreeMonthSaleCount);

        // Benchmark data
        $('#tblThreeMonthSaleCount').DataTable({
            order: [],
            bInfo: false,
            destroy: true,
            data: arrayThreeMonthSaleCount,
            columns: [
                {data: 'monthRange', sortable: false},
                {data: 'saleCount', sortable: false},
                {data: 'medianValue', sortable: false}
            ],
            paging: false,
            searching: false
        });

        
        
        // Check if local storage is supported by the browser
        if(!window.localStorage){
            $("#storageWarning").html("Sorry! Your browser does not support this Web Storage to use this feature.");
            // Disable input interface
            $("#txtGroupedDataFileName").prop('disabled', true);
            $("#btnGroupedDataSave").prop('disabled', true);
            $("#btnGroupedDataDeleteAll").prop('disabled', true);

        } else {
            // Enable input interface
            $("#txtGroupedDataName").prop('disabled', false);
            $("#btnGroupedDataSave").prop('disabled', false);
            $("#btnGroupedDataDeleteAll").prop('disabled', false);

            // Load grouped data, if exists
            if(!(localStorage.getItem("groupedData3") === null || localStorage.getItem("groupedData3") == '')){
                groupedData3 = JSON.parse(localStorage.getItem("groupedData3"));
                loadGroupedData(groupedData3);
            }
        };
    }); // end of viewComparisonData click event handler



    $(".Hidegraph").on('click', function () {
        if ($(".Hidegraph").html() == "Hide Graph") {
            $("#container").hide();
            $(".Hidegraph").html("View Graph");
        } else {
            $("#container").show();
            $(".Hidegraph").html("Hide Graph");
        }
    });

    $(".saveValues").on('click', function () {
        $(".modal").modal('show');
        $(".modal-body").html(sqlQuery);

    });
    function format(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>View:</td>' +
            '<td>' + d.view + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Market Area:</td>' +
            '<td>' + d.marketArea + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Half Bath:</td>' +
            '<td>' + d.halfBath + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Rooms:</td>' +
            '<td>' + d.rooms + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Garage:</td>' +
            '<td>' + d.garage + '</td>' +
            '</tr>' +
            '</table>';
    }

    $(".dt-body-center").removeClass("sorting_asc");
    $(".dt-body-center").addClass("sorting_disabled");
    $('.market').on('change', function () {
        selectedMarkets = [];
        $(this).find("option:selected").each(function () {
            //            $(".first_check").css({
            //                "display": 'none !important'
            //            });

            selectedMarkets.push($(this).val());
        });
        $(".dt-body-center").removeClass("sorting_asc");
        $(".dt-body-center").addClass("sorting_disabled");
        UpdateMinMax();
        $(this).selectpicker('toggle');
        table.draw();
    });

    $('.status').on('change', function () {
        selectedStatus = [];
        $(this).find("option:selected").each(function () {
            selectedStatus.push($(this).val());
        });
        UpdateMinMax();
        $(this).selectpicker('toggle');
        table.draw();
    });
    $('.view').on('change', function () {
        selectedView = [];
        $(this).find("option:selected").each(function () {
            selectedView.push($(this).val());
        });
        UpdateMinMax();
        $(this).selectpicker('toggle');
        table.draw();
    });
    var startingDate = new Date('08/05/2014');
    $('#datepicker').datepicker('setStartDate', startingDate)
        .on("input change", function (e) {
            startDate = $(this).val();
            table.draw();
        });
    var endingDate = new Date('09/23/2016');
    $('#datepicker1').datepicker('setEndDate', endingDate)
        .on("input change", function (e) {
            endDate = $(this).val();
            table.draw();
        });

    slider = $('#glaSlider').slider().on('slide', function () {
        UpdateMinMax();
        table.draw();
    }).data('slider');
    slider1 = $('#siteSlider').slider().on('slide', function () {
        UpdateMinMax();
        table.draw();
    }).data('slider');
    slider2 = $('#bedsSlider').slider().on('slide', function () {
        UpdateMinMax();
        table.draw();
    }).data('slider');
    slider3 = $('#yearSlider').slider().on('slide', function(){
        UpdateMinMax();
        table.draw();
    }).data('slider');
});

// Filter by built year
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        var yearValue = $('#yearSlider').val();
        aYear = yearValue.split(",");
        var min = parseInt(aYear[0], 10);
        var max = parseInt(aYear[1], 10);
        var y = parseInt(data[10], 10) || 0;
        
        if(y >= min && y <= max){
            return true;
        } 
        return false;
    }
);

// Filter datatables based on site's square feet
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var siteValue = $("#siteSlider").val();
            aSite = siteValue.split(",");
            var min = parseInt(aSite[0], 10);
            var max = parseInt(aSite[1], 10);
            var site = data[11].toNumber(); // use data for the site column
           
            if ((isNaN(min) && isNaN(max)) ||
                    (isNaN(min) && site <= max) ||
                    (min <= site && isNaN(max)) ||
                    (min <= site && site <= max))
            {
                return true;
            }
            return false;
        }
    );

// Filter datatables based on GLA
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var glaValue = $("#glaSlider").val();
            aGla = glaValue.split(",");
            var min = parseInt(aGla[0], 10);
            var max = parseInt(aGla[1], 10);
            var gla = parseFloat(data[9]) || 0; // use data for the gla column

            if ((isNaN(min) && isNaN(max)) ||
                    (isNaN(min) && gla <= max) ||
                    (min <= gla && isNaN(max)) ||
                    (min <= gla && gla <= max))
            {
                return true;
            }
            return false;
        }
        );

// Filter datatables based on # of bedroom
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var bedsValue = $("#bedsSlider").val();
            aBeds = bedsValue.split(",");
            var min = parseInt(aBeds[0], 10);
            var max = parseInt(aBeds[1], 10);
            var beds = parseFloat(data[12]) || 0; // use data for the age column

            if ((isNaN(min) && isNaN(max)) ||
                    (isNaN(min) && beds <= max) ||
                    (min <= beds && isNaN(max)) ||
                    (min <= beds && beds <= max))
            {
                return true;
            }
            return false;
        }
        );

// Filter datatables based on market area
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (selectedMarkets.length > 0) {
                for (var i = 0; i < selectedMarkets.length; i++) {
                    if (selectedMarkets[i] == data[15]) {
                        return true;
                    }
                }
                return false;
            } else {
                return true;
            }
        }
        );

// Filter datatables based on status
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (selectedStatus.length > 0) {
                for (var i = 0; i < selectedStatus.length; i++) {
                    if (selectedStatus[i] == data[2]) {
                        return true;
                    }
                }
                return false;
            } else {
                return true;
            }
        }
        );



// Filter datatables based on view
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (selectedView.length > 0) {
                for (var i = 0; i < selectedView.length; i++) {
                    if (data[14].indexOf(selectedView[i]) != -1) {

                        return true;
                    }
                }
                return false;
            } else {

                return true;
            }
        }
        );

// Filter datatables based on closing date, compared to the selected range start date
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (startDate != "") {
                if (moment(data[5]).isAfter(startDate, 'day')) {
                    return true;
                }
                return false;
            } else {
                return true;
            }
        }
        );

// Closing date is within the selected date range.
$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (endDate != "") {
                if (moment(data[5]).isBefore(endDate, 'day')) {
                    return true;
                }
                return false;
            } else {
                return true;
            };
        }
);

function UpdateMinMax() {
    $(".select-checkbox").removeClass("sorting_asc");
    $(".select-checkbox").addClass("sorting_disabled");

    // Get filtered data
    var filteredData = table.rows({filter: 'applied'}).data();
    // Calculate low, median, high
    var result = processGroupedData(filteredData);

    if(filteredData.length == 0){
        $("#salelow").html("");
        $("#salemed").html("");
        $("#salehigh").html("");

        $("#glalow").html("");
        $("#glamed").html("");
        $("#glahigh").html("");

        $("#bedslow").html("");
        $("#bedsmed").html("");
        $("#bedshigh").html("");

        // This is actually for lot size
        $("#bathslow").html("");
        $("#bathsmed").html("");
        $("#bathshigh").html("");

        $("#yearlow").html("");
        $("#yearmed").html("");
        $("#yearhigh").html("");
    } else {
        $("#salelow").html(result.soldPrice.low.toCurrency(0));
        $("#salemed").html(result.soldPrice.median.toCurrency(0));
        $("#salehigh").html(result.soldPrice.high.toCurrency(0));

        $("#glalow").html(result.gla.low);
        $("#glahigh").html(result.gla.high);
        $("#glamed").html(result.gla.median);

        $("#bedslow").html(result.beds.low);
        $("#bedshigh").html(result.beds.high);
        $("#bedsmed").html(result.beds.median);

        // For lot size
        $("#bathslow").html(result.lotSize.low);
        $("#bathshigh").html(result.lotSize.high);
        $("#bathsmed").html(result.lotSize.median);

        $("#yearlow").html(result.buildYear.low);
        $("#yearhigh").html(result.buildYear.high);
        $("#yearmed").html(result.buildYear.median);
    }
};


function genDivs(v) {
    var e = document.body; // whatever you want to append the rows to: 
    for (var i = 0; i < v; i++) {
        var row = document.createElement("div");
        row.className = "row";
        for (var x = 1; x <= v; x++) {
            var cell = document.createElement("div");
            cell.className = "gridsquare";
            cell.innerText = (i * v) + x;
            row.appendChild(cell);
        }
        e.appendChild(row);
    }
    document.getElementById("code").innerText = e.innerHTML;

};

function verifyCheckedRow(){
    checklen = table.$(":checkbox:checked");
    if (checklen.length > 1) {
        $("#viewComparisonData").prop("disabled", false);
    } else {
        $("#viewComparisonData").prop("disabled", true);
    }
};


function compoundMonthSaleCount(array, step){
    // Build result array 
    var arrayTemp = [];
    var tempMedianArray = [];
    var mergedCount = 0;
    var mergedValue = 0;
    var dtRange = "";
    var tempDate = new Date(array[0].dateMonth)

    // Insert empty data row
    // Commented for now
    // for(var n = 0; n < step-1; n++){
    //     arrayTemp.push({'monthRange': '&#160;', 'saleCount': '&#160;', 'medianValue': '&#160;'});
    // }

    // Loop through monthly dataset
    for(var i = 0; i < (array.length - (step - 1)); i++){
        tempDate = new Date(array[i].dateMonth);                
        var endDate = new Date(array[i].dateMonth);
            endDate = new Date(endDate.setMonth(endDate.getMonth() + (step - 1)));
        dtRange = monthNames[tempDate.getMonth()];
            dtRange += tempDate.getFullYear() == endDate.getFullYear() ? '' : ' ' + tempDate.getFullYear();
            dtRange += ' - ' + monthNames[endDate.getMonth()] + ' ' + endDate.getFullYear();    
        
        // Combine months sale count
        mergedCount = 0;
        mergedValue = 0;
        tempMedianArray = [];
        for(var s = 0; s < step; s++){
            if(array[i+s]){
                mergedCount += array[i+s].monthSaleCount;
                mergedValue += array[i+s].monthSaleTotal;
                tempMedianArray = tempMedianArray.concat(array[i+s].monthSaleData);
            };
        }
        
        arrayTemp.push({
            'monthStart': tempDate,
            'monthEnd': endDate,
            'monthRange' : dtRange,
            'saleCount' : mergedCount,
            'medianValue' : mergedCount > 0 ? getMedian(tempMedianArray).toCurrency(0) : ''
        });                       
    };


    return arrayTemp;
}

function plotRunningMonthGraph(divGraph, arraySource){
    var arrayForMonthGraph = [];
    var arrayForMonthChart = [];
    var arrayForMonthList = [];

    for(var i = 0; i < arraySource.length; i++){
        // Month list
        var dt = new Date(arraySource[i].monthEnd);
        arrayForMonthList.push(monthNames[dt.getMonth()] + ' ' + dt.getFullYear()) ;
        // Median value
        arrayForMonthGraph.push(arraySource[i].medianValue.toNumber());

        // Three months rolling
        var total = 0;
        if(i>1){
            total = arraySource[i].medianValue.toNumber() + arraySource[i-1].medianValue.toNumber() + arraySource[i-2].medianValue.toNumber();
            arrayForMonthChart.push(Math.round(total/3/1000)*1000);
        } else {
            arrayForMonthChart.push('');
        };
    }
    
    // Benchmark graph
    divGraph.highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: arrayForMonthList
        }],
        yAxis: [{// Primary yAxis
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            min: 0,
            max: Math.max.apply(Math, arrayForMonthGraph)
        }, {// Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true,
            min: 0,
            max: Math.max.apply(Math, arrayForMonthGraph)
        }],
        legend: {
                layout: 'horizontal',
                align: 'left',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        credits:{
            enabled: false
        },
        series: [{
            name: 'Median Sale Price',
            type: 'column',
            yAxis: 1,
            color: '#74983E',
            data: arrayForMonthGraph,
            tooltip: {
                valuePrefix: '$'
            }

        }, {
            name: '3-Month Average of Median Sale Price',
            type: 'line',
            color: '#a02b22',
            visible: true,
            data: arrayForMonthChart,
            toolTip: {
                valuePrefix: '$'
            } 
        }],
        tooltip: {
            valuePrefix: '$',
            shared: true,
            crosshairs: true,
            formatter: function() {
                var s = '<b>'+ this.x +'</b><br/>';                
                $.each(this.points, function(i, point) {
                    s += '<span style="color:'+point.series.color+'">'+point.series.name+'</span>: <b>'+point.y.toCurrency()+'</b><br />'
                });
                return s;
            },
        }
    });
}

// Grouped Data event handlers
// Delete saved grouped data
$("#groupedDataDetail").on('click', 'button.btngdDelete', function(){
    var i = parseInt($(this).attr("recordId"));
    if(i >= 0){
        // Remove item from array
        groupedData3 = JSON.parse(localStorage.getItem("groupedData3"));
        // If there are more than one item stored in the local storage
        if(groupedData3.length > 1){
            if(confirm("Delete grouped data "+ $(this).attr("dataName") +"?")){
                // Remove item from array by index
                groupedData3.splice(i, 1);
                // Store array back to local storage
                localStorage.setItem("groupedData3", JSON.stringify(groupedData3));
            }
        } else {
            if(confirm("The last stored data is about to be deleted. Are you sure?")){
                deleteAllGroupedData();
            }
        }
    }
    // Refresh display
    loadGroupedData(groupedData3);
});

// Temporary grouped data variable: tempGroupedData3
// Save grouped data, on click
$("#groupedDataContainer").on('click', 'button#btnGroupedDataSave', function(){

    // Add check for null value in the name field
    if($("#txtGroupedDataName").val().length === 0){
        alert("Please enter a name for this group.");
        return;
    } else {
        var dataName = $("#txtGroupedDataName").val().trim()
    }
    
    // Add to array
    groupedData3.push({
        name: dataName, 
        data: tempGroupedData3}
    );
    // Add object to the local storage
    localStorage.setItem("groupedData3", JSON.stringify(groupedData3));
    // Refresh display of stored grouped data
    loadGroupedData(groupedData3);
    // Clear the name input
    document.getElementById('txtGroupedDataName').value='';
});
// Delete all saved grouped data
$("#groupedDataContainer").on('click', 'button#btnGroupedDataDeleteAll', function(){
    if(confirm("You are about to delete all stored data. Are you sure?")){
        deleteAllGroupedData();
        loadGroupedData(groupedData3);
    }
});

// End-of Grouped Data event handlers


// Comparables collection event handlers

// addComparable
// Add property, on button click in the modal. 
// On modal open, the selected property is temporarily stored in tempCompData
$(document).on('click', 'h4.modal-title button.addComparable', function(){
    addToComparableCollection(tempCompData);
    $(this).html('Added').attr('disabled', true);
    $('h4.modal-title button.removeComparable').attr('visibility', 'visible');
});

$(document).on('click', 'h4.modal-title button.removeComparable', function(){
removeComparableCollection(parseInt($(this).attr('dataid')));
    $(this).html('Removed').attr('disabled', true);
    $('h4.modal-title button.addComparable').attr('visible', true);
});

$(document).on('click', 'div.comparableControl.callControls button.btn.btn-primary.btn-block', function(){
    var dt = new Object();
    for(var i = 0; i < arrayRawData.length; i++){
        if(arrayRawData[i].id == $(this).attr('dataid')){
            dt = arrayRawData[i];
            break;
        }
    }
    addToComparableCollection(dt);
    $(this).html('Added').attr('disabled', true);
});

function addToComparableCollection(objData){
    var scormData = parent.scormGet('cmi.suspend_data');
    if(!(scormData === null || scormData == '')){
        scenarioThreeWorksheet = JSON.parse(scormData);
        if(scenarioThreeWorksheet.comparableData.length > 0){
             compData = scenarioThreeWorksheet.comparableData;
        }else{var compData=[];}
    }else{var scenarioThreeWorksheet={};var compData=[];}

    // compData = JSON.parse(localStorage.getItem("compDataCollectionS3"));
    if(!compData){
        compData = [];
    }
    // Add to array
    if(objData && compData.length < 9){
        compData.push(objData);
    }
    // Add object to the local storage
    // localStorage.setItem("compDataCollectionS3", JSON.stringify(compData));
    scenarioThreeWorksheet.comparableData = compData;
    parent.scormSend(JSON.stringify(scenarioThreeWorksheet));
}

// End-of Comparables collection event handlers

function resetChecked(){
    table.$('.selected_item').prop('checked', false);
    $('#checkall').prop('checked', false);
}

// JSON to CSV
function JSONToCSV(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function getMedian(args) {
    if (!args.length) {return 0};
    var numbers = args.slice(0).sort( function(a,b) {
        return a - b;
    });
    var middle = Math.floor(numbers.length / 2);
    var isEven = numbers.length % 2 === 0;
    return isEven ? Number((numbers[middle] + numbers[middle - 1]) / 2) : Number(numbers[middle]);
}

function processGroupedData(arrayData){
    // GLA
    var glaTotal = 0;
    var glaArray = [];
    for (var i = 0; i < arrayData.length; i++) {
        if (arrayData[i].status.toLowerCase() == 'sld') {
            glaTotal += parseInt(arrayData[i].gla);
            glaArray.push(parseInt(arrayData[i].gla));
        }
    }
    var glaAvg = glaTotal / arrayData.length;
    var glaMedian = getMedian(glaArray).toMarked();
    // Beds
    var bedTotal = 0;
    var bedsArray = [];
    for (var i = 0; i < arrayData.length; i++) {
        if (arrayData[i].status.toLowerCase() == 'sld') {
            bedTotal += parseInt(arrayData[i].bed);
            bedsArray.push(parseInt(arrayData[i].bed));
        }
    }
    var bedAvg = bedTotal / arrayData.length;
    var bedMedian = getMedian(bedsArray);
    // Year built
    var yearTotal = 0;
    var yearArray = [];
    for (var i = 0; i < arrayData.length; i++) {
        if (arrayData[i].status.toLowerCase() == 'sld') {    
            yearTotal += parseInt(arrayData[i].yearBuilt);
            yearArray.push(parseInt(arrayData[i].yearBuilt));
        }
    }
    var yearAvg = yearTotal / arrayData.length;
    var yearMedian = getMedian(yearArray);
    // Sales
    var SoldH = 0;
    var SoldArray = [];
    for (var i = 0; i < arrayData.length; i++) {
        if (arrayData[i].status.toLowerCase() == 'sld') {
            SoldH += parseInt(arrayData[i].soldPrice);
            SoldArray.push(arrayData[i].soldPrice);
        }
    }
    var SoldAvg = 0;
    if(arrayData.length != 0){
        SoldAvg = SoldH / arrayData.length;
    }
    var soldPriceMedian = getMedian(SoldArray);

    var soldPriceLow = 0;
    var soldPriceHigh = 0;
    if(SoldArray.length > 0){
        soldPriceLow = Math.min.apply(Math, SoldArray);
        soldPriceHigh = Math.max.apply(Math, SoldArray);
    }
    // Lot Size 
    var lotSizeTotal = 0;
    var lotSizeArray = [];
    for (var i = 0; i < arrayData.length; i++) {
        if (arrayData[i].status.toLowerCase() == 'sld') {
            lotSizeTotal += arrayData[i].siteSquareFeet;
            lotSizeArray.push(arrayData[i].siteSquareFeet);
        }
    }
    var lotSizeAvg = lotSizeTotal / arrayData.length;
    var lotSizeMedian = getMedian(lotSizeArray).toMarked();

    // Result objects
    var ret = {
        // GLA result object
        gla : {
            low       : glaArray.length > 0 ? Math.min.apply(Math, glaArray).toMarked() : 0,
            median    : glaMedian,
            high      : glaArray.length > 0 ? Math.max.apply(Math, glaArray).toMarked() : 0
        },
        // Beds result object
        beds : {
            low       : bedsArray.length > 0 ? Math.min.apply(Math, bedsArray).toMarked() : 0,
            median    : bedMedian,
            high      : bedsArray.length > 0 ? Math.max.apply(Math, bedsArray).toMarked() : 0
        },
        // Build year result object
        buildYear : {
            low       : yearArray.length > 0 ? Math.min.apply(Math, yearArray) : 0,
            median    : yearMedian,
            high      : yearArray.length > 0 ? Math.max.apply(Math, yearArray) : 0
        },
        // Sold price result object
        soldPrice : {
            low       : soldPriceLow,
            median    : soldPriceMedian,
            high      : soldPriceHigh
        },
        // Lot size object
        lotSize : {
            low       : lotSizeArray.length > 0 ? Math.min.apply(Math, lotSizeArray).toMarked() : 0,
            median    : lotSizeMedian,
            high      : lotSizeArray.length > 0 ? Math.max.apply(Math, lotSizeArray).toMarked() : 0
        }
    };

    return ret;
}

function loadGroupedData(objData){
    var strHtml = "";

    if(objData !== 'null' && objData !== 'undefined' && objData.length > 0){
        for(var i = 0; i < objData.length; i++){
            strHtml += "<div class='col-md-4'><div class='panel panel-default'>";
            strHtml += "<div class='item_info panel-heading'><h2 class='panel-title'>" + objData[i].name + "</h2></div>";
            strHtml += "<div class='panel-body'>";
            strHtml += "<table id=\"propertyCharacteristics\" class=\"table table table-condensed\">";
            strHtml += "<thead style=\"border:0\">";
            strHtml += "<tr>";
            strHtml += "<th>Subject</th>";
            strHtml += "<th>Low</th>";
            strHtml += "<th>Median</th>";
            strHtml += "<th>High</th>";
            strHtml += "</tr>";
            strHtml += "</thead>";
            strHtml += "<tbody>";
            strHtml += "<tr>";
            strHtml += "<th>Sale Price</th>";
            strHtml += "<td id=\"gdsalelow\">"+ objData[i].data.soldPrice.low.toCurrency(0) +"</td>";
            strHtml += "<td id=\"gdsalemed\">"+ objData[i].data.soldPrice.median.toCurrency(0) +"</td>";
            strHtml += "<td id=\"gdsalehigh\">"+ objData[i].data.soldPrice.high.toCurrency(0) +"</td>";
            strHtml += "</tr><tr>";
            strHtml += "<th>GLA</th>";
            strHtml += "<td id=\"gdglalow\">"+ objData[i].data.gla.low +"</td>";
            strHtml += "<td id=\"gdglamed\">"+ objData[i].data.gla.median +"</td>";
            strHtml += "<td id=\"gdglahigh\">"+ objData[i].data.gla.high +"</td>";
            strHtml += "</tr><tr>";
            strHtml += "<th>Bedrooms</th>";
            strHtml += "<td id=\"gdbedslow\">"+ objData[i].data.beds.low +"</td>";
            strHtml += "<td id=\"gdbedsmed\">"+ objData[i].data.beds.median +"</td>";
            strHtml += "<td id=\"gdbedshigh\">"+ objData[i].data.beds.high +"</td>";
            strHtml += "</tr><tr>";
            strHtml += "<th>Lot Size</th>";
            strHtml += "<td id=\"gdsizelow\">"+ objData[i].data.lotSize.low.toNumber(0) +"</td>";
            strHtml += "<td id=\"gdsizemed\">"+ objData[i].data.lotSize.median.toNumber(0) +"</td>";
            strHtml += "<td id=\"gdsizehigh\">"+ objData[i].data.lotSize.high.toNumber(0) +"</td>";
            strHtml += "</tr><tr>";                        
            strHtml += "<th>Year Built</th>";
            strHtml += "<td id=\"gdyearlow\">"+ objData[i].data.buildYear.low +"</td>";
            strHtml += "<td id=\"gdyearmed\">"+ objData[i].data.buildYear.median +"</td>";
            strHtml += "<td id=\"gdyearhigh\">"+ objData[i].data.buildYear.high +"</td>";
            strHtml += "</tr>";
            strHtml +="</tbody>";
            strHtml += "</table>";
            strHtml += "</div>";            
            strHtml += "<div class=\"panel-footer text-center\"><button class=\"btngdDelete btn btn-default\" recordId=\""+i+"\" dataName=\""+objData[i].name+"\"><span class='glyphicon glyphicon-trash'></span> Delete</button></div>";
            strHtml += "</div></div>"
        }
    }
    // Display html
    //$("#groupedDataDetail").load(window.location.href + " #groupedDataDetail");
    $("#groupedDataDetail").html(strHtml);
}

function deleteAllGroupedData(){
    // Empty local storage
    groupedData3 = [];
    localStorage.removeItem("groupedData3");
}

// issue reset warning if user hits browser back button
window.onbeforeunload = function() {
    return "You will lose your sorted data if you leave this page. If you want to go back to the main MLS page, use the Go Back button on the top right of this page.";
};

