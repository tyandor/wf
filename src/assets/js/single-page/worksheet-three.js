var scenarioThreeWorksheet = new Object();
var scenarioThreeFeedback = new Object();
var assessmentQuestions = [];

// set attempt enumerator
if (typeof variable !== 'undefined') {
    s3attempts = s3attempts;
} else {
    var s3attempts = 0;
}

// Get comparable data collection from local storage
$(document).ready(function(){
    
    // Main questions
    assessmentQuestions = buildQuestions();
    // Additional questions
    var additionalQuestions = shuffle(buildAdditionalQuestions());

    // Additional random two questions
    assessmentQuestions.push(additionalQuestions[0]);
    assessmentQuestions.push(additionalQuestions[1]);

    // Display assessment questions
    displayAssessmentQuestions(assessmentQuestions);

    // init scorm
    // scormInit();
    var scormData = parent.scormGet('cmi.suspend_data');

    if(!(scormData === null || scormData == '')){
        scenarioThreeWorksheet = JSON.parse(scormData);
        if(scenarioThreeWorksheet.comparableData.length > 0){
            compData = scenarioThreeWorksheet.comparableData;
        }
    } 

    // Fill out form
    if(typeof compData !== "undefined" && compData != null){
        // Comparable Sale no. 1
        if(compData[0]){
            var data = compData[0];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs1Address').val(addr);
            $('input#cs1ID').val(data.id);
            $('input#cs1Proximity').val(data.proximityToSubject);
            $('input#cs1SalePrice').val(data.soldPrice);
            $('input#cs1SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs1DateOfSaleTime').val(data.closingDate);
            $('input#cs1Site').val(data.siteSquareFeet);
            $('input#cs1Location').val(data.area);
            $('input#cs1View').val(data.view);
            $('input#cs1DesignStyle').val(data.designStyle);
            $('input#cs1QualityOfConstruction').val(data.quality);
            $('input#cs1ActualAge').val(ageActual);
            $('input#cs1Condition').val(data.condition);
            $('input#cs1AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs1AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs1AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs1GrossLivingArea').val(data.gla);
            $('input#cs1GarageCarport').val(data.garage);
            $('input#cs1SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs1SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs1DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs1LocationAdjustment').val(data.locationAdjustment);
            $('input#cs1LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs1SiteAdjustment').val(data.siteAdjustment);
            $('input#cs1ViewAdjustment').val(data.viewAdjustment);
            $('input#cs1DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs1QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs1ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs1ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs1AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs1AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs1GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs1BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs1FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs1HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs1EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs1GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs1PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs1PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs1SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs1SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs1LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs1NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs1NetAdjustment').val(data.netAdjustment);
            $('input#cs1AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs1AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs1AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 2
        if(compData[1]){
            var data = compData[1];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs2Address').val(addr);
            $('input#cs2ID').val(data.id);
            $('input#cs2Proximity').val(data.proximityToSubject);
            $('input#cs2SalePrice').val(data.soldPrice);
            $('input#cs2SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs2DateOfSaleTime').val(data.closingDate);
            $('input#cs2Site').val(data.siteSquareFeet);
            $('input#cs2Location').val(data.area);
            $('input#cs2View').val(data.view);
            $('input#cs2DesignStyle').val(data.designStyle);
            $('input#cs2QualityOfConstruction').val(data.quality);
            $('input#cs2ActualAge').val(ageActual);
            $('input#cs2Condition').val(data.condition);
            $('input#cs2AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs2AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs2AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs2GrossLivingArea').val(data.gla);
            $('input#cs2GarageCarport').val(data.garage);
            $('input#cs2SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs2SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs2DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs2LocationAdjustment').val(data.locationAdjustment);
            $('input#cs2LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs2SiteAdjustment').val(data.siteAdjustment);
            $('input#cs2ViewAdjustment').val(data.viewAdjustment);
            $('input#cs2DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs2QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs2ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs2ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs2AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs2AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs2GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs2BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs2FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs2HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs2EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs2GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs2PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs2PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs2SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs2SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs2LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs2NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs2NetAdjustment').val(data.netAdjustment);
            $('input#cs2AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs2AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs2AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 3
        if(compData[2]){
            var data = compData[2];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs3Address').val(addr);
            $('input#cs3ID').val(data.id);
            $('input#cs3Proximity').val(data.proximityToSubject);
            $('input#cs3SalePrice').val(data.soldPrice);
            $('input#cs3SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs3DateOfSaleTime').val(data.closingDate);
            $('input#cs3Site').val(data.siteSquareFeet);
            $('input#cs3Location').val(data.area);
            $('input#cs3View').val(data.view);
            $('input#cs3DesignStyle').val(data.designStyle);
            $('input#cs3QualityOfConstruction').val(data.quality);
            $('input#cs3ActualAge').val(ageActual);
            $('input#cs3Condition').val(data.condition);
            $('input#cs3AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs3AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs3AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs3GrossLivingArea').val(data.gla);
            $('input#cs3GarageCarport').val(data.garage);
            $('input#cs3SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs3SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs3DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs3LocationAdjustment').val(data.locationAdjustment);
            $('input#cs3LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs3SiteAdjustment').val(data.siteAdjustment);
            $('input#cs3ViewAdjustment').val(data.viewAdjustment);
            $('input#cs3DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs3QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs3ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs3ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs3AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs3AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs3GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs3BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs3FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs3HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs3EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs3GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs3PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs3PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs3SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs3SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs3LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs3NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs3NetAdjustment').val(data.netAdjustment);
            $('input#cs3AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs3AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs3AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 4
        if(compData[3]){
            var data = compData[3];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs4Address').val(addr);
            $('input#cs4ID').val(data.id);
            $('input#cs4Proximity').val(data.proximityToSubject);
            $('input#cs4SalePrice').val(data.soldPrice);
            $('input#cs4SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs4DateOfSaleTime').val(data.closingDate);
            $('input#cs4Site').val(data.siteSquareFeet);
            $('input#cs4Location').val(data.area);
            $('input#cs4View').val(data.view);
            $('input#cs4DesignStyle').val(data.designStyle);
            $('input#cs4QualityOfConstruction').val(data.quality);
            $('input#cs4ActualAge').val(ageActual);
            $('input#cs4Condition').val(data.condition);
            $('input#cs4AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs4AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs4AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs4GrossLivingArea').val(data.gla);
            $('input#cs4GarageCarport').val(data.garage);
            $('input#cs4SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs4SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs4DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs4LocationAdjustment').val(data.locationAdjustment);
            $('input#cs4LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs4SiteAdjustment').val(data.siteAdjustment);
            $('input#cs4ViewAdjustment').val(data.viewAdjustment);
            $('input#cs4DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs4QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs4ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs4ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs4AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs4AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs4GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs4BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs4FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs4HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs4EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs4GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs4PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs4PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs4SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs4SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs4LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs4NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs4NetAdjustment').val(data.netAdjustment);
            $('input#cs4AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs4AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs4AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 5
        if(compData[4]){
            var data = compData[4];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs5Address').val(addr);
            $('input#cs5ID').val(data.id);
            $('input#cs5Proximity').val(data.proximityToSubject);
            $('input#cs5SalePrice').val(data.soldPrice);
            $('input#cs5SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs5DateOfSaleTime').val(data.closingDate);
            $('input#cs5Site').val(data.siteSquareFeet);
            $('input#cs5Location').val(data.area);
            $('input#cs5View').val(data.view);
            $('input#cs5DesignStyle').val(data.designStyle);
            $('input#cs5QualityOfConstruction').val(data.quality);
            $('input#cs5ActualAge').val(ageActual);
            $('input#cs5Condition').val(data.condition);
            $('input#cs5AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs5AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs5AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs5GrossLivingArea').val(data.gla);
            $('input#cs5GarageCarport').val(data.garage);
            $('input#cs5SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs5SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs5DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs5LocationAdjustment').val(data.locationAdjustment);
            $('input#cs5LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs5SiteAdjustment').val(data.siteAdjustment);
            $('input#cs5ViewAdjustment').val(data.viewAdjustment);
            $('input#cs5DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs5QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs5ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs5ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs5AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs5AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs5GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs5BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs5FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs5HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs5EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs5GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs5PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs5PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs5SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs5SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs5LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs5NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs5NetAdjustment').val(data.netAdjustment);
            $('input#cs5AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs5AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs5AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 6
        if(compData[5]){
            var data = compData[5];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs6Address').val(addr);
            $('input#cs6ID').val(data.id);
            $('input#cs6Proximity').val(data.proximityToSubject);
            $('input#cs6SalePrice').val(data.soldPrice);
            $('input#cs6SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs6DateOfSaleTime').val(data.closingDate);
            $('input#cs6Site').val(data.siteSquareFeet);
            $('input#cs6Location').val(data.area);
            $('input#cs6View').val(data.view);
            $('input#cs6DesignStyle').val(data.designStyle);
            $('input#cs6QualityOfConstruction').val(data.quality);
            $('input#cs6ActualAge').val(ageActual);
            $('input#cs6Condition').val(data.condition);
            $('input#cs6AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs6AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs6AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs6GrossLivingArea').val(data.gla);
            $('input#cs6GarageCarport').val(data.garage);
            $('input#cs6SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs6SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs6DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs6LocationAdjustment').val(data.locationAdjustment);
            $('input#cs6LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs6SiteAdjustment').val(data.siteAdjustment);
            $('input#cs6ViewAdjustment').val(data.viewAdjustment);
            $('input#cs6DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs6QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs6ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs6ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs6AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs6AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs6GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs6BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs6FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs6HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs6EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs6GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs6PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs6PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs6SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs6SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs6LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs6NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs6NetAdjustment').val(data.netAdjustment);
            $('input#cs6AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs6AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs6AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 7
        if(compData[6]){
            var data = compData[6];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs7Address').val(addr);
            $('input#cs7ID').val(data.id);
            $('input#cs7Proximity').val(data.proximityToSubject);
            $('input#cs7SalePrice').val(data.soldPrice);
            $('input#cs7SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs7DateOfSaleTime').val(data.closingDate);
            $('input#cs7Site').val(data.siteSquareFeet);
            $('input#cs7Location').val(data.area);
            $('input#cs7View').val(data.view);
            $('input#cs7DesignStyle').val(data.designStyle);
            $('input#cs7QualityOfConstruction').val(data.quality);
            $('input#cs7ActualAge').val(ageActual);
            $('input#cs7Condition').val(data.condition);
            $('input#cs7AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs7AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs7AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs7GrossLivingArea').val(data.gla);
            $('input#cs7GarageCarport').val(data.garage);
            $('input#cs7SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs7SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs7DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs7LocationAdjustment').val(data.locationAdjustment);
            $('input#cs7LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs7SiteAdjustment').val(data.siteAdjustment);
            $('input#cs7ViewAdjustment').val(data.viewAdjustment);
            $('input#cs7DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs7QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs7ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs7ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs7AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs7AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs7GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs7BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs7FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs7HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs7EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs7GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs7PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs7PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs7SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs7SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs7LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs7NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs7NetAdjustment').val(data.netAdjustment);
            $('input#cs7AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs7AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs7AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 8
        if(compData[7]){
            var data = compData[7];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs8Address').val(addr);
            $('input#cs8ID').val(data.id);
            $('input#cs8Proximity').val(data.proximityToSubject);
            $('input#cs8SalePrice').val(data.soldPrice);
            $('input#cs8SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs8DateOfSaleTime').val(data.closingDate);
            $('input#cs8Site').val(data.siteSquareFeet);
            $('input#cs8Location').val(data.area);
            $('input#cs8View').val(data.view);
            $('input#cs8DesignStyle').val(data.designStyle);
            $('input#cs8QualityOfConstruction').val(data.quality);
            $('input#cs8ActualAge').val(ageActual);
            $('input#cs8Condition').val(data.condition);
            $('input#cs8AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs8AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs8AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs8GrossLivingArea').val(data.gla);
            $('input#cs8GarageCarport').val(data.garage);
            $('input#cs8SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs8SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs8DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs8LocationAdjustment').val(data.locationAdjustment);
            $('input#cs8LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs8SiteAdjustment').val(data.siteAdjustment);
            $('input#cs8ViewAdjustment').val(data.viewAdjustment);
            $('input#cs8DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs8QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs8ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs8ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs8AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs8AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs8GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs8BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs8FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs8HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs8EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs8GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs8PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs8PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs8SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs8SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs8LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs8NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs8NetAdjustment').val(data.netAdjustment);
            $('input#cs8AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs8AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs8AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 9
        if(compData[8]){
            var data = compData[8];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs9Address').val(addr);
            $('input#cs9ID').val(data.id);
            $('input#cs9Proximity').val(data.proximityToSubject);
            $('input#cs9SalePrice').val(data.soldPrice);
            $('input#cs9SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs9DateOfSaleTime').val(data.closingDate);
            $('input#cs9Site').val(data.siteSquareFeet);
            $('input#cs9Location').val(data.area);
            $('input#cs9View').val(data.view);
            $('input#cs9DesignStyle').val(data.designStyle);
            $('input#cs9QualityOfConstruction').val(data.quality);
            $('input#cs9ActualAge').val(ageActual);
            $('input#cs9Condition').val(data.condition);
            $('input#cs9AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs9AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs9AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs9GrossLivingArea').val(data.gla);
            $('input#cs9GarageCarport').val(data.garage);
            $('input#cs9SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs9SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs9DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs9LocationAdjustment').val(data.locationAdjustment);
            $('input#cs9LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs9SiteAdjustment').val(data.siteAdjustment);
            $('input#cs9ViewAdjustment').val(data.viewAdjustment);
            $('input#cs9DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs9QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs9ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs9ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs9AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs9AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs9GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);

            $('input#cs9BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs9FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs9HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs9EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs9GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs9PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs9PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs9SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs9SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs9LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs9NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs9NetAdjustment').val(data.netAdjustment);
            $('input#cs9AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs9AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs9AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 10
        if(compData[9]){
            var data = compData[9];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs10Address').val(addr);
            $('input#cs10ID').val(data.id);
            $('input#cs10Proximity').val(data.proximityToSubject);
            $('input#cs10SalePrice').val(data.soldPrice);
            $('input#cs10SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs10DateOfSaleTime').val(data.closingDate);
            $('input#cs10Site').val(data.siteSquareFeet);
            $('input#cs10Location').val(data.area);
            $('input#cs10View').val(data.view);
            $('input#cs10DesignStyle').val(data.designStyle);
            $('input#cs10QualityOfConstruction').val(data.quality);
            $('input#cs10ActualAge').val(ageActual);
            $('input#cs10Condition').val(data.condition);
            $('input#cs10AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs10AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs10AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs10GrossLivingArea').val(data.gla);
            $('input#cs10GarageCarport').val(data.garage);
            $('input#cs10SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs10SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs10DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs10LocationAdjustment').val(data.locationAdjustment);
            $('input#cs10LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs10SiteAdjustment').val(data.siteAdjustment);
            $('input#cs10ViewAdjustment').val(data.viewAdjustment);
            $('input#cs10DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs10QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs10ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs10ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs10AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs10AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs10GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);
            $('input#cs10BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs10FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs10HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs10EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs10GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs10PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs10PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs10SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs10SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs10LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs10NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs10NetAdjustment').val(data.netAdjustment);
            $('input#cs10AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs10AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs10AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 11
        if(compData[10]){
            var data = compData[10];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs11Address').val(addr);
            $('input#cs11ID').val(data.id);
            $('input#cs11Proximity').val(data.proximityToSubject);
            $('input#cs11SalePrice').val(data.soldPrice);
            $('input#cs11SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs11DateOfSaleTime').val(data.closingDate);
            $('input#cs11Site').val(data.siteSquareFeet);
            $('input#cs11Location').val(data.area);
            $('input#cs11View').val(data.view);
            $('input#cs11DesignStyle').val(data.designStyle);
            $('input#cs11QualityOfConstruction').val(data.quality);
            $('input#cs11ActualAge').val(ageActual);
            $('input#cs11Condition').val(data.condition);
            $('input#cs11AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs11AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs11AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs11GrossLivingArea').val(data.gla);
            $('input#cs11GarageCarport').val(data.garage);
            $('input#cs11SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs11SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs11DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs11LocationAdjustment').val(data.locationAdjustment);
            $('input#cs11LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs11SiteAdjustment').val(data.siteAdjustment);
            $('input#cs11ViewAdjustment').val(data.viewAdjustment);
            $('input#cs11DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs11QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs11ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs11ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs11AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs11AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs11GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);
            $('input#cs11BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs11FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs11HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs11EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs11GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs11PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs11PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs11SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs11SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs11LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs11NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs11NetAdjustment').val(data.netAdjustment);
            $('input#cs11AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs11AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs11AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }

        // Comparable Sale no. 12
        if(compData[11]){
            var data = compData[11];
            var addr = '';
            var d = new Date();
            d.setFullYear(2016);
            var ageActual = (d.getFullYear()-data.yearBuilt);
            console.log(ageActual);

            if(typeof data.address !== 'undefined'){
                addr = data.address;
                ageActual = data.actualAge;
            } else {
                addr = data.streetNumber + ' ' + data.streetName;
            }

            var totalRooms = (data.bed+data.bath);
            console.log(totalRooms);

            var regexp = /^([^,]*)/;
            var firstAmenity = data.amenities.match(regexp)[0];
            console.log(firstAmenity);

            $('input#cs12Address').val(addr);
            $('input#cs12ID').val(data.id);
            $('input#cs12Proximity').val(data.proximityToSubject);
            $('input#cs12SalePrice').val(data.soldPrice);
            $('input#cs12SalePriceGrossLivArea').val((data.soldPrice/data.gla).toFixed(2));
            $('input#cs12DateOfSaleTime').val(data.closingDate);
            $('input#cs12Site').val(data.siteSquareFeet);
            $('input#cs12Location').val(data.area);
            $('input#cs12View').val(data.view);
            $('input#cs12DesignStyle').val(data.designStyle);
            $('input#cs12QualityOfConstruction').val(data.quality);
            $('input#cs12ActualAge').val(ageActual);
            $('input#cs12Condition').val(data.condition);
            $('input#cs12AboveGradeRoomCountTotal').val((data.bed+data.bath));
            $('input#cs12AboveGradeRoomCountBdrms').val(data.bed);
            $('input#cs12AboveGradeRoomCountBaths').val(data.bath);
            $('input#cs12GrossLivingArea').val(data.gla);
            $('input#cs12GarageCarport').val(data.garage);
            $('input#cs12SiteAmenities1').val(data.amenities);

            // Adjustments
            $('input#cs12SaleOrFinancingConcessionsAdjustment').val(data.saleOrFinancingConcessionsAdjustment);
            $('input#cs12DateOfSaleTimeAdjustment').val(data.dateOfSaleTimeAdjustment);
            $('input#cs12LocationAdjustment').val(data.locationAdjustment);
            $('input#cs12LeaseholdFreeSimpleAdjustment').val(data.leaseholdFeeSimpleAdjusment);
            $('input#cs12SiteAdjustment').val(data.siteAdjustment);
            $('input#cs12ViewAdjustment').val(data.viewAdjustment);
            $('input#cs12DesignStyleAdjustment').val(data.designStyleAdjustment);
            $('input#cs12QualityOfConstructionAdjustment').val(data.qualityOfConstructionAdjustment);
            $('input#cs12ActualAgeAdjustment').val(data.actualAgeAdjustment);
            $('input#cs12ConditionAdjustment').val(data.conditionAdjustment);
            $('input#cs12AboveGradeRoomCountTotal').val(data.aboveGradeRoomCountTotal);
            $('input#cs12AboveGradeRoomCountAdjustment').val(data.aboveGradeRoomCountAdjustment);
            $('input#cs12GrossLivingAreaAdjustment').val(data.grossLivingAreaAdjustment);
            $('input#cs12BasementAndFinishedRoomsBelowGradeAdjustment').val(data.basementAndDinishedRoomsBelowGradeAdjusment);
            $('input#cs12FunctionalUtilityAdjustment').val(data.functionalUtilityAdjusment);
            $('input#cs12HeatingCoolingAdjustment').val(data.heatingCoolingAdjustment);
            $('input#cs12EnergyEfficientItemsAdjustment').val(data.energyEfficientItemsAdjustment);
            $('input#cs12GarageCarportAdjustment').val(data.garageCarportAdjustment);
            $('input#cs12PorchPatioDeckAdjustment').val(data.porchPatioDeckAdjusment);
            $('input#cs12PoolHouseAdjustment').val(data.poolHouseAdjusment);
            $('input#cs12SiteAmenities1Adjustment').val(data.siteAmenities1Adjustment);
            $('input#cs12SiteAmenities2Adjustment').val(data.siteAmenities2Adjustment);
            $('input#cs12LakeFrontageAdjustment').val(data.lakeFrontageAdjustment);
            $('input[name=cs12NetAdjustment][value='+data.netAdjustmentTotal+']').prop('checked', true);
            $('input#cs12NetAdjustment').val(data.netAdjustment);
            $('input#cs12AdjustedSalesPriceOfComparablesNetPercentage').val(data.adjustedSalesPriceOfComparablesNetPercentage);
            $('input#cs12AdjustedSalesPriceOfComparablesGrossPercentage').val(data.adjustedSalesPriceOfComparablesGrossPercentage);
            $('input#cs12AdjustedSalesPriceOfComparables').val(data.adjustedSalesPriceOfComparables);
        }
    }else{alert("You must complete comparable data on the MLS page before starting this worksheet.");}

    $('input[name=saleTransferHistory][value='+scenarioThreeWorksheet.saleTransferHistory+']').prop('checked', true);
    $('textarea#saleTransferHistoryExplanation').val(scenarioThreeWorksheet.saleTrasferHistoryExplanation);
    $('input[name=priorSaleTransferHistoryThreeYears][value='+scenarioThreeWorksheet.priorSaleTransferHistoryThreeYears+']').prop('checked', true);
    $('input#priorSaleTransferHistoryDataSources').val(scenarioThreeWorksheet.priorSaleTransferHistoryDataSources);
    $('input[name=priorSaleTransferHistoryOneYear][value='+scenarioThreeWorksheet.priorSaleTransferHistoryOneYear+']').prop('checked', true);
    $('input#priorSaleTransferHistoryOneYearDataSources').val(scenarioThreeWorksheet.priorSaleTransferHistoryOneYearDataSources);
    $('textarea#analysisOfPriorSaleTransferHistory').val(scenarioThreeWorksheet.analysisOfPriorSaleTransferHistory);
    $('textarea#summaryOfSalesComparisonApproach1').val(scenarioThreeWorksheet.summaryOfSalesComparisonApproach1);
    $('input#indicatedValueBySalesComparisonApproach').val(scenarioThreeWorksheet.indicatedValueBySalesComparisonApproach);
    $('input#indicatedValueByCostApproach').val(scenarioThreeWorksheet.indicatedValueByCostApproach);
    $('input#indicatedValueByIncomeApproach').val(scenarioThreeWorksheet.indicatedValueByIncomeApproach);
    $('textarea#recExplanation').val(scenarioThreeWorksheet.recExplanation);
    $('input#recAsIs').prop('checked', scenarioThreeWorksheet.recAsIs);
    $('input#recImprovementsCompleted').prop('checked', scenarioThreeWorksheet.recImprovementsCompleted);
    $('input#recRepairsAlterationsCompleted').prop('checked', scenarioThreeWorksheet.recImprovementsCompleted);
    $('input#recRepairsNotRequired').prop('checked', scenarioThreeWorksheet.recRepairsNotRequired);
    $('input#recRepairsNotRequiredExplanation').val(scenarioThreeWorksheet.recRepairsNotRequiredExplanation);
    $('input#finalMarketValue').val(scenarioThreeWorksheet.finalMarketValue);
    $('textarea#summaryOfSalesComparisonApproach2').val(scenarioThreeWorksheet.summaryOfSalesComparisonApproach2);
    $('textarea#summaryOfSalesComparisonApproach3').val(scenarioThreeWorksheet.summaryOfSalesComparisonApproach3);
    $('textarea#additionalComments').val(scenarioThreeWorksheet.additionalComments);
    $('textarea#costApproachSupportDescription').val(scenarioThreeWorksheet.costApproachSupportDescription);
    $('input#costApproachEstimated').prop('checked', scenarioThreeWorksheet.costApproachEstimated);
    $('input#costApproachReproduction').prop('checked', scenarioThreeWorksheet.costApproachReproduction);
    $('input#costApproachReplacementCostNew').prop('checked', scenarioThreeWorksheet.costApproachReplacementCostNew);
    $('input#costApproachSourceOfCostData').val(scenarioThreeWorksheet.costApproachSourceOfCostData);
    $('input#costApproachQualityRating').val(scenarioThreeWorksheet.costApproachQualityRating);
    $('input#costApproachEffectiveDate').val(scenarioThreeWorksheet.costApproachEffectiveDate);
    $('input#costApproachComments').val(scenarioThreeWorksheet.costApproachComments);    
    $('input#costApproachRemainingEconomicLife').val(scenarioThreeWorksheet.costApproachRemainingEconomicLife);
});

function saveScenarioThreeWorksheet () {
    var arr = [];
    if($('input#cs1Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs1ID').val();
        comp.address = $('input#cs1Address').val();
        comp.proximityToSubject = $('input#cs1Proximity').val();
        comp.soldPrice = $('input#cs1SalePrice').val();
        comp.salePricePerGLA = $('input#cs1SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs1DataSources').val();
        comp.verificationSources = $('input#cs1VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs1SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs1SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs1DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs1DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs1Location').val();
        comp.locationAdjustment = $('input#cs1LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs1LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs1LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs1Site').val();
        comp.siteAdjustment = $('input#cs1SiteAdjustment').val();
        comp.view = $('input#cs1View').val();
        comp.viewAdjustment = $('input#cs1ViewAdjustment').val();
        comp.designStyle = $('input#cs1DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs1DesignStyleAdjustment').val();
        comp.quality = $('input#cs1QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs1QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs1ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs1ActualAgeAdjustment').val();
        comp.condition = $('input#cs1Condition').val();
        comp.conditionAdjustment = $('input#cs1ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs1AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs1AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs1AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs1AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs1GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs1GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs1BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs1BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs1FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs1FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs1HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs1HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs1EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs1EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs1GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs1GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs1PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs1PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs1PoolHouseAdjustment').val();
        comp.amenities = $('input#cs1SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs1SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs1SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs1SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs1NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs1NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs1AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs1AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs1AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs1DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs1PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs1DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs1EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs1LakeFrontageAdjustment').val();

        arr.push(comp);
    }

    if($('input#cs2Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs2ID').val();
        comp.address = $('input#cs2Address').val();
        comp.proximityToSubject = $('input#cs2Proximity').val();
        comp.soldPrice = $('input#cs2SalePrice').val();
        comp.salePricePerGLA = $('input#cs2SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs2DataSources').val();
        comp.verificationSources = $('input#cs2VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs2SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs2SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs2DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs2DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs2Location').val();
        comp.locationAdjustment = $('input#cs2LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs2LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs2LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs2Site').val();
        comp.siteAdjustment = $('input#cs2SiteAdjustment').val();
        comp.view = $('input#cs2View').val();
        comp.viewAdjustment = $('input#cs2ViewAdjustment').val();
        comp.designStyle = $('input#cs2DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs2DesignStyleAdjustment').val();
        comp.quality = $('input#cs2QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs2QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs2ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs2ActualAgeAdjustment').val();
        comp.condition = $('input#cs2Condition').val();
        comp.conditionAdjustment = $('input#cs2ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs2AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs2AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs2AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs2AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs2GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs2GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs2BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs2BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs2FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs2FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs2HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs2HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs2EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs2EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs2GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs2GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs2PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs2PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs2PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs2PoolHouseAdjustment').val();
        comp.amenities = $('input#cs2SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs2SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs2SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs2SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs2NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs2NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs2AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs2AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs2AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs2DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs2PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs2DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs2EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs2LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs3Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs3ID').val();
        comp.address = $('input#cs3Address').val();
        comp.proximityToSubject = $('input#cs3Proximity').val();
        comp.soldPrice = $('input#cs3SalePrice').val();
        comp.salePricePerGLA = $('input#cs3SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs3DataSources').val();
        comp.verificationSources = $('input#cs3VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs3SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs3SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs3DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs3DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs3Location').val();
        comp.locationAdjustment = $('input#cs3LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs3LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs3LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs3Site').val();
        comp.siteAdjustment = $('input#cs3SiteAdjustment').val();
        comp.view = $('input#cs3View').val();
        comp.viewAdjustment = $('input#cs3ViewAdjustment').val();
        comp.designStyle = $('input#cs3DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs3DesignStyleAdjustment').val();
        comp.quality = $('input#cs3QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs3QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs3ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs3ActualAgeAdjustment').val();
        comp.condition = $('input#cs3Condition').val();
        comp.conditionAdjustment = $('input#cs3ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs3AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs3AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs3AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs3AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs3GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs3GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs3BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs3BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs3FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs3FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs3HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs3HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs3EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs3EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs3GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs3GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs3PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs3PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs3PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs3PoolHouseAdjustment').val();
        comp.amenities = $('input#cs3SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs3SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs3SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs3SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs3NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs3NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs3AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs3AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs3AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs3DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs3PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs3DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs3EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs3LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs4Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs4ID').val();
        comp.address = $('input#cs4Address').val();
        comp.proximityToSubject = $('input#cs4Proximity').val();
        comp.soldPrice = $('input#cs4SalePrice').val();
        comp.salePricePerGLA = $('input#cs4SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs4DataSources').val();
        comp.verificationSources = $('input#cs4VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs4SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs4SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs4DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs4DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs4Location').val();
        comp.locationAdjustment = $('input#cs4LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs4LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs4LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs4Site').val();
        comp.siteAdjustment = $('input#cs4SiteAdjustment').val();
        comp.view = $('input#cs4View').val();
        comp.viewAdjustment = $('input#cs4ViewAdjustment').val();
        comp.designStyle = $('input#cs4DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs4DesignStyleAdjustment').val();
        comp.quality = $('input#cs4QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs4QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs4ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs4ActualAgeAdjustment').val();
        comp.condition = $('input#cs4Condition').val();
        comp.conditionAdjustment = $('input#cs4ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs4AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs4AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs4AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs4AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs4GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs4GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs4BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs4BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs4FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs4FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs4HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs4HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs4EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs4EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs4GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs4GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs4PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs4PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs4PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs4PoolHouseAdjustment').val();
        comp.amenities = $('input#cs4SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs4SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs4SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs4SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs4NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs4NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs4AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs4AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs4AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs4DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs4PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs4DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs4EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs4LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs5Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs5ID').val();
        comp.address = $('input#cs5Address').val();
        comp.proximityToSubject = $('input#cs5Proximity').val();
        comp.soldPrice = $('input#cs5SalePrice').val();
        comp.salePricePerGLA = $('input#cs5SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs5DataSources').val();
        comp.verificationSources = $('input#cs5VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs5SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs5SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs5DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs5DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs5Location').val();
        comp.locationAdjustment = $('input#cs5LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs5LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs5LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs5Site').val();
        comp.siteAdjustment = $('input#cs5SiteAdjustment').val();
        comp.view = $('input#cs5View').val();
        comp.viewAdjustment = $('input#cs5ViewAdjustment').val();
        comp.designStyle = $('input#cs5DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs5DesignStyleAdjustment').val();
        comp.quality = $('input#cs5QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs5QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs5ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs5ActualAgeAdjustment').val();
        comp.condition = $('input#cs5Condition').val();
        comp.conditionAdjustment = $('input#cs5ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs5AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs5AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs5AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs5AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs5GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs5GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs5BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs5BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs5FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs5FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs5HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs5HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs5EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs5EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs5GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs5GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs5PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs5PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs5PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs5PoolHouseAdjustment').val();
        comp.amenities = $('input#cs5SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs5SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs5SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs5SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs5NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs5NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs5AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs5AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs5AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs5DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs5PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs5DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs5EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs5LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs6Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs6ID').val();
        comp.address = $('input#cs6Address').val();
        comp.proximityToSubject = $('input#cs6Proximity').val();
        comp.soldPrice = $('input#cs6SalePrice').val();
        comp.salePricePerGLA = $('input#cs6SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs6DataSources').val();
        comp.verificationSources = $('input#cs6VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs6SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs6SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs6DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs6DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs6Location').val();
        comp.locationAdjustment = $('input#cs6LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs6LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs6LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs6Site').val();
        comp.siteAdjustment = $('input#cs6SiteAdjustment').val();
        comp.view = $('input#cs6View').val();
        comp.viewAdjustment = $('input#cs6ViewAdjustment').val();
        comp.designStyle = $('input#cs6DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs6DesignStyleAdjustment').val();
        comp.quality = $('input#cs6QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs6QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs6ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs6ActualAgeAdjustment').val();
        comp.condition = $('input#cs6Condition').val();
        comp.conditionAdjustment = $('input#cs6ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs6AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs6AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs6AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs6AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs6GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs6GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs6BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs6BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs6FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs6FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs6HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs6HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs6EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs6EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs6GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs6GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs6PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs6PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs6PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs6PoolHouseAdjustment').val();
        comp.amenities = $('input#cs6SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs6SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs6SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs6SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs6NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs6NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs6AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs6AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs6AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs6DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs6PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs6DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs6EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs6LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs7Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs7ID').val();
        comp.address = $('input#cs7Address').val();
        comp.proximityToSubject = $('input#cs7Proximity').val();
        comp.soldPrice = $('input#cs7SalePrice').val();
        comp.salePricePerGLA = $('input#cs7SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs7DataSources').val();
        comp.verificationSources = $('input#cs7VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs7SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs7SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs7DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs7DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs7Location').val();
        comp.locationAdjustment = $('input#cs7LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs7LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs7LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs7Site').val();
        comp.siteAdjustment = $('input#cs7SiteAdjustment').val();
        comp.view = $('input#cs7View').val();
        comp.viewAdjustment = $('input#cs7ViewAdjustment').val();
        comp.designStyle = $('input#cs7DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs7DesignStyleAdjustment').val();
        comp.quality = $('input#cs7QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs7QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs7ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs7ActualAgeAdjustment').val();
        comp.condition = $('input#cs7Condition').val();
        comp.conditionAdjustment = $('input#cs7ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs7AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs7AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs7AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs7AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs7GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs7GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs7BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs7BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs7FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs7FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs7HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs7HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs7EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs7EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs7GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs7GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs7PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs7PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs7PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs7PoolHouseAdjustment').val();
        comp.amenities = $('input#cs7SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs7SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs7SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs7SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs7NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs7NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs7AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs7AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs7AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs7DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs7PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs7DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs7EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs7LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs8Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs8ID').val();
        comp.address = $('input#cs8Address').val();
        comp.proximityToSubject = $('input#cs8Proximity').val();
        comp.soldPrice = $('input#cs8SalePrice').val();
        comp.salePricePerGLA = $('input#cs8SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs8DataSources').val();
        comp.verificationSources = $('input#cs8VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs8SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs8SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs8DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs8DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs8Location').val();
        comp.locationAdjustment = $('input#cs8LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs8LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs8LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs8Site').val();
        comp.siteAdjustment = $('input#cs8SiteAdjustment').val();
        comp.view = $('input#cs8View').val();
        comp.viewAdjustment = $('input#cs8ViewAdjustment').val();
        comp.designStyle = $('input#cs8DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs8DesignStyleAdjustment').val();
        comp.quality = $('input#cs8QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs8QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs8ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs8ActualAgeAdjustment').val();
        comp.condition = $('input#cs8Condition').val();
        comp.conditionAdjustment = $('input#cs8ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs8AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs8AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs8AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs8AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs8GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs8GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs8BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs8BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs8FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs8FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs8HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs8HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs8EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs8EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs8GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs8GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs8PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs8PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs8PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs8PoolHouseAdjustment').val();
        comp.amenities = $('input#cs8SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs8SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs8SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs8SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs8NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs8NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs8AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs8AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs8AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs8DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs8PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs8DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs8EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs8LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs9Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs9ID').val();
        comp.address = $('input#cs9Address').val();
        comp.proximityToSubject = $('input#cs9Proximity').val();
        comp.soldPrice = $('input#cs9SalePrice').val();
        comp.salePricePerGLA = $('input#cs9SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs9DataSources').val();
        comp.verificationSources = $('input#cs9VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs9SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs9SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs9DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs9DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs9Location').val();
        comp.locationAdjustment = $('input#cs9LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs9LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs9LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs9Site').val();
        comp.siteAdjustment = $('input#cs9SiteAdjustment').val();
        comp.view = $('input#cs9View').val();
        comp.viewAdjustment = $('input#cs9ViewAdjustment').val();
        comp.designStyle = $('input#cs9DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs9DesignStyleAdjustment').val();
        comp.quality = $('input#cs9QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs9QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs9ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs9ActualAgeAdjustment').val();
        comp.condition = $('input#cs9Condition').val();
        comp.conditionAdjustment = $('input#cs9ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs9AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs9AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs9AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs9AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs9GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs9GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs9BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs9BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs9FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs9FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs9HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs9HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs9EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs9EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs9GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs9GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs9PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs9PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs9PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs9PoolHouseAdjustment').val();
        comp.amenities = $('input#cs9SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs9SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs9SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs9SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs9NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs9NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs9AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs9AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs9AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs9DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs9PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs9DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs9EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs9LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs10Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs10ID').val();
        comp.address = $('input#cs10Address').val();
        comp.proximityToSubject = $('input#cs10Proximity').val();
        comp.soldPrice = $('input#cs10SalePrice').val();
        comp.salePricePerGLA = $('input#cs10SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs10DataSources').val();
        comp.verificationSources = $('input#cs10VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs10SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs10SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs10DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs10DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs10Location').val();
        comp.locationAdjustment = $('input#cs10LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs10LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs10LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs10Site').val();
        comp.siteAdjustment = $('input#cs10SiteAdjustment').val();
        comp.view = $('input#cs10View').val();
        comp.viewAdjustment = $('input#cs10ViewAdjustment').val();
        comp.designStyle = $('input#cs10DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs10DesignStyleAdjustment').val();
        comp.quality = $('input#cs10QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs10QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs10ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs10ActualAgeAdjustment').val();
        comp.condition = $('input#cs10Condition').val();
        comp.conditionAdjustment = $('input#cs10ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs10AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs10AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs10AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs10AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs10GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs10GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs10BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs10BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs10FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs10FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs10HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs10HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs10EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs10EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs10GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs10GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs10PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs10PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs10PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs10PoolHouseAdjustment').val();
        comp.amenities = $('input#cs10SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs10SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs10SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs10SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs10NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs10NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs10AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs10AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs10AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs10DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs10PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs10DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs10EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs10LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs11Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs11ID').val();
        comp.address = $('input#cs11Address').val();
        comp.proximityToSubject = $('input#cs11Proximity').val();
        comp.soldPrice = $('input#cs11SalePrice').val();
        comp.salePricePerGLA = $('input#cs11SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs11DataSources').val();
        comp.verificationSources = $('input#cs11VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs11SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs11SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs11DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs11DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs11Location').val();
        comp.locationAdjustment = $('input#cs11LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs11LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs11LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs11Site').val();
        comp.siteAdjustment = $('input#cs11SiteAdjustment').val();
        comp.view = $('input#cs11View').val();
        comp.viewAdjustment = $('input#cs11ViewAdjustment').val();
        comp.designStyle = $('input#cs11DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs11DesignStyleAdjustment').val();
        comp.quality = $('input#cs11QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs11QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs11ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs11ActualAgeAdjustment').val();
        comp.condition = $('input#cs11Condition').val();
        comp.conditionAdjustment = $('input#cs11ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs11AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs11AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs11AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs11AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs11GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs11GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs11BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs11BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs11FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs11FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs11HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs11HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs11EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs11EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs11GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs11GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs11PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs11PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs11PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs11PoolHouseAdjustment').val();
        comp.amenities = $('input#cs11SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs11SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs11SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs11SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs11NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs11NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs11AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs11AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs11AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs11DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs11PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs11DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs11EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs11LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    if($('input#cs12Address').val() != ''){
        var comp = new Object();
        comp.id = $('input#cs12ID').val();
        comp.address = $('input#cs12Address').val();
        comp.proximityToSubject = $('input#cs12Proximity').val();
        comp.soldPrice = $('input#cs12SalePrice').val();
        comp.salePricePerGLA = $('input#cs12SalePriceGrossLivArea').val();
        comp.dataSources = $('input#cs12DataSources').val();
        comp.verificationSources = $('input#cs12VerificationSources').val();
        comp.saleOrFinancingConcessions = $('input#cs12SaleOrFinancingConcessions').val();
        comp.saleOrFinancingConcessionsAdjustment = $('input#cs12SaleOrFinancingConcessionsAdjustment').val();
        comp.closingDate = $('input#cs12DateOfSaleTime').val();
        comp.dateOfSaleTimeAdjustment = $('input#cs12DateOfSaleTimeAdjustment').val();
        comp.area = $('input#cs12Location').val();
        comp.locationAdjustment = $('input#cs12LocationAdjustment').val();
        comp.leaseholdFeeSimple = $('input#cs12LeaseholdFreeSimple').val();
        comp.leaseholdFeeSimpleAdjusment = $('input#cs12LeaseholdFreeSimpleAdjustment').val();
        comp.siteSquareFeet = $('input#cs12Site').val();
        comp.siteAdjustment = $('input#cs12SiteAdjustment').val();
        comp.view = $('input#cs12View').val();
        comp.viewAdjustment = $('input#cs12ViewAdjustment').val();
        comp.designStyle = $('input#cs12DesignStyle').val();
        comp.designStyleAdjustment = $('input#cs12DesignStyleAdjustment').val();
        comp.quality = $('input#cs12QualityOfConstruction').val();
        comp.qualityOfConstructionAdjustment = $('input#cs12QualityOfConstructionAdjustment').val();
        comp.actualAge = $('input#cs12ActualAge').val();
        comp.actualAgeAdjustment = $('input#cs12ActualAgeAdjustment').val();
        comp.condition = $('input#cs12Condition').val();
        comp.conditionAdjustment = $('input#cs12ConditionAdjustment').val();
        comp.aboveGradeRoomCountTotal = $('input#cs12AboveGradeRoomCountTotal').val();
        comp.bed = $('input#cs12AboveGradeRoomCountBdrms').val();
        comp.bath = $('input#cs12AboveGradeRoomCountBaths').val();
        comp.aboveGradeRoomCountAdjustment = $('input#cs12AboveGradeRoomCountAdjusment').val();
        comp.gla = $('input#cs12GrossLivingArea').val();
        comp.grossLivingAreaAdjustment = $('input#cs12GrossLivingAreaAdjustment').val();
        comp.basementAndFinishedRoomsBelowGrade = $('input#cs12BasementAndFinishedRoomsBelowGrade').val();
        comp.basementAndDinishedRoomsBelowGradeAdjusment = $('input#cs12BasementAndFinishedRoomsBelowGradeAdjusment').val();
        comp.functionalUtility = $('input#cs12FunctionalUtility').val();
        comp.functionalUtilityAdjusment = $('input#cs12FunctionalUtilityAdjustment').val();
        comp.heatingCooling = $('input#cs12HeatingCooling').val();
        comp.heatingCoolingAdjustment = $('input#cs12HeatingCoolingAdjustment').val();
        comp.energyEfficientItems = $('input#cs12EnergyEfficientItems').val();
        comp.energyEfficientItemsAdjustment = $('input#cs12EnergyEfficientItemsAdjustment').val();
        comp.garage = $('input#cs12GarageCarport').val();
        comp.garageCarportAdjustment = $('input#cs12GarageCarportAdjustment').val();
        comp.porchPatioDeck = $('input#cs12PorchPatioDeck').val();
        comp.porchPatioDeckAdjusment = $('input#cs12PorchPatioDeckAdjustment').val();
        comp.poolHouse = $('input#cs12PoolHouse').val();
        comp.poolHouseAdjusment = $('input#cs12PoolHouseAdjustment').val();
        comp.amenities = $('input#cs12SiteAmenities1').val();
        comp.siteAmenities1Adjustment = $('input#cs12SiteAmenities1Adjustment').val();
        comp.siteAmenities2 = $('input#cs12SiteAmenities2').val();
        comp.siteAmenities2Adjustment = $('input#cs12SiteAmenities2Adjustment').val();
        comp.netAdjustmentTotal = $('input[name=cs12NetAdjustment]:checked').val();
        comp.netAdjustment = $('input#cs12NetAdjustment').val();
        comp.adjustedSalesPriceOfComparablesNetPercentage = $('input#cs12AdjustedSalesPriceOfComparablesNetPercentage').val();
        comp.adjustedSalesPriceOfComparablesGrossPercentage = $('input#cs12AdjustedSalesPriceOfComparablesGrossPercentage').val();
        comp.adjustedSalesPriceOfComparables = $('input#cs12AdjustedSalesPriceOfComparables').val();
        comp.dateOfPriorSaleTransfer = $('input#cs12DateOfPriorSaleTransfer').val();
        comp.priceOfPriorSaleTransfer = $('input#cs12PriceOfPriorSaleTransfer').val();
        comp.dataSource = $('input#cs12DataSource').val();
        comp.effectiveDateOfDataSources = $('input#cs12EffectiveDateOfDataSources').val();
        comp.lakeFrontageAdjustment = $('input#cs12LakeFrontageAdjustment').val();
        
        arr.push(comp);
    }

    // Add comparable data to worksheet object
    scenarioThreeWorksheet.comparableData = arr;
    scenarioThreeWorksheet.saleTransferHistory = $('input[name=saleTransferHistory]:checked').val();
    scenarioThreeWorksheet.saleTrasferHistoryExplanation = $('textarea#saleTransferHistoryExplanation').val();
    scenarioThreeWorksheet.priorSaleTransferHistoryThreeYears = $('input[name=priorSaleTransferHistoryThreeYears]:checked').val();
    scenarioThreeWorksheet.priorSaleTransferHistoryDataSources = $('input#priorSaleTransferHistoryDataSources').val();
    scenarioThreeWorksheet.priorSaleTransferHistoryOneYear = $('input[name=priorSaleTransferHistoryOneYear]:checked').val();
    scenarioThreeWorksheet.priorSaleTransferHistoryOneYearDataSources = $('input#priorSaleTransferHistoryOneYearDataSources').val();
    scenarioThreeWorksheet.analysisOfPriorSaleTransferHistory = $('textarea#analysisOfPriorSaleTransferHistory').val();
    scenarioThreeWorksheet.summaryOfSalesComparisonApproach1 = $('textarea#summaryOfSalesComparisonApproach1').val();
    scenarioThreeWorksheet.indicatedValueBySalesComparisonApproach = $('input#indicatedValueBySalesComparisonApproach').val();
    scenarioThreeWorksheet.indicatedValueByCostApproach = $('input#indicatedValueByCostApproach').val();
    scenarioThreeWorksheet.indicatedValueByIncomeApproach = $('input#indicatedValueByIncomeApproach').val();
    scenarioThreeWorksheet.recExplanation = $('textarea#recExplanation').val();
    scenarioThreeWorksheet.recAsIs = $('input#recAsIs').is(':checked');
    scenarioThreeWorksheet.recImprovementsCompleted = $('input#recImprovementsCompleted').is(':checked');
    scenarioThreeWorksheet.recRepairsAlterationsCompleted = $('input#recRepairsAlterationsCompleted').is(':checked');
    scenarioThreeWorksheet.recRepairsNotRequired = $('input#recRepairsNotRequired').is(':checked');
    scenarioThreeWorksheet.recRepairsNotRequiredExplanation = $('input#recRepairsNotRequiredExplanation').val();
    scenarioThreeWorksheet.finalMarketValue = $('input#finalMarketValue').val();
    scenarioThreeWorksheet.summaryOfSalesComparisonApproach2 = $('textarea#summaryOfSalesComparisonApproach2').val();
    scenarioThreeWorksheet.summaryOfSalesComparisonApproach3 = $('textarea#summaryOfSalesComparisonApproach3').val();
    scenarioThreeWorksheet.additionalComments = $('textarea#additionalComments').val();
    scenarioThreeWorksheet.costApproachSupportDescription = $('textarea#costApproachSupportDescription').val();
    scenarioThreeWorksheet.costApproachEstimated = $('input#costApproachEstimated').is(':checked');
    scenarioThreeWorksheet.costApproachReproduction = $('input#costApproachReproduction').is(':checked');
    scenarioThreeWorksheet.costApproachReplacementCostNew = $('input#costApproachReplacementCostNew').is(':checked');
    scenarioThreeWorksheet.costApproachSourceOfCostData = $('input#costApproachSourceOfCostData').val();
    scenarioThreeWorksheet.costApproachQualityRating = $('input#costApproachQualityRating').val();
    scenarioThreeWorksheet.costApproachEffectiveDate = $('input#costApproachEffectiveDate').val();
    scenarioThreeWorksheet.costApproachComments = $('input#costApproachComments').val();    
    scenarioThreeWorksheet.costApproachRemainingEconomicLife = $('input#costApproachRemainingEconomicLife').val();

    // Send via scorm
    parent.scormSend(JSON.stringify(scenarioThreeWorksheet));
}

// save the worksheet click handler
$('#saveWorksheet').on('click', function(){

    // run the save worksheet function
    saveScenarioThreeWorksheet();

    // let the user know it worked
    $(this).addClass('hide');
    $(this).parent().find('[data-loading-end]').removeClass('hide');

    setTimeout(function() {
        $('[data-loading-start]').removeClass('hide');
        $('[data-loading-end]').addClass('hide');
        $('[data-success-message]').removeClass('hide');
    }, 3000);

    setTimeout(function() {
        $('[data-success-message]').fadeOut( "slow", function() {
            $(this).hide();
        });
    }, 4500);

});

function generateScenarioThreeFeedback() {
    var arr = scenarioThreeWorksheet.comparableData;
    var msg = [];

    // var fback contains the section numbers learners require to review
    // Section 1 Identify the problem you are trying to solve
    // Section 6 Write a Smart Report
    // Learners have to go view section 1 and 6
    var fback = [1, 2, 6];

    // Section 5 Verify Everything
    // Section 3 Find the comparables you need to solve the problem
    // Go through the learner's comparable
    var corrComp = false;
    var arrCorrComp = [
            '2664 silverthorne dr',
            '820 silver lake rd',
            '13588 blue ridge dr',
            '12108 cascade view dr',
            '2200 silverthorne dr',
            '15002 blue ridge dr',
            '11780 cascade mountain dr',
            '868 silver lake rd',
            '2712 silverthorne dr',
            '20547 pigeon lake rd'
        ];


    var points = 0;
    for(var i=0; i < arr.length; i++){
        // Search for the correct comparable, index 0, 2, or 4
        // At least one is selected
        var checkComp = arrCorrComp.indexOf(arr[i].address.toLowerCase().trim())
        if(checkComp == 0 || checkComp == 2 || checkComp == 4){
            corrComp = true;
            points += 2;
        }
    }

    // If learner's comparable is not the correct comparable, view section 3
    if(!corrComp){
        fback.push(3);
        msg.push('You found comparable properties, but there are comps that better compare to your subject property.');
    }

    // Comparable adjustment assessment
    var adjComp = true;
    for(var i = 0; i < arr.length; i++){
        // 2664 Silverthorne Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[0]){
            points++;
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 416000 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 480000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6816000 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6880000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 820 Silver Lake Road
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[1]){
            points++;
            // Date of sale/time adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 0 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 71250){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < -451500 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > -354750){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Site amenities 2
            if(arr[i].siteAmenities2 != 'No Dock' ||  arr[i].siteAmenities2 != '-dock' || arr[i].siteAmenities2 != 'no dock'){
                adjComp = false;
                msg.push(arr[i].address + ': Your Site Amenities adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Lake Frontage - this one looks in the Site adjustment field (see note on worksheet 3 instructions)
            if(parseInt(arr[i].siteAdjustment.replace(/,/g, "")) < -375000 ||  parseInt(arr[i].siteAdjustment.replace(/,/g, "")) > -262500){
                adjComp = false;
                msg.push(arr[i].address + ': Your Lake Frontage adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            console.log("Site/Lake Frontage Adjustment: " + (parseInt(arr[i].siteAdjustment)));
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6369750 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6579000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 13588 Blue Ridge Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[2]){
            points++;
            // Location adjustment
            if(parseInt(arr[i].locationAdjustment.replace(/,/g, "")) < 318750 ||  parseInt(arr[i].locationAdjustment.replace(/,/g, "")) > 573750){
                adjComp = false;
                msg.push(arr[i].address + ': Your Location adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6693750 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6948750){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 12108 Cascade View Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[3]){
            points++;
            // Location adjustment
            if(parseInt(arr[i].locationAdjustment.replace(/,/g, "")) < -936000 ||  parseInt(arr[i].locationAdjustment.replace(/,/g, "")) > -858000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Location adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6864000 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6942000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 2200 Silverthorne Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[4]){
            points++;
            // Date of sale/time adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 492000 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 615000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < 151250 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > 192500){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6793250 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6957500){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 15002 Blue Ridge Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[5]){
            points++;
            // Date of sale/time adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 123000 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 185000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < 341000 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > 434000){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Lake Frontage - this one looks in the Site adjustment field (see note on worksheet 3 instructions)
            if(parseInt(arr[i].siteAdjustment.replace(/,/g, "")) < 280000 ||  parseInt(arr[i].siteAdjustment.replace(/,/g, "")) > 400000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Lake Frontage adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            console.log("Site/Lake Frontage Adjustment: " + (parseInt(arr[i].siteAdjustment)));
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6894000 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 7168500){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 11780 Cascade Mountain Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[6]){
            points++;
            // Date of sale/time adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 352500 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 423000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < 247500 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > 315000){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Location adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < -934560 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > -841500){
                adjComp = false;
                msg.push(arr[i].address + ': Your Location adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6715440 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6946500){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 868 Silver Lake Road
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[7]){
            points++;
            // Date of sale/time adjustment
            if(parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) < 490000 ||  parseInt(arr[i].dateOfSaleTimeAdjustment.replace(/,/g, "")) > 630000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Time adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < -402500 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > -316250){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Lake Frontage - this one looks in the Site adjustment field (see note on worksheet 3 instructions)
            if(parseInt(arr[i].siteAdjustment.replace(/,/g, "")) < -540000 ||  parseInt(arr[i].siteAdjustment.replace(/,/g, "")) > -420000){
                adjComp = false;
                msg.push(arr[i].address + ': Your Lake Frontage adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            console.log("Site/Lake Frontage Adjustment: " + (parseInt(arr[i].siteAdjustment)));
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6487500 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6823750){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 2712 Silverthorne Drive
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[8]){
            points++;
            // List to Sale Price
            // if(parseInt(arr[i].listToSalePrice.replace(/,/g, "")) < -203850 ||  parseInt(arr[i].listToSalePrice.replace(/,/g, "")) > 0){
                // adjComp = false;
                // msg.push(arr[i].address + ': Your List to Sale Price adjustment is inaccurate based on the provided data.');
            // } else {
                // points++;
            // }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < 60775 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > 77350){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6651925 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6872350){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }

        // 20547 Pigeon Lake Road
        if(arr[i].address.toLowerCase().trim() == arrCorrComp[9]){
            points++;
            // List to Sale Price
            // if(parseInt(arr[i].listToSalePrice.replace(/,/g, "")) < -208500 ||  parseInt(arr[i].listToSalePrice.replace(/,/g, "")) > 0){
                // adjComp = false;
                // msg.push(arr[i].address + ': Your List to Sale Price adjustment is inaccurate based on the provided data.');
            // } else {
                // points++;
            // }
            // GLA adjustment
            if(parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) < -128100 ||  parseInt(arr[i].grossLivingAreaAdjustment.replace(/,/g, "")) > -100650){
                adjComp = false;
                msg.push(arr[i].address + ': Your GLA adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
            // Sale price adjustment
            if(parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) < 6613400 ||  parseInt(arr[i].adjustedSalesPriceOfComparables.replace(/,/g, "")) > 6849350){
                adjComp = false;
                msg.push(arr[i].address + ': Your Sale Price adjustment is inaccurate based on the provided data.');
            } else {
                points++;
            }
        }
    }

    // Adjustment assessment result
    if(!adjComp && points == 0){
        fback.push(4);
    }

    // Assessment questions
    $(".questions").each(function(){
        var questionId = parseInt($(this).attr('id'));
        var answer = $(this).val();
        // Find the answer
        var q = $.grep(assessmentQuestions, function(e){return e.id == questionId;});
        if(q[0].answer == answer){
            // Right, add one point
            points++;
        } else {
            // Wrong, provide section to review
            fback.push(q[0].section);
        }
    });

    // Indicated value by sales comparison approach
    // Set assessment result to view all sections, if learner miss this one
    var indicatedValue = scenarioThreeWorksheet.indicatedValueBySalesComparisonApproach ? parseInt(scenarioThreeWorksheet.indicatedValueBySalesComparisonApproach.replace(/,/g, "")) : 0; 
    if((indicatedValue < 6555000 || indicatedValue > 7245000)){
        fback = [];
        fback = ['all'];
        points = points - 101;
        msg.push('Your &quot;indicated value by sales comparison approach&quot; is incorrect by more than +/- 5%.');
    } else { points+=2; }

    scenarioThreeFeedback.points = points;
    scenarioThreeFeedback.sections = sort_unique(fback);
    scenarioThreeFeedback.message = msg;

    return scenarioThreeFeedback;
}

// submit form and generate feedback
$('#submitWorksheet').on('click', function(){

    // enumerate the attempt tracker
    s3attempts++;
    localStorage.setItem("scenarioThreeAttempts", s3attempts);

    // run the save function to generate comparableProperties object
    saveScenarioThreeWorksheet();

    // generate the assessment feedback
    generateScenarioThreeFeedback(); 

    // Store scenario two feedback to local storage | used in feedback-three.js
    localStorage.setItem("feedbackScenarioThree", JSON.stringify(scenarioThreeFeedback));
    
    // run scorm
    parent.scormComplete();

    // Go to feedback page
    window.location= "feedback.html";
});

// clear the saved worksheet data
function clearWorksheetData(){
    if(confirm("Are you sure? You are about to delete all of your saved comparable properties. You will have to re-add comps from the MLS, or add the information manually.")){
        parent.scormSet('cmi.suspend_data', '');
        location.reload();
    } else {
        return false;
    }
}

// event handler: clear all worksheet data
$('#clearWorksheet').on('click', function(){
    clearWorksheetData();
});

// scroll to the comps
$('#scrollToComps').on('click', function(){
    var theComps = $('.sales-comparison-approach');
    $('html, body').animate({
        scrollTop: theComps.offset().top
    }, 1000);
    return false;
});

// scroll to the addendum and back
$('#viewAddendum').on('click', function(){
    var theAddendum = $('.addendum-description');
    $('html, body').animate({
        scrollTop: theAddendum.offset().top
    }, 1000);
    $('#backToNeighborhood').show();
    return false;
});

// event handler: scroll to the comps
$('#backToNeighborhood').on('click', function(){
    var theNeighborhood = $('.neighborhood');
    $('html, body').animate({
        scrollTop: theNeighborhood.offset().top
    }, 1000);
    $('#backToNeighborhood').hide();
    return false;
});

// integer prototype
String.prototype.toNumber = function(){
    var str = this;
    if(str.length > 0){
        str = str.replace(/[^0-9\.]+/g, "");
    } else {
        str = "0";
    }
    // Check for decimal or round number
    if(str.indexOf(".") > -1){
        return parseFloat(str);
    } else {
        return parseInt(str);
    }
};

function shuffle(array) {
    var tmp, current, top = array.length;

    if(top) while(--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
    }

    return array;
}

function sort_unique(arr) {
    if (arr.length === 0) return arr;
    arr = arr.sort(function (a, b) { return a*1 - b*1; });
    var ret = [arr[0]];
    for (var i = 1; i < arr.length; i++) { 
        if (arr[i-1] !== arr[i]) {
            ret.push(arr[i]);
        }
    }
    return ret;
}

function buildQuestions(){
    var arrQuestion = [];

    // Question 1
    var tempChoices = [
                    'GLA',
                    'View',
                    'Private dock',
                    'Lake frontage'
                ];
    var objQuestion = new Object();
    objQuestion.id = 1;
    objQuestion.questionText = 'What was the most significant feaure of the subject property?';
    objQuestion.answer = 'Lake frontage';
    objQuestion.section = 1;
    objQuestion.choices = shuffle(tempChoices);

    arrQuestion.push(objQuestion);

    // Question 2
    var tempChoices = [
                    'Price',
                    'Date of sale',
                    'GLA',
                    'Location (market area)'
                ];
    var objQuestion = new Object();
    objQuestion.id = 2;
    objQuestion.questionText = 'You should consider all of these factors when analyzing the market, except:';
    objQuestion.answer = 'Price';
    objQuestion.section = 2;
    objQuestion.choices = shuffle(tempChoices);

    arrQuestion.push(objQuestion);

    // Question 3
    var tempChoices = [
                    '(New sale - old sale)/new sale = % of change',
                    '(New sale - old sale)/old sale = % of change',
                    '(Old sale - new sale)/old sale = % of change',
                    '(Old sale - new sale)/new sale = % of change'
                ];
    var objQuestion = new Object();
    objQuestion.id = 3;
    objQuestion.questionText = 'What is the formula for a time adjustment?';
    objQuestion.answer = '(New sale - old sale)/old sale = % of change';
    objQuestion.section = 4;
    objQuestion.choices = shuffle(tempChoices);

    arrQuestion.push(objQuestion);

    // Question 4
    var tempChoices = [
                    '2664 Silverthorne Dr.; same lake frontage and similar physical characteristics',
                    '13588 Blue Ridge Dr.; current sale with same lake frontage',
                    '12108 Cascade View Dr.; current sale with similar physical characteristics',
                    '2200 Silverthorne Dr.; similar lake frontage and similar design and style'
                ];
    var objQuestion = new Object();
    objQuestion.id = 4;
    objQuestion.questionText = 'Based on the course content, which comparable would be the most reliable and why?';
    objQuestion.answer = '2664 Silverthorne Dr.; same lake frontage and similar physical characteristics';
    objQuestion.section = 3;
    objQuestion.choices = shuffle(tempChoices);

    arrQuestion.push(objQuestion);

    return arrQuestion;
}

function buildAdditionalQuestions(){
    var arrAddQuestions = [];

    // Question 5
    var tempChoices = [
                    'Go back in time.',
                    'Use MLS.',
                    'Use public records.',
                    'Review old appraisal files.'
                ];
    var objQuestion = new Object();
    objQuestion.id = 5;
    objQuestion.questionText = 'What is one of the best ways to find additional comparables for high-end appraisal assignments?';
    objQuestion.answer = 'Go back in time.';
    objQuestion.section = 2;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    // Question 6
    var tempChoices = [
                    'Search parameters',
                    'Your years of experience',
                    'Results of the analysis',
                    'Source(s) of the data'
                ];
    var objQuestion = new Object();
    objQuestion.id = 6;
    objQuestion.questionText = 'When providing the supporting data for your adjustments, you should include all of the following except which?';
    objQuestion.answer = 'Your years of experience';
    objQuestion.section = 4;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    // Question 7
    var tempChoices = [
                    'When any adjustments are made to a comparable',
                    'When the adjustment to the comparable exceeds 5% of the sale price',
                    'When a significant feature of the comparable is similar, but not identical to the subject property and no adjustment has been made',
                    'When the net adjustments exceed 10% of the sale price of a comparable'
                ];
    var objQuestion = new Object();
    objQuestion.id = 7;
    objQuestion.questionText = 'In which of the following cases are you expected to provide supporting data?';
    objQuestion.answer = 'When a significant feature of the comparable is similar, but not identical to the subject property and no adjustment has been made';
    objQuestion.section = 4;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    // Question 8
    var tempChoices = [
                    'Paired data analysis',
                    'Trend analysis',
                    'Grouped data analysis',
                    'Regression'
                ];
    var objQuestion = new Object();
    objQuestion.id = 8;
    objQuestion.questionText = 'An adjustment technique that compares a larger data set of properties with and without a particular feature is known as:';
    objQuestion.answer = 'Grouped data analysis';
    objQuestion.section = 4;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    // Question 9
    var tempChoices = [
                    'market data grid',
                    'spreadsheet',
                    'calculator',
                    'regression tool'
                ];
    var objQuestion = new Object();
    objQuestion.id = 9;
    objQuestion.questionText = 'A ___________ can help you apply adjustments systematically and identify possible paired data sets.';
    objQuestion.answer = 'market data grid';
    objQuestion.section = 4;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    // Question 10
    var tempChoices = [
                    'Avoid stating the obvious.',
                    'Emphasize how the adjustments were derived.',
                    'Comment on insignificant items.',
                    'Start with the most crucial details.'
                ];
    var objQuestion = new Object();
    objQuestion.id = 10;
    objQuestion.questionText = 'You should consider all of the following guidelines when writing a smart report except which one?';
    objQuestion.answer = 'Comment on insignificant items.';
    objQuestion.section = 6;
    objQuestion.choices = shuffle(tempChoices);

    arrAddQuestions.push(objQuestion);

    return arrAddQuestions;
}

function displayAssessmentQuestions(questions){
    var str = "";
    for(var i = 0; i < questions.length; i++){
        str += '<li><label>'+questions[i].questionText+'</label>';
        str += '<select id="'+questions[i].id+'" class="questions">';
        for(var j = 0; j < questions[i].choices.length; j++){
            str += '<option value="'+questions[i].choices[j]+'">'+questions[i].choices[j]+'</option>';
        }
        str += '</select></li>'
    }

    $('div#assessmentQuestions').html(str);
}

// add double scrollbar to comp table
$(document).ready(function() {
    $('.double-scroll').doubleScroll({
        resetOnWindowResize: true
    });
});

// disable mousewheel on a input number field when in focus
$('input[type=number]').on('focus', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault();
    });
});

