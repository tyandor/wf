// event handler for home button
var siteurl = 'http://wf.21skills.com';

// init scrollmagic controller
var controller = new ScrollMagic.Controller();

$('#goHome').on('click', function(){
    window.location.href = siteurl+"/course/view.php?id=2";
});

// scrollbar progress
scrollProgress.set({
    color: '#ffffff',
    height: '7px',
    bottom: false
});

// toc nav
var toc = document.getElementsByClassName('section-nav');
    hamBurger = document.getElementById("sectionNav"),
    lineOne = document.getElementsByClassName('nav-one'),
    lineTwo = document.getElementsByClassName('nav-two'),
    lineThree = document.getElementsByClassName('nav-three'),
    lineFour = document.getElementsByClassName('nav-four'),
    tl = new TimelineMax({paused:true, reversed:false}),
    duration = 0.18,
    alphaduration = 0.15,
    rotation = 45,
    ease = Back.easeOut.config(2);

// toc animations
var flipTheBurger = new TimelineMax({paused:true, reversed:true})
    .from(lineOne, alphaduration, {y:-41, opacity:0}, 0)
    .from(lineFour, alphaduration, {y:41, opacity:0}, 0)
    .from(lineTwo, duration, {rotation:rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .from(lineThree, duration, {rotation:-rotation, transformOrigin:"center center", ease:ease}, 0.1)
    .to(toc, duration, {x:-400, ease: Circ.easeOut}, 0.5);

$(hamBurger).on('click', function(){
    flipTheBurger.reversed() ? flipTheBurger.play() : flipTheBurger.reverse();
});

// generate the table of contents
var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
        "<h4>Contents</h4>" +
        "<ul class='menu vertical'>";

var el, title, link;

$('.sect').each(function(index, elem) {

    el = $(this);
    title = el.attr("title");
    link = "#" + el.attr("id");

    newItem = 
        "<li>" +
            "<a href='" + link + "'>" +
                title +
            "</a>" +
        "</li>";

    ToC += newItem;
});

ToC +=
        "</ul>" +
    "</nav>";

$('.section-nav').append(ToC);

// set cookies for bookmarking
$('.sect').each(function(){
    var sectionID = '#' + $(this).attr("id");

    var scene = new ScrollMagic.Scene({triggerElement: sectionID, triggerHook: "onLeave", reverse: false})
        .on("start", function(e) {
            console.log(sectionID);

            if (!(Cookies.get(sectionID))) {
                Cookies.set(sectionID, 'true');
            }

            $("a[href*=\\"+sectionID+"]").addClass("viewed");
        })
        .addTo(controller);
});

// animate scroll to ToC sections
$('.section-nav nav ul li a').each(function() {
    var $this = $(this),
        scrollTo = $this.attr("href");

    $this.append("<span></span>");
    var uline = $(this).find("span");

    var underLine = new TimelineMax({paused:true});
        underLine.fromTo(uline, 0.25, {x:-400, opacity:0, ease:Sine.easeOut}, {x:0, opacity:1, ease:Sine.easeOut});

    $this.on('mouseenter', function() {
        underLine.play().timeScale(1);
    });

    $this.on('mouseleave', function() {
        underLine.reverse().timeScale(5);
    });

    if (Cookies.get(scrollTo)) {
        $(this).addClass("viewed");
    }

    $this.on('click', function() {
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top
        }, 1000, function() {
            flipTheBurger.play();
        });

        return false;
    });
});

// close ToC on scroll
$(window).scroll(function() {
    if (flipTheBurger.reversed()) {
        flipTheBurger.play();
    }
});

// animate all the supplimentary images
$(".circle-shape img").each(function (index, elem) {
    var circleUp = TweenMax.fromTo(elem, 0.5, {scale: 0.01, z: 0 }, {scale: 1, z: 0 });
    new ScrollMagic.Scene({
        duration: "50%",
        triggerElement: elem,
        triggerHook: "onEnter",
        offset: 100
    })
    .setTween(circleUp)
    .addTo(controller)
});

// #ls1 #compare video
$(document).ready(function() {
    var compareVideo = document.getElementById("#compareVideo");
});

function comparePlay() {
    compareVideo.play();
}
function comparePause() {
    compareVideo.pause();
}

new ScrollMagic.Scene({ 
    triggerElement: "#comparisonVideo", 
    triggerHook: 0.25
})

.on('enter', function () {
    comparePlay();
})
.on('end', function () {
    comparePause();
})
.addTo(controller);

new ScrollMagic.Scene({
    triggerElement: "#comparisonVideo",
    triggerHook: 0.25,
    })

.on('enter', function(){

    TweenMax.fromTo("#video-description", 1, 
        {scale: 0, opacity:0},
        {scale:1, opacity:1, delay:11.5, ease:Back.easeOut.config(1.5), force3D:true}, 
        0.2);    
})
.setClassToggle("#video-description p", "show-me")
.addTo(controller);

// PRIVACY VIDEO
jwplayer("video-privacy").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/9w/b5byxrFPH8Ih0WRO6l6reDR2AmY0hMZx8F0GrHhKA/ls1-1of2-price-of-privacy.mp4?x=0&h=71815aabe0dc701056e9110103a551a6",
    image: "../assets/img/ls/video-posters/price-of-privacy.jpg",
    tracks: [{ 
        file: "../assets/captions/ls1-1of2-price-of-privacy.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});

$(".play-privacy").click(function(e){
    $('.jw-media .jw-reset').get(0).play();
    e.preventDefault();
});

// Don't be that appraiser VIDEO
$(".play-shirt").click(function(e){
    $('.jw-media .jw-reset').get(1).play();
    e.preventDefault();
});

// DON'T BE THIS APPRAISER 
jwplayer("dont-be-appraiser").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/BL/NqgchduYn-GI83Um9pUmPsHQ2hR1K2lD-f-EwgFWM/ls1-2of2-no-shirt-no-shoes-no-appraisal.mp4?x=0&h=7e31ad16d10997c98be1bb8537351cc9",
    image: "../assets/img/ls/video-posters/no-shirt-no-shoes.jpg",
    tracks: [{ 
        file: "../assets/captions/ls1-2of2-no-shirt-no-shoes-no-appraisal.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});


// CONNECTING THE DOTS
jwplayer("connecting-the-dots-one").setup({
    file: "//production.smedia.lvp.llnw.net/8fc5279f395743bdb68c29b842d3e431/RH/hJJctuE32q5SrR5-MOExL-okH7CJi-kki8dzU4AOw/ls1-rick-message.mp4?x=0&h=89b07cc07915fe2d4b04becf34cbc1bd",
    image: "../assets/img/ls/video-posters/connecting-the-dots.jpg",
    tracks: [{ 
        file: "../assets/captions/ls1-rick-message.vtt", 
        label: "English",
        kind: "captions",
        "default": true 
    }],
    captions: {
        color: "FFCC00",
        backgroundColor: "000000",
        backgroundOpacity: 50
    }
});


// mansion map
$('.marker').on('click', function(){
    $('.marker').removeClass('active');
    $(this).toggleClass('active');    
});

$('#marker1').on('click', function(){
    $('.markerInfo').html( "<p>This built-in, lighted wall-niche is the perfect place to display priceless sculptures, urns or vases.</p>" );    
});

$('#marker2').on('click', function(){
    $('.markerInfo').html( "<p>This ornate ceiling features hand-carved flowers around the inner ceiling medallion. The twelve-arm chandelier illuminates the room with over 60 hand-cut lead crystal prisms.</p>" );
});

$('#marker3').on('click', function(){
    $('.markerInfo').html( "<p>Custom door and window moulding add to the quality of this elegant entrance.</p>" );
});

$('#marker4').on('click', function(){
    $('.markerInfo').html( "<p>This custom floor medallion is made from several types of granite and marble. Custom installations such as this one are a hallmark of quality in a high-end home.</p>" );
});

// animate the #keep-up list
var keepUp = new TimelineMax()
    .staggerFromTo("#keep-up ul li", 0.45, {scale:0, z:-500, ease:Elastic.easeOut, force3D:true}, {scale:1, z:0, ease:Power2.easeInOut}, 0.15);
 
var animateKeepUp = new ScrollMagic.Scene({
    triggerElement: '#keep-up',
    triggerHook: 0.5
})
.setTween(keepUp)
.addTo(controller);

// Change hamburger menu to yellow 
var yellowHamburger = new ScrollMagic.Scene({
    triggerElement: "#red-river-trail-problem",
    triggerHook: -0.1,
    duration: 10
})
.setTween(".section-nav #sectionNav .menubar", 
        {   
            background:"#F4E066"                                         
        })
.addTo(controller);

// Change menu back to black BACK IN BLACK
var blackHamburger = new ScrollMagic.Scene({
    triggerElement: "#points-to-remember-l1",
    triggerHook: -0.1,
    duration: 10
})
.setTween(".section-nav #sectionNav .menubar", .1, 
        {   
            background:"#0a0a0a"                                         
        })
.addTo(controller);

// animate the #points-to-remember bkg and header
var whatWeExpect = new TimelineMax()
    .add("end", 1)
    .fromTo('.alpha-ramp', 1, {css:{backgroundColor:"#557A8E"}, ease:Power2.easeOut}, {css:{backgroundColor:"#5f9556"}, ease:Power2.easeOut}, "end")
    .fromTo('#points-to-remember-l1 h2', 1, {y:-700, scale:2, ease:Power2.easeOut}, {y:0, scale:1, ease:Power2.easeOut}, "end");

var animateWWE = new ScrollMagic.Scene({
    triggerElement: '#points-to-remember-l1',
    triggerHook: 0.9,
    offset: -350,
    duration: 900
})
.setTween(whatWeExpect)
.addTo(controller);

TweenMax.set(".points-to-remember ul", {visibility:"visible"});

var rememberThis = new TimelineMax()
    .staggerFromTo(".points-to-remember ul li", 0.5, {x:-250, opacity: 0, ease:Power1.easeOut}, {x:0, opacity:1, ease:Power1.easeOut}, 0.45)
    .fromTo(".l1", 0.5, {height:0}, {height:"100%"}, 2)
    .fromTo(".l2", 0.5, {width:0}, {width:"100%"})
    .fromTo(".l3", 0.5, {height:0}, {height:"100%"})
    .fromTo(".l4", 0.5, {width:0}, {width:"100%"})
    .timeScale(2);
 
var animatePTR = new ScrollMagic.Scene({
    triggerElement: '.points-to-remember',
    triggerHook: 0.5
})
.setTween(rememberThis)
.addTo(controller);

// animate learning section footer menu
var introListAnimate = new TimelineMax()
  .to(".ls-footer", 0.25, {opacity: 1})
  .fromTo(".ls-footer .cl-title", 0.5, {scale:0}, {scale:1}) 
  .add("clist", 1.75)
  .staggerFromTo(".ls-footer .cl-items .item", 0.234, {opacity:0, ease: Power2.easeInOut}, {opacity:1, ease: Power2.easeInOut}, 0.195, "clist")
  .staggerFromTo(".ls-footer .cl-items .square", 0.3, {scale:0, opacity:0, ease: Power2.easeInOut}, {scale:1, opacity:1, ease: Power2.easeInOut}, 0.195, "clist");
 
var introList = new ScrollMagic.Scene({
    triggerElement: '.ls-footer',
    triggerHook: 0.5
})
.setTween(introListAnimate)
.addTo(controller);

