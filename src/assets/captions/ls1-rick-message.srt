﻿1
00:00:00,952 --> 00:00:04,619
(gentle instrumental music)

2
00:00:08,120 --> 00:00:10,008
- When we look at a completed appraisal,

3
00:00:10,008 --> 00:00:12,077
particularly in the high end here,

4
00:00:12,077 --> 00:00:15,651
and we see extremely poor comp selection,

5
00:00:15,651 --> 00:00:17,624
that's just completely inexcusable.

6
00:00:17,624 --> 00:00:19,864
What we want you to do is stop using

7
00:00:19,864 --> 00:00:22,883
these perceived conforming rule sets.

8
00:00:22,883 --> 00:00:24,664
You know what they are,

9
00:00:24,664 --> 00:00:26,616
everything within 12 months

10
00:00:26,616 --> 00:00:29,464
or something no further than one mile.

11
00:00:29,464 --> 00:00:30,979
Those conforming rule sets,

12
00:00:30,979 --> 00:00:32,429
particularly in the high end,

13
00:00:32,429 --> 00:00:34,371
are probably our number one problem

14
00:00:34,371 --> 00:00:36,845
why we see poor comp selection.

15
00:00:36,845 --> 00:00:38,337
We're gonna talk a lot more about these

16
00:00:38,337 --> 00:00:41,769
conforming rule sets as
we go through this course.

17
00:00:41,769 --> 00:00:43,936
But if the problem is that you simply

18
00:00:43,936 --> 00:00:45,164
didn't understand the problem

19
00:00:45,164 --> 00:00:46,741
that you were trying to solve for,

20
00:00:46,741 --> 00:00:49,963
it's because you're not
using a simple Google search

21
00:00:49,963 --> 00:00:52,801
along with your county
records and your MLS.

22
00:00:52,801 --> 00:00:54,327
What we're asking you to do

23
00:00:54,327 --> 00:00:56,652
is to make certain you
do the front-end research

24
00:00:56,652 --> 00:00:58,998
that's required to understand everything

25
00:00:58,998 --> 00:01:01,015
about the subject property's quality,

26
00:01:01,015 --> 00:01:03,125
how high end or not it is,

27
00:01:03,125 --> 00:01:04,951
its condition, its location.

28
00:01:04,951 --> 00:01:06,375
It's those things are gonna help you

29
00:01:06,375 --> 00:01:08,785
pick the right kind of comparables.

30
00:01:08,785 --> 00:01:12,272
And if you do the front-end
research we're asking you to do,

31
00:01:12,272 --> 00:01:13,788
then you can make the decision,

32
00:01:13,788 --> 00:01:17,180
maybe I got to go get a
little more dated sale

33
00:01:17,180 --> 00:01:19,100
that has those features that are affecting

34
00:01:19,100 --> 00:01:20,551
the value of this property,

35
00:01:20,551 --> 00:01:24,476
and/or a distant comparable
that has those same features.

36
00:01:24,476 --> 00:01:27,068
Now, the other thing we're
asking you to work on,

37
00:01:27,068 --> 00:01:29,617
is to ensure you're up
with the current trends

38
00:01:29,617 --> 00:01:32,367
in the high end market.

39
00:01:32,367 --> 00:01:33,777
What does that mean?

40
00:01:33,777 --> 00:01:35,473
That means you've got to understand

41
00:01:35,473 --> 00:01:38,225
the new architectural
styles that are out there,

42
00:01:38,225 --> 00:01:39,505
the type of quality features

43
00:01:39,505 --> 00:01:41,351
they're putting in homes today.

44
00:01:41,351 --> 00:01:43,868
We're asking to ensure
that you understand those

45
00:01:43,868 --> 00:01:45,575
so when you walk into a home,

46
00:01:45,575 --> 00:01:48,220
you don't embarrass yourself
by not understanding

47
00:01:48,220 --> 00:01:51,174
those kinds of features
that are in this property.

48
00:01:51,174 --> 00:01:54,011
So again, what we're asking you to do

49
00:01:54,011 --> 00:01:56,497
is to make certain that you understand

50
00:01:56,497 --> 00:01:59,633
the problem we're trying
to solve, number one.

51
00:01:59,633 --> 00:02:02,769
And number two, you don't
let the perceived conforming

52
00:02:02,769 --> 00:02:05,819
rule sets drive your comp selection.

53
00:02:05,819 --> 00:02:08,540
And number three, prepare yourself

54
00:02:08,540 --> 00:02:10,193
so that you're ready and able

55
00:02:10,193 --> 00:02:11,804
to appraise the high end,

56
00:02:11,804 --> 00:02:15,407
understand all those
features about that property.

