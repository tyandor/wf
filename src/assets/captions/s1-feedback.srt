﻿1
00:00:00,626 --> 00:00:03,353
- So, you didn't do as
well as you thought.

2
00:00:03,353 --> 00:00:04,693
That's okay.

3
00:00:04,693 --> 00:00:07,089
We told you it'd be challenging.

4
00:00:07,089 --> 00:00:08,797
The following learning sections

5
00:00:08,797 --> 00:00:11,398
will show you exactly
what we're looking for.

6
00:00:11,398 --> 00:00:13,731
Trust us, trust the process.

