﻿1
00:00:00,597 --> 00:00:03,347
(sand whooshing)

2
00:00:05,860 --> 00:00:09,136
- We ran across this property,
it sat right on the sand

3
00:00:09,136 --> 00:00:11,886
with a beautiful oceanfront view.

4
00:00:12,852 --> 00:00:15,913
And when we were looking at the appraisal,

5
00:00:15,913 --> 00:00:18,077
something kinda struck me.

6
00:00:18,077 --> 00:00:21,548
Three of the comparables
were on the cliffs.

7
00:00:21,548 --> 00:00:24,863
Down the road about a quarter
mile, they were close,

8
00:00:24,863 --> 00:00:29,188
but they're on the cliff,
they're not on the sand.

9
00:00:29,188 --> 00:00:31,636
That's not the same buyer.

10
00:00:31,636 --> 00:00:34,368
Someone who wants to be on the sand

11
00:00:34,368 --> 00:00:37,640
generally is not gonna buy on the cliff.

12
00:00:37,640 --> 00:00:41,851
So we have to ask ourselves
why is the appraiser

13
00:00:41,851 --> 00:00:43,566
finding comparables on a cliff

14
00:00:43,566 --> 00:00:47,039
when the property's on the sand?

15
00:00:47,039 --> 00:00:49,892
I have to think that the
only reason they're trying

16
00:00:49,892 --> 00:00:51,929
to find those is 'cause maybe
they're are current sale

17
00:00:51,929 --> 00:00:53,925
and they're close.

18
00:00:53,925 --> 00:00:55,113
Well generally speaking,

19
00:00:55,113 --> 00:00:58,137
that's a pretty good comp selection.

20
00:00:58,137 --> 00:01:00,830
But in this particular case,

21
00:01:00,830 --> 00:01:02,748
they didn't understand the problem again.

22
00:01:02,748 --> 00:01:06,949
Problem is, you have a
property on the sand.

23
00:01:06,949 --> 00:01:09,975
So, all we had to do simply again

24
00:01:09,975 --> 00:01:12,548
was we did a really good market study,

25
00:01:12,548 --> 00:01:14,057
understood our market,

26
00:01:14,057 --> 00:01:17,290
was it appreciating,
depreciating or stable,

27
00:01:17,290 --> 00:01:20,884
found dated comparables on the beach,

28
00:01:20,884 --> 00:01:24,067
applied a very solid time adjustment

29
00:01:24,067 --> 00:01:26,239
that we put in our report,

30
00:01:26,239 --> 00:01:28,485
and were to establish
the correct valuation

31
00:01:28,485 --> 00:01:30,484
of this property.

32
00:01:30,484 --> 00:01:33,786
So we have to ask ourself
in comp selection,

33
00:01:33,786 --> 00:01:35,997
is that the same buyer?

34
00:01:35,997 --> 00:01:37,716
And if it's not the same buyer,

35
00:01:37,716 --> 00:01:39,726
let's go find comparables that are,

36
00:01:39,726 --> 00:01:41,614
and let's find those right comparables

37
00:01:41,614 --> 00:01:44,844
and make those right adjustments,
supportable adjustments,

38
00:01:44,844 --> 00:01:46,761
to get the value right.

