﻿1
00:00:08,520 --> 00:00:10,639
- Verification in the
appraisal process today

2
00:00:10,639 --> 00:00:12,940
is truly a lost art

3
00:00:12,940 --> 00:00:16,482
it's much much more than just
verifying the sales price

4
00:00:16,482 --> 00:00:18,899
and they physical characteristics.

5
00:00:18,899 --> 00:00:22,381
Understanding the sale on the
high end is very important.

6
00:00:22,381 --> 00:00:24,760
Understanding the
transaction, the motivation,

7
00:00:24,760 --> 00:00:27,043
the details of the property.

8
00:00:27,043 --> 00:00:31,280
They will help you understand
why it's sold for what it did.

9
00:00:31,280 --> 00:00:33,640
This is why it's so important

10
00:00:33,640 --> 00:00:35,560
that you verify this information

11
00:00:35,560 --> 00:00:39,501
with the real estate agents
involved in the transaction.

12
00:00:39,501 --> 00:00:43,442
Also remember, you can
Google these properties.

13
00:00:43,442 --> 00:00:46,894
Believe me, there's a lot
of eye-opening information

14
00:00:46,894 --> 00:00:49,720
that you'll find about that property.

15
00:00:49,720 --> 00:00:52,400
It can also be equally important

16
00:00:52,400 --> 00:00:54,722
that you develop relationships

17
00:00:54,722 --> 00:00:58,163
with other high-end real
estate professionals

18
00:00:58,163 --> 00:01:00,301
because they might know information

19
00:01:00,301 --> 00:01:02,719
about that property also.

20
00:01:02,719 --> 00:01:06,561
We can assure you that our
reviewers are taking the time

21
00:01:06,561 --> 00:01:08,579
to verify these properties.

22
00:01:08,579 --> 00:01:10,639
What you don't want to have happen,

23
00:01:10,639 --> 00:01:14,920
is for us to find information
about some of these properties

24
00:01:14,920 --> 00:01:17,800
that really truly makes a
difference in the valuation.

25
00:01:17,800 --> 00:01:21,741
Most important, we expect
to see your verification

26
00:01:21,741 --> 00:01:23,824
in your appraisal report.

