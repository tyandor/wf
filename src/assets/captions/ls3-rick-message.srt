﻿1
00:00:01,534 --> 00:00:04,117
(bright tones)

2
00:00:08,116 --> 00:00:10,975
- Find comps, not make adjustments.

3
00:00:10,975 --> 00:00:13,875
The process is finding comparables

4
00:00:13,875 --> 00:00:16,894
that are truly comparable
to the subject property

5
00:00:16,894 --> 00:00:19,622
in the minds of the buyer.

6
00:00:19,622 --> 00:00:21,820
The problem though is,

7
00:00:21,820 --> 00:00:26,064
what kinda limitations are
you putting on your research

8
00:00:26,064 --> 00:00:28,591
to find those kinda comparables

9
00:00:28,591 --> 00:00:32,712
that that same buyer
would consider comparable?

10
00:00:32,712 --> 00:00:35,366
Here's something we're
gonna ask you to do.

11
00:00:35,366 --> 00:00:37,286
Inside your report,

12
00:00:37,286 --> 00:00:40,757
we would like to see
your search parameters.

13
00:00:40,757 --> 00:00:44,430
We can tell from the
complexity of the assignment

14
00:00:44,430 --> 00:00:46,187
and your search parameters,

15
00:00:46,187 --> 00:00:50,354
if you have the kinda data it
takes to solve the problem.

16
00:00:51,360 --> 00:00:54,051
If your property is very complex,

17
00:00:54,051 --> 00:00:56,257
your search parameters are very narrow,

18
00:00:56,257 --> 00:00:58,378
I have a pretty good idea

19
00:00:58,378 --> 00:01:02,337
that you don't have enough
data to solve the problem.

20
00:01:02,337 --> 00:01:04,868
And on the other hand,
if your property is,

21
00:01:04,868 --> 00:01:08,553
again very complex but your
search parameters are very wide,

22
00:01:08,553 --> 00:01:10,615
very few filters,

23
00:01:10,615 --> 00:01:14,242
I'm almost assured you saw
enough of the right data

24
00:01:14,242 --> 00:01:16,040
to solve the problem.

25
00:01:16,040 --> 00:01:18,039
One more thing,

26
00:01:18,039 --> 00:01:22,156
if you go outside your
subject's market area

27
00:01:22,156 --> 00:01:23,906
to find a comparable,

28
00:01:25,011 --> 00:01:29,789
please take the time and
analyze the difference between

29
00:01:29,789 --> 00:01:32,843
your subject's market
area and the comparable.

30
00:01:32,843 --> 00:01:35,168
We find this to be a huge problem

31
00:01:35,168 --> 00:01:37,737
in the comp selection of high end.

32
00:01:37,737 --> 00:01:41,162
So if you go outside of your market area

33
00:01:41,162 --> 00:01:43,858
to find the same features
of the subject property

34
00:01:43,858 --> 00:01:45,735
to a different market area,

35
00:01:45,735 --> 00:01:48,190
what we're asking for you to do,

36
00:01:48,190 --> 00:01:50,354
is to analyze the differences between

37
00:01:50,354 --> 00:01:51,864
those two market areas,

38
00:01:51,864 --> 00:01:54,765
and what we expect to see in your report

39
00:01:54,765 --> 00:01:58,637
is this support for an
adjustment or non-adjustment

40
00:01:58,637 --> 00:02:02,804
for going outside of the market
area for that comparable.

