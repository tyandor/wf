﻿1
00:00:05,176 --> 00:00:07,647
(golf swing swoosh)

2
00:00:07,647 --> 00:00:11,499
- We have this area, a high-end,
prestigious golf course.

3
00:00:11,499 --> 00:00:15,612
It's an absolutely beautiful
area with great views.

4
00:00:15,612 --> 00:00:19,553
I mean, it's a very high-end,
desirable place to be.

5
00:00:19,553 --> 00:00:21,809
Right adjacent to it is another

6
00:00:21,809 --> 00:00:24,590
very high-end, desirable place.

7
00:00:24,590 --> 00:00:27,998
It's got great views also,
wonderful properties.

8
00:00:27,998 --> 00:00:30,964
It just doesn't have the golf course.

9
00:00:30,964 --> 00:00:34,615
We've run into this problem
in this area several times.

10
00:00:34,615 --> 00:00:37,463
Where, in this particular example,

11
00:00:37,463 --> 00:00:41,486
the property sells over
here in the non-golf area.

12
00:00:41,486 --> 00:00:43,763
And the appraisers reach out

13
00:00:43,763 --> 00:00:46,513
to the golf area for comparables.

14
00:00:47,639 --> 00:00:50,941
And appraise the property
and come in at a value.

15
00:00:50,941 --> 00:00:55,060
When we looked at it
from a review position,

16
00:00:55,060 --> 00:00:57,750
we thought, let's study the difference.

17
00:00:57,750 --> 00:00:59,956
Let's make sure there's
not a value difference

18
00:00:59,956 --> 00:01:02,145
between the golf area and this other area.

19
00:01:02,145 --> 00:01:04,737
Sure enough, we found a
very significant difference

20
00:01:04,737 --> 00:01:06,070
between the two.

21
00:01:07,473 --> 00:01:11,112
What's troubling is
that we had the support

22
00:01:11,112 --> 00:01:12,696
for that difference.

23
00:01:12,696 --> 00:01:15,076
We're using match pairing, group data set,

24
00:01:15,076 --> 00:01:17,409
it's very solid information.

25
00:01:18,935 --> 00:01:22,662
Most of the time, I'm reading
in the original appraisal,

26
00:01:22,662 --> 00:01:27,311
it's due to my experience
there's no adjustment.

27
00:01:27,311 --> 00:01:31,338
In today's appraisal world,
taking the techniques

28
00:01:31,338 --> 00:01:34,235
of match pairing and group data set,

29
00:01:34,235 --> 00:01:38,402
and applying those and
coming up with a solid number

30
00:01:39,661 --> 00:01:41,514
and putting it in the appraisal

31
00:01:41,514 --> 00:01:44,654
so people can see how
credible that adjustment is,

32
00:01:44,654 --> 00:01:49,012
that's the goal, that's
what we're looking for.

33
00:01:49,012 --> 00:01:53,179
We no longer want to, or accept,
your 20 years of experience

34
00:01:54,301 --> 00:01:56,413
on a significant adjustment.

35
00:01:56,413 --> 00:02:00,580
Again, take the time, learn
how to use these techniques.

36
00:02:01,703 --> 00:02:03,892
Learn how to bring in big data.

37
00:02:03,892 --> 00:02:06,273
Learn how to support this adjustment,

38
00:02:06,273 --> 00:02:10,229
put it in your appraisal, so
people can see it's credible.

39
00:02:10,229 --> 00:02:13,487
And make the adjustment
that's appropriate.

