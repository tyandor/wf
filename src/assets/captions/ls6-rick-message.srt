﻿1
00:00:01,202 --> 00:00:03,785
(bright tones)

2
00:00:08,263 --> 00:00:11,128
- There is no question in
the appraisal industry today,

3
00:00:11,128 --> 00:00:14,724
our appraisal reports are getting longer.

4
00:00:14,724 --> 00:00:17,540
We can tell you we're
having a real difficult time

5
00:00:17,540 --> 00:00:20,116
digesting all this information.

6
00:00:20,116 --> 00:00:22,725
It tends to be a little unorganized,

7
00:00:22,725 --> 00:00:24,521
redundant for sure,

8
00:00:24,521 --> 00:00:27,052
not clearly labeled at all,

9
00:00:27,052 --> 00:00:29,090
and quite frankly, some of the information

10
00:00:29,090 --> 00:00:30,673
is just irrelevant.

11
00:00:31,700 --> 00:00:35,258
So we've got to get better
at this as an industry.

12
00:00:35,258 --> 00:00:38,614
And that's what we're
calling a smart report.

13
00:00:38,614 --> 00:00:41,189
So what is a smart report?

14
00:00:41,189 --> 00:00:44,172
Well, it consists of
many different things,

15
00:00:44,172 --> 00:00:47,194
but for sure it has to include these.

16
00:00:47,194 --> 00:00:51,389
The data should be consistent
with your conclusions.

17
00:00:51,389 --> 00:00:55,308
And your report needs to
be organized and labeled.

18
00:00:55,308 --> 00:00:56,983
So if we wanna go in

19
00:00:56,983 --> 00:00:59,722
and find the support for
that locational adjustment,

20
00:00:59,722 --> 00:01:00,743
we can find it.

21
00:01:00,743 --> 00:01:04,335
If we want to go find that
support for the time adjustment,

22
00:01:04,335 --> 00:01:05,585
we can find it.

23
00:01:06,419 --> 00:01:09,851
And you've taken the time
to organize your report

24
00:01:09,851 --> 00:01:10,934
and label it.

25
00:01:11,771 --> 00:01:15,407
And really most importantly, be concise.

26
00:01:15,407 --> 00:01:19,078
Don't report or re-report the obvious.

27
00:01:19,078 --> 00:01:22,346
Just give us the information
that we need to know

28
00:01:22,346 --> 00:01:24,752
to agree with your report.

29
00:01:24,752 --> 00:01:27,243
I would put all those disclaimers,

30
00:01:27,243 --> 00:01:28,834
which are important,

31
00:01:28,834 --> 00:01:31,455
and put them at the back of the report.

32
00:01:31,455 --> 00:01:34,882
They tend to get in the way
as we're trying to understand

33
00:01:34,882 --> 00:01:37,299
your support for this report.

34
00:01:38,229 --> 00:01:40,271
So as an industry,

35
00:01:40,271 --> 00:01:42,068
this has become an issue,

36
00:01:42,068 --> 00:01:46,235
and we all need to work together
to improve this process.

