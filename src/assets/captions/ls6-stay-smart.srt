﻿1
00:00:00,316 --> 00:00:02,983
(upbeat jingle)

2
00:00:06,651 --> 00:00:07,522
- When I think about it,

3
00:00:07,522 --> 00:00:10,772
this profession, real estate appraiser,

4
00:00:11,982 --> 00:00:16,732
you know, it takes a lot
of time and a lot of talent

5
00:00:16,732 --> 00:00:20,191
to become a really good
appraiser, it really does,

6
00:00:20,191 --> 00:00:22,238
starting with all the education courses

7
00:00:22,238 --> 00:00:25,013
that we take to learn the techniques,

8
00:00:25,013 --> 00:00:28,331
the methodologies, and all the skills

9
00:00:28,331 --> 00:00:31,971
that are required to be a good appraiser.

10
00:00:31,971 --> 00:00:33,307
There's a lot that goes in

11
00:00:33,307 --> 00:00:36,307
to becoming a really good appraiser.

12
00:00:37,322 --> 00:00:39,944
So I don't really want
to go through all that,

13
00:00:39,944 --> 00:00:42,485
actually, but I do want to
recognize the time and effort

14
00:00:42,485 --> 00:00:46,296
it takes to be a very good appraiser.

15
00:00:46,296 --> 00:00:48,802
But what I want to do is look at

16
00:00:48,802 --> 00:00:51,469
what we've been discussing here,

17
00:00:53,017 --> 00:00:57,595
and just look at it from a
very simple point of view,

18
00:00:57,595 --> 00:01:01,428
and to say to ourselves,
I have an assignment.

19
00:01:04,143 --> 00:01:07,000
It's a high end assignment.

20
00:01:07,000 --> 00:01:10,374
It's gonna be a complex
property in some way, for sure.

21
00:01:10,374 --> 00:01:14,756
It might be very large
in size, great views,

22
00:01:14,756 --> 00:01:17,423
very unique features, something.

23
00:01:19,552 --> 00:01:22,635
But when we stop and truly understand

24
00:01:23,506 --> 00:01:25,583
the problem we're trying to solve,

25
00:01:25,583 --> 00:01:27,178
not just, well, it's big.

26
00:01:27,178 --> 00:01:28,599
I gotta solve for it's big.

27
00:01:28,599 --> 00:01:29,960
But really ensuring we understand

28
00:01:29,960 --> 00:01:32,598
what are those factors
driving to the value,

29
00:01:32,598 --> 00:01:34,624
and we gotta solve for those factors

30
00:01:34,624 --> 00:01:37,531
either through comp
selection or adjustments.

31
00:01:37,531 --> 00:01:39,788
We gotta understand that.

32
00:01:39,788 --> 00:01:44,194
Secondly, we gotta get out there
and study the market first.

33
00:01:44,194 --> 00:01:46,413
If you get your market studied first,

34
00:01:46,413 --> 00:01:50,644
that opens the window
to a ton of comparables.

35
00:01:50,644 --> 00:01:53,783
You don't have to stay within 12 months.

36
00:01:53,783 --> 00:01:57,063
If you study your market first,
you can go back a year, two,

37
00:01:57,063 --> 00:01:59,538
whatever it's gonna take
to solve this property,

38
00:01:59,538 --> 00:02:01,596
because you're gonna have
a credible set of data

39
00:02:01,596 --> 00:02:05,443
in your report to prove
your time adjustment.

40
00:02:05,443 --> 00:02:08,056
Three, go out and select
those comparables.

41
00:02:08,056 --> 00:02:09,603
Now you can go!

42
00:02:09,603 --> 00:02:12,887
Either select comparables
that are dated, like I said,

43
00:02:12,887 --> 00:02:14,385
and make a time adjustment,

44
00:02:14,385 --> 00:02:16,943
or select a comparable
that might be distant

45
00:02:16,943 --> 00:02:20,053
that carries the same
features you're looking for.

46
00:02:20,053 --> 00:02:22,067
But remember, if you go distance,

47
00:02:22,067 --> 00:02:25,480
you're gonna have to prove to
me that has the same location.

48
00:02:25,480 --> 00:02:28,397
Again, putting it in your appraisal report

49
00:02:28,397 --> 00:02:32,564
showing me that you have
analyzed the difference, right?

50
00:02:33,815 --> 00:02:36,815
Four, verify everything, everything!

51
00:02:39,042 --> 00:02:43,462
In the high end, these
transactions can be complicated.

52
00:02:43,462 --> 00:02:45,459
There are things you're not just gonna see

53
00:02:45,459 --> 00:02:47,627
from the MLS that are gonna tell you

54
00:02:47,627 --> 00:02:51,560
the whole story about why
it sold for what it did.

55
00:02:51,560 --> 00:02:54,415
Other real estate
professionals are your friend.

56
00:02:54,415 --> 00:02:56,202
You have to reach out to them.

57
00:02:56,202 --> 00:02:59,717
You have to make them
part of your process.

58
00:02:59,717 --> 00:03:01,851
Verify everything.

59
00:03:01,851 --> 00:03:06,155
And last, communicate this
report in a smart way,

60
00:03:06,155 --> 00:03:08,232
so when we receive this report,

61
00:03:08,232 --> 00:03:11,698
we can clearly see and understand

62
00:03:11,698 --> 00:03:15,871
the support you have
provided for that valuation.

63
00:03:15,871 --> 00:03:18,589
I tell you, if you do those things,

64
00:03:18,589 --> 00:03:22,672
it's gonna deliver a report
to us the first time,

65
00:03:23,517 --> 00:03:26,555
and in that first time, we're not gonna

66
00:03:26,555 --> 00:03:28,779
be kicking it back to you.

67
00:03:28,779 --> 00:03:30,707
You're gonna have
everything we're looking for

68
00:03:30,707 --> 00:03:34,374
to prove that's the
value of that property.

