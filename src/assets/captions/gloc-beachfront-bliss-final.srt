﻿1
00:00:00,679 --> 00:00:03,762
(upbeat dance music)

2
00:00:05,051 --> 00:00:08,446
- [Host] Welcome to the
Glamorous Life of Collinswood,

3
00:00:08,446 --> 00:00:12,409
where sometimes a property can
be a priceless work of art.

4
00:00:12,409 --> 00:00:14,576
Today's property celebrates this aesthetic

5
00:00:14,576 --> 00:00:16,822
in the beachfront locale that will leave

6
00:00:16,822 --> 00:00:20,719
even the most fickle art
aficionado seething with envy.

7
00:00:20,719 --> 00:00:22,923
Renowned architect Lorenzo Giuseppe

8
00:00:22,923 --> 00:00:25,826
partnered with eccentric
artist Paolo Piazzo

9
00:00:25,826 --> 00:00:28,603
to create this jewel
of modern architecture

10
00:00:28,603 --> 00:00:30,998
where cubes and circles work together

11
00:00:30,998 --> 00:00:34,243
to echo the tenets of
mid-century modern art.

12
00:00:34,243 --> 00:00:38,099
Inside, Piazzo's artistic
principles are on full display.

13
00:00:38,099 --> 00:00:41,042
Rooms of vivid colors
that delight the senses

14
00:00:41,042 --> 00:00:43,252
are matched with a minimalistic decor

15
00:00:43,252 --> 00:00:46,501
to create a sensation of beach side bliss.

16
00:00:46,501 --> 00:00:50,078
Custom tile installations
adorn the color changing spa

17
00:00:50,078 --> 00:00:52,233
and adjoining steam room.

18
00:00:52,233 --> 00:00:55,996
With 5,200 square feet of
imaginative living space

19
00:00:55,996 --> 00:00:59,202
that includes five
bedrooms, six bathrooms,

20
00:00:59,202 --> 00:01:03,065
a stainless steel kitchen,
a custom Piazzo art gallery,

21
00:01:03,065 --> 00:01:05,222
and a dock big enough for a yacht.

22
00:01:05,222 --> 00:01:07,389
The owner hopes to meet or exceed

23
00:01:07,389 --> 00:01:11,151
the robust listing price of $22 million.

24
00:01:11,151 --> 00:01:13,348
No other beachfront
property in Collinswood

25
00:01:13,348 --> 00:01:17,459
features the combined design
power of Giuseppe and Piazzo.

26
00:01:17,459 --> 00:01:20,166
This appraisal will likely
take many comparables

27
00:01:20,166 --> 00:01:21,614
with several adjustments.

28
00:01:21,614 --> 00:01:24,947
How will you reconcile and support them?

