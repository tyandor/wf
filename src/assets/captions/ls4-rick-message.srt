﻿1
00:00:00,958 --> 00:00:04,291
(calm electronic music)

2
00:00:08,564 --> 00:00:09,823
- When we get an appraisal report

3
00:00:09,823 --> 00:00:14,405
turn to the grid, and recognize
a significant adjustment.

4
00:00:14,405 --> 00:00:16,881
What we're expecting is to be able to turn

5
00:00:16,881 --> 00:00:21,588
into your addendum and find the
support for that adjustment.

6
00:00:21,588 --> 00:00:24,712
Remember in math class
and the teacher said,

7
00:00:24,712 --> 00:00:27,921
"Show your work", that's
what we're looking for.

8
00:00:27,921 --> 00:00:31,861
Data is really too rich
today not to be able

9
00:00:31,861 --> 00:00:34,826
to support those adjustments.

10
00:00:34,826 --> 00:00:38,909
In this lesson, we
illustrated several techniques

11
00:00:40,174 --> 00:00:43,592
to be used to support these adjustments.

12
00:00:43,592 --> 00:00:46,440
What we're asking you
to do, is to use any one

13
00:00:46,440 --> 00:00:49,326
of these techniques that are appropriate

14
00:00:49,326 --> 00:00:53,493
with the data required to
get the credible results.

15
00:00:54,770 --> 00:00:58,020
What we don't want to
see, is you telling us

16
00:00:58,020 --> 00:01:00,538
your years of experience for the support

17
00:01:00,538 --> 00:01:01,837
of the adjustment.

18
00:01:01,837 --> 00:01:03,337
Show us your work.

