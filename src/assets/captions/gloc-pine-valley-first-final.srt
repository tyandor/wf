﻿1
00:00:00,641 --> 00:00:02,757
(ragtime music)

2
00:00:02,757 --> 00:00:06,030
- [Host] Welcome to the
Glamorous Life of Collinswood,

3
00:00:06,030 --> 00:00:08,638
where properties sell with a bang!

4
00:00:08,638 --> 00:00:10,797
Today's property may not look high-end,

5
00:00:10,797 --> 00:00:13,112
but it was involved in historical events

6
00:00:13,112 --> 00:00:15,346
that were once the talk of the nation.

7
00:00:15,346 --> 00:00:17,625
So, grab your appraiser's tool box,

8
00:00:17,625 --> 00:00:19,604
you'll need every tool available

9
00:00:19,604 --> 00:00:23,446
to determine the final value
of this infamous property.

10
00:00:23,446 --> 00:00:26,828
From the outside, our subject
looks like a plain old house

11
00:00:26,828 --> 00:00:29,552
by the lake, not unlike
the other plain old houses

12
00:00:29,552 --> 00:00:30,700
that surround it.

13
00:00:30,700 --> 00:00:34,223
But within, you'll find a
charming abode frozen in time.

14
00:00:34,223 --> 00:00:36,701
The same family has
owned this three bedroom,

15
00:00:36,701 --> 00:00:39,627
two bath wooden home since the 1930s.

16
00:00:39,627 --> 00:00:42,522
It hasn't changed much
since, aside from some

17
00:00:42,522 --> 00:00:44,725
minor kitchen updates
for modern convenience,

18
00:00:44,725 --> 00:00:47,388
and a few dazzling decor additions,

19
00:00:47,388 --> 00:00:51,030
circa 1935 courtesy of the FBI.

20
00:00:51,030 --> 00:00:54,573
Over 2000 bullet holes to be precise.

21
00:00:54,573 --> 00:00:56,257
While Collinswood is known today for its

22
00:00:56,257 --> 00:00:58,805
crime-free streets and empty jail cells,

23
00:00:58,805 --> 00:01:03,133
in 1935, it was home to
public enemy number one,

24
00:01:03,133 --> 00:01:04,539
Ma Carter.

25
00:01:04,539 --> 00:01:06,945
After her sons rampaged the country

26
00:01:06,945 --> 00:01:10,460
on a five state crime spree,
Ma Carter took her cut

27
00:01:10,460 --> 00:01:13,926
of the money and retired to
this off-the-map neighborhood.

28
00:01:13,926 --> 00:01:16,889
Of course, the FBI
eventually tracked her down.

29
00:01:16,889 --> 00:01:19,369
When Ma refused to
surrender, federal officers

30
00:01:19,369 --> 00:01:22,520
opened fire for three tumultuous days,

31
00:01:22,520 --> 00:01:26,185
shooting more than 2000 bullets
into the home's windows,

32
00:01:26,185 --> 00:01:29,949
walls, furniture, and
ultimately, Ma's torso.

33
00:01:29,949 --> 00:01:32,554
The property owner tidied
the mess, but kept all

34
00:01:32,554 --> 00:01:35,448
original furnishings
present during the shootout.

35
00:01:35,448 --> 00:01:37,517
The property has been impeccably preserved

36
00:01:37,517 --> 00:01:39,960
for over 80 years, and is now up for sale

37
00:01:39,960 --> 00:01:42,327
for the very first time in its history.

38
00:01:42,327 --> 00:01:44,843
The asking price is three million dollars,

39
00:01:44,843 --> 00:01:47,867
almost three times the price
of other homes in the area.

40
00:01:47,867 --> 00:01:49,764
What tools would you use to appraise

41
00:01:49,764 --> 00:01:51,764
this notorious property?

