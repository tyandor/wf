﻿1
00:00:00,675 --> 00:00:04,258
(light instrumental music)

2
00:00:07,175 --> 00:00:10,008
- We received this appraisal,
it was a high-end home,

3
00:00:10,008 --> 00:00:12,124
beautiful home, it was up in the hills.

4
00:00:12,124 --> 00:00:13,817
And you know what it's like
to appraise up in the hills,

5
00:00:13,817 --> 00:00:18,371
there's not a lot of lot
utility to many of the lots.

6
00:00:18,371 --> 00:00:21,279
And the unique feature of
this particular property

7
00:00:21,279 --> 00:00:24,867
was that the seller had taken the lot

8
00:00:24,867 --> 00:00:27,034
and absolutely leveled it.

9
00:00:28,160 --> 00:00:30,243
Made it extremely usable.

10
00:00:31,270 --> 00:00:33,772
So, sitting here looking at the problem,

11
00:00:33,772 --> 00:00:35,422
we realized we've got a leveled lot

12
00:00:35,422 --> 00:00:36,955
and most of the comps on the appraisal

13
00:00:36,955 --> 00:00:39,088
were all not level.

14
00:00:39,088 --> 00:00:41,608
There wasn't much utility to those lots.

15
00:00:41,608 --> 00:00:43,742
And this happened to be a cut sale.

16
00:00:43,742 --> 00:00:45,711
So, they came in lower.

17
00:00:45,711 --> 00:00:48,556
So, we're asking ourselves,
kind of wondering,

18
00:00:48,556 --> 00:00:51,889
does this level lot actually have value?

19
00:00:52,805 --> 00:00:54,748
So, we went in the data
and looked for the data,

20
00:00:54,748 --> 00:00:55,830
and we really couldn't find

21
00:00:55,830 --> 00:00:57,661
other level lots in the area either.

22
00:00:57,661 --> 00:00:59,869
Very difficult to find.

23
00:00:59,869 --> 00:01:02,389
So, what we decided to do
was we needed to reach out

24
00:01:02,389 --> 00:01:04,572
to other real estate professionals.

25
00:01:04,572 --> 00:01:06,529
So, we went on the internet
and found a realtor

26
00:01:06,529 --> 00:01:09,455
who works this area exclusively.

27
00:01:09,455 --> 00:01:11,599
We called her up, we told
her what problem we had

28
00:01:11,599 --> 00:01:13,697
and the property that we were appraising.

29
00:01:13,697 --> 00:01:16,587
She goes, I know that
property, I know it well.

30
00:01:16,587 --> 00:01:18,659
And she told us all about the property,

31
00:01:18,659 --> 00:01:20,506
the subject property, although we,

32
00:01:20,506 --> 00:01:23,239
it helped verify more of it with us.

33
00:01:23,239 --> 00:01:26,661
And she said, you know what,
I actually just sold one

34
00:01:26,661 --> 00:01:28,502
just like that.

35
00:01:28,502 --> 00:01:31,777
The seller leveled the
lot, my buyer bought it,

36
00:01:31,777 --> 00:01:35,192
but it never hit MLS,
it's an off-market sale.

37
00:01:35,192 --> 00:01:37,177
The seller didn't want it on the market.

38
00:01:37,177 --> 00:01:39,228
So, it was an absolutely perfect sale.

39
00:01:39,228 --> 00:01:41,483
And when we applied
the sale on our review,

40
00:01:41,483 --> 00:01:44,528
it actually supported the purchase price.

41
00:01:44,528 --> 00:01:48,695
So, when you think about
this particular assignment,

42
00:01:50,103 --> 00:01:52,514
you have to look at a couple things here.

43
00:01:52,514 --> 00:01:55,432
Number one, did the original appraiser

44
00:01:55,432 --> 00:01:57,675
really understand what
problem they were solving?

45
00:01:57,675 --> 00:02:00,591
I think they probably did, I
think they probably realized

46
00:02:00,591 --> 00:02:03,277
that the level lot maybe
had some value or not,

47
00:02:03,277 --> 00:02:05,117
but they just couldn't find
any comparables to solve it

48
00:02:05,117 --> 00:02:08,679
and they didn't attempt any
other approach than that.

49
00:02:08,679 --> 00:02:10,950
What they didn't do was
reach out to the internet

50
00:02:10,950 --> 00:02:14,538
and find other real estate
professionals to help you.

51
00:02:14,538 --> 00:02:18,553
Reaching out and working with
the real estate professionals

52
00:02:18,553 --> 00:02:21,181
out there is extremely important

53
00:02:21,181 --> 00:02:23,085
to getting these values right.

54
00:02:23,085 --> 00:02:25,449
We would have not gotten
this value correctly

55
00:02:25,449 --> 00:02:28,656
if we would have just stuck with MLS data

56
00:02:28,656 --> 00:02:30,482
and what we had at our fingertips.

57
00:02:30,482 --> 00:02:33,518
We had to dig deeper and get information

58
00:02:33,518 --> 00:02:35,601
to solve this assignment.

