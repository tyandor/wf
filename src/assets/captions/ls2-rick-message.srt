﻿1
00:00:01,164 --> 00:00:03,747
(bright tones)

2
00:00:08,382 --> 00:00:10,668
- Market first, comp second,

3
00:00:10,668 --> 00:00:12,967
think about that for a minute.

4
00:00:12,967 --> 00:00:15,575
And let's think about the high end home.

5
00:00:15,575 --> 00:00:18,058
The high end home will and does

6
00:00:18,058 --> 00:00:19,615
have dynamic features

7
00:00:19,615 --> 00:00:22,063
that drives the value of this property,

8
00:00:22,063 --> 00:00:25,008
anywhere from it's location, it's quality,

9
00:00:25,008 --> 00:00:29,087
condition, amenities, and
think about trying to find

10
00:00:29,087 --> 00:00:31,420
comparables within 12 months

11
00:00:32,601 --> 00:00:34,640
to appraise that property.

12
00:00:34,640 --> 00:00:36,596
It's almost impossible to do,

13
00:00:36,596 --> 00:00:38,473
you might find one.

14
00:00:38,473 --> 00:00:41,000
Really the only way you can go out

15
00:00:41,000 --> 00:00:43,078
and really solve this problem,

16
00:00:43,078 --> 00:00:45,644
is sometimes you have to go dated.

17
00:00:45,644 --> 00:00:48,414
To go dated to find those same features,

18
00:00:48,414 --> 00:00:50,697
you're gonna have to study your market,

19
00:00:50,697 --> 00:00:52,535
or you're market first.

20
00:00:52,535 --> 00:00:54,831
So if you go back and study your market

21
00:00:54,831 --> 00:00:56,546
around your subject property,

22
00:00:56,546 --> 00:00:57,935
that market area,

23
00:00:57,935 --> 00:01:00,957
and decide is that market appreciating,

24
00:01:00,957 --> 00:01:03,578
depreciating or stable,

25
00:01:03,578 --> 00:01:05,784
now you can go back in time,

26
00:01:05,784 --> 00:01:10,517
grab the comparables that have
those same dynamic features

27
00:01:10,517 --> 00:01:13,339
and make the appropriate adjustment.

28
00:01:13,339 --> 00:01:17,588
What's important here is
that when you do this,

29
00:01:17,588 --> 00:01:21,519
we want to see your
research, your market trend

30
00:01:21,519 --> 00:01:24,537
in your report so we
can see the appropriate

31
00:01:24,537 --> 00:01:26,623
time adjustment was made.

32
00:01:26,623 --> 00:01:29,623
So again, market first, comp second.

33
00:01:30,461 --> 00:01:32,546
Study your market,

34
00:01:32,546 --> 00:01:35,405
so you have the ability
to go back in time,

35
00:01:35,405 --> 00:01:37,634
grab the appropriate comparables,

36
00:01:37,634 --> 00:01:39,673
make the correct adjustment,

37
00:01:39,673 --> 00:01:43,592
and stay away from some of those
online market trend numbers

38
00:01:43,592 --> 00:01:45,719
cause I'm sure the information in there

39
00:01:45,719 --> 00:01:48,164
is not what you're looking for.

