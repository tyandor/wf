﻿1
00:00:04,132 --> 00:00:07,085
- We just recently received a complaint.

2
00:00:07,085 --> 00:00:10,077
In fact, we kind of hear
this complaint a lot,

3
00:00:10,077 --> 00:00:12,092
to be honest with you.

4
00:00:12,092 --> 00:00:16,132
We had a very high-end
home, large, beautiful home.

5
00:00:16,132 --> 00:00:20,521
Boy, the finish of this
home was outstanding.

6
00:00:20,521 --> 00:00:23,744
And it was a smart home, too.

7
00:00:23,744 --> 00:00:27,911
The technology in it was
industry standard, for sure.

8
00:00:29,719 --> 00:00:31,684
What happened was is the
appraiser that went out

9
00:00:31,684 --> 00:00:36,407
to appraise the property,
three steps in the door,

10
00:00:36,407 --> 00:00:39,496
looks around the house and says,

11
00:00:39,496 --> 00:00:43,496
"Wow, I've never seen
anything quite this nice."

12
00:00:45,304 --> 00:00:47,637
And then persists, persists.

13
00:00:49,351 --> 00:00:51,694
Looked down at the floor and said,

14
00:00:51,694 --> 00:00:55,017
"What kind of material is that?"

15
00:00:55,017 --> 00:00:59,358
I think you all understand
the problem here.

16
00:00:59,358 --> 00:01:01,738
It's upon us to be as professional

17
00:01:01,738 --> 00:01:05,078
as this industry expects us to be.

18
00:01:05,078 --> 00:01:08,009
The minute you walk in that door,

19
00:01:08,009 --> 00:01:10,592
you're representing the client.

20
00:01:11,617 --> 00:01:14,670
And those people are expecting you

21
00:01:14,670 --> 00:01:17,707
to be a professional in your
profession and in your trade,

22
00:01:17,707 --> 00:01:21,218
and understand exactly your market areas,

23
00:01:21,218 --> 00:01:23,453
the trends that are
going on with buildings,

24
00:01:23,453 --> 00:01:26,604
smart homes, they expect that.

25
00:01:26,604 --> 00:01:29,463
You can kinda say they're sizing you up.

26
00:01:29,463 --> 00:01:31,804
It could be the realtor, the homeowner,

27
00:01:31,804 --> 00:01:35,599
whoever's at those property
is asking themselves,

28
00:01:35,599 --> 00:01:37,303
are you the professional

29
00:01:37,303 --> 00:01:41,303
that's gonna get the value
right on my property?

30
00:01:42,161 --> 00:01:44,695
Well, I hope you are.

31
00:01:44,695 --> 00:01:47,619
But I can tell you personally myself,

32
00:01:47,619 --> 00:01:49,832
the only way I keep up on trends

33
00:01:49,832 --> 00:01:52,028
and understanding what are
the latest and greatest

34
00:01:52,028 --> 00:01:55,114
in building out there, from architectural

35
00:01:55,114 --> 00:01:57,812
to all the finishes inside.

36
00:01:57,812 --> 00:02:00,539
I just recently spent time

37
00:02:00,539 --> 00:02:02,735
and visited three different showrooms.

38
00:02:02,735 --> 00:02:07,197
You know, high-end bathroom
and kitchens, cabinetry,

39
00:02:07,197 --> 00:02:09,114
countertops, the works.

40
00:02:11,416 --> 00:02:12,961
And asked them questions.

41
00:02:12,961 --> 00:02:14,272
What are you seeing?

42
00:02:14,272 --> 00:02:15,764
What are people coming in,

43
00:02:15,764 --> 00:02:19,209
what are designers doing,
what are they talking about,

44
00:02:19,209 --> 00:02:22,148
what people are looking for?

45
00:02:22,148 --> 00:02:23,700
You have to do that.

46
00:02:23,700 --> 00:02:26,073
And when you're on site,

47
00:02:26,073 --> 00:02:30,171
you have to help people understand
you know what's going on.

48
00:02:30,171 --> 00:02:32,664
So, that takes care of understanding

49
00:02:32,664 --> 00:02:37,393
kind of the property itself
and your market area, right?

50
00:02:37,393 --> 00:02:40,401
That you establish your knowledge there.

51
00:02:40,401 --> 00:02:43,925
Secondly, you need to be professional

52
00:02:43,925 --> 00:02:46,494
with how you present yourself.

53
00:02:46,494 --> 00:02:49,822
Believe it or not, we
get the phone call a lot.

54
00:02:49,822 --> 00:02:54,523
"Appraiser showed up in shorts
and sandals, flip-flops."

55
00:02:54,523 --> 00:02:56,547
That doesn't represent you.

56
00:02:56,547 --> 00:03:00,214
Nor does it represent
your client, for sure.

57
00:03:01,285 --> 00:03:04,119
And that doesn't represent this industry.

58
00:03:04,119 --> 00:03:07,869
So, we're asking you,
please, be professional

59
00:03:09,124 --> 00:03:12,196
in how you present yourself,

60
00:03:12,196 --> 00:03:16,199
be professional in your
knowledge about the market area.

61
00:03:16,199 --> 00:03:19,050
Your knowledge about building trends.

62
00:03:19,050 --> 00:03:21,800
That you understand what it takes

63
00:03:22,923 --> 00:03:25,417
to know about this
property and this location

64
00:03:25,417 --> 00:03:28,334
to get that customer's value right.

