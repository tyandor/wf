﻿1
00:00:00,733 --> 00:00:02,680
- Hi, I'm Rick Langdon.

2
00:00:02,680 --> 00:00:04,920
We're here because we've been studying

3
00:00:04,920 --> 00:00:07,720
thousands of high-end appraisals.

4
00:00:07,720 --> 00:00:10,963
And we've found that 75% of them

5
00:00:10,963 --> 00:00:14,861
are being returned back
to you for correction.

6
00:00:14,861 --> 00:00:16,979
We've also heard from you directly.

7
00:00:16,979 --> 00:00:20,221
You said, just tell us
what your expectations are

8
00:00:20,221 --> 00:00:22,120
and we can deliver.

9
00:00:22,120 --> 00:00:25,101
That's exactly how we
designed this course.

10
00:00:25,101 --> 00:00:29,400
We've set out to show you exactly
what our expectations are.

11
00:00:29,400 --> 00:00:31,123
And the thing I like about this course,

12
00:00:31,123 --> 00:00:34,655
it gives you time to practice
and perfect those skills.

13
00:00:34,655 --> 00:00:36,840
This course is gonna be good for you,

14
00:00:36,840 --> 00:00:38,734
it's gonna be good for us

15
00:00:38,734 --> 00:00:40,893
and it's also gonna be
good for the customer.

16
00:00:40,893 --> 00:00:44,083
This course is gonna be
challenging and meaningful.

17
00:00:44,083 --> 00:00:44,916
Good luck.

