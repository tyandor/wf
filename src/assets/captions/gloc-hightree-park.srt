﻿1
00:00:00,836 --> 00:00:03,878
(upbeat music)

2
00:00:03,878 --> 00:00:07,356
- [Narrator] Welcome to the
Glamorous Life of Collinswood,

3
00:00:07,356 --> 00:00:09,567
where sometimes it's hard to distinguish

4
00:00:09,567 --> 00:00:13,192
between the celebrities and
the athletes they cheer for.

5
00:00:13,192 --> 00:00:15,493
Today's property is
located in the ultra posh

6
00:00:15,493 --> 00:00:17,402
High Tree Park neighborhood,

7
00:00:17,402 --> 00:00:21,181
home to some of Collinswood's
wealthiest celebrity athletes.

8
00:00:21,181 --> 00:00:23,618
This nine bedroom, 15 bathroom,

9
00:00:23,618 --> 00:00:26,082
56,000 square foot home

10
00:00:26,082 --> 00:00:29,460
belongs to a basketball
legend who has quite literally

11
00:00:29,460 --> 00:00:32,508
left his mark on this stunning property.

12
00:00:32,508 --> 00:00:34,866
While the rafters here may be banner free,

13
00:00:34,866 --> 00:00:37,449
this home is definitely an MVP.

14
00:00:38,472 --> 00:00:41,902
Complete with a full-sized
professional basketball court

15
00:00:41,902 --> 00:00:44,879
and an Olympic-size infinity pool.

16
00:00:44,879 --> 00:00:47,558
As you tour the grounds,
you'll find a series

17
00:00:47,558 --> 00:00:49,865
of murals that will transport you

18
00:00:49,865 --> 00:00:51,377
through the history of sports,

19
00:00:51,377 --> 00:00:53,146
all painted by some of the country's

20
00:00:53,146 --> 00:00:55,433
most famous street artists.

21
00:00:55,433 --> 00:00:59,039
To escape the paparazzi, this
former All-American champion

22
00:00:59,039 --> 00:01:01,787
has surrounded this
property with lush elms

23
00:01:01,787 --> 00:01:05,306
and white oaks which
offer exceptional privacy.

24
00:01:05,306 --> 00:01:07,682
Once inside the gate,
you need not worry about

25
00:01:07,682 --> 00:01:11,307
chance of air ball as this
property's secluded nature

26
00:01:11,307 --> 00:01:15,825
makes it nearly impossible
to see or hear the neighbors.

27
00:01:15,825 --> 00:01:19,007
Of course, such privacy and
luxury comes with a price,

28
00:01:19,007 --> 00:01:21,845
a hefty $15 million dollars.

29
00:01:21,845 --> 00:01:24,606
With such a unique property,
where do you even begin

30
00:01:24,606 --> 00:01:28,773
to find the information you
need to complete the appraisal?

