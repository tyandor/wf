﻿1
00:00:01,078 --> 00:00:04,245
(Scottish folk music)

2
00:00:08,205 --> 00:00:11,843
- [Narrator] Welcome to the
Glamorous Life of Collinswood.

3
00:00:11,843 --> 00:00:13,796
Where even those living the good life

4
00:00:13,796 --> 00:00:15,677
need to get away from it all.

5
00:00:15,677 --> 00:00:18,894
So, grab your skis and
your most poufy parka

6
00:00:18,894 --> 00:00:21,186
because today's property is located

7
00:00:21,186 --> 00:00:23,307
at the top of Mount Collins.

8
00:00:23,307 --> 00:00:25,912
This isn't your everyday
cabin in the woods,

9
00:00:25,912 --> 00:00:29,477
this Palladian manor
house is called Excelsior,

10
00:00:29,477 --> 00:00:32,475
and as the name implies,
its features are far greater

11
00:00:32,475 --> 00:00:34,411
than the average vacation homes.

12
00:00:34,411 --> 00:00:38,513
Among its comforts are 16
bedrooms, 18 bathrooms,

13
00:00:38,513 --> 00:00:42,392
a wine cellar, an indoor
pool, a 16 horse stable,

14
00:00:42,392 --> 00:00:45,101
a helipad, and an underground bunker

15
00:00:45,101 --> 00:00:48,005
large enough for a family of 24 to survive

16
00:00:48,005 --> 00:00:51,360
even the most desperate zombie apocalypse.

17
00:00:51,360 --> 00:00:53,941
Traditional English country
homes serve as inspiration

18
00:00:53,941 --> 00:00:56,506
for the home's symmetrical
interior layout.

19
00:00:56,506 --> 00:00:58,758
Many rooms feature a sporty atmosphere,

20
00:00:58,758 --> 00:01:01,851
but make no mistake, it's
not all tack and trophies

21
00:01:01,851 --> 00:01:03,030
at Excelsior.

22
00:01:03,030 --> 00:01:05,513
Elegance and grace are also a hallmark

23
00:01:05,513 --> 00:01:07,353
as many of the rooms are adorned with only

24
00:01:07,353 --> 00:01:10,278
the finest French country
style furnishings.

25
00:01:10,278 --> 00:01:14,232
In total, this gorgeous
property rests on 50 acres

26
00:01:14,232 --> 00:01:18,374
full of forests, streams,
and a private fishing pond.

27
00:01:18,374 --> 00:01:23,049
Truly, this is a retreat fit
for a royal hunting party.

28
00:01:23,049 --> 00:01:26,067
All interior furnishings
and six seat helicopter

29
00:01:26,067 --> 00:01:30,607
are included in the $95
million asking price.

30
00:01:30,607 --> 00:01:33,253
This is the fifth time this
property has been for sale

31
00:01:33,253 --> 00:01:34,812
in the past 10 years.

32
00:01:34,812 --> 00:01:38,394
Indeed, Excelsior has
changed hands many times

33
00:01:38,394 --> 00:01:40,978
during its 65 year life span.

34
00:01:40,978 --> 00:01:43,935
With this much luxury,
and a wealth of historical

35
00:01:43,935 --> 00:01:47,244
sales information, how
do we analyze the data

36
00:01:47,244 --> 00:01:50,744
to appraise this vacationer's dream home?

